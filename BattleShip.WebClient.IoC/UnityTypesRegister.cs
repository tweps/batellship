﻿using Microsoft.Practices.Unity;

namespace BattleShip.WebClient.IoC
{
    public static class UnityTypesRegister
    {
        public static void RegisterDomain(IUnityContainer container)
        {
            container.RegisterInstance<BattleShip.Domain.IGameDefinitions>(BattleShip.Domain.Game.GameDefinitions.Instance);
            container.RegisterType<BattleShip.Domain.IBattleShipGame, BattleShip.Domain.BattleShipGame>(new ContainerControlledLifetimeManager(), new InjectionConstructor()); 
        }
    }
}
