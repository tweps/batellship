﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain
{
    public interface IOponentBoard : IGameBoard
    {
        BoardState Shoot(int positionX, int positionY);
    }
}
