﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain
{
    public interface IPlayerBoard : IGameBoard
    {
        IShip GetShip(int positionX, int positionY);
        IEnumerable<IShip> GetAllShips();
        void Add(IShip ship);
        void Remove(IShip ship);
        bool CanAddShip(IShip ship);
        void ConfirmShipPlacement();

        IShip ConstructShip(int x, int y, int masts, ShipDirection direction);
    }
}
