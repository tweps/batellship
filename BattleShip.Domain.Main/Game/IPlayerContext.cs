﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BattleShip.Domain
{
    public interface IPlayerContext
    {
        Guid Id { get; }
        Guid GameId { get; }
        IPlayer Player { get; }
        IPlayer Oponent { get; set; }
        IPlayerBoard PlayerBoard { get; }
        IOponentBoard OponentBoard { get; }                
        GameStatus Status { get; }
        void UpdateStatus(GameStatus newStatus);
        void Notify();

        event EventHandler RefreshNotification;
    }
}
