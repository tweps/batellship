﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain
{
    public interface IPlayer : IEquatable<IPlayer>
    {
        string PlayerName { get; }
        string PlayerId { get; }        
        string AvatarUrl { get; }
    }
}
