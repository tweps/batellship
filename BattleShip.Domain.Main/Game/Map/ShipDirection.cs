﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BattleShip.Domain
{
    public enum ShipDirection
    {
        North,
        South,
        East,
        West
    }
}
