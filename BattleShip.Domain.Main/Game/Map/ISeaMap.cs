﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain
{
    public interface ISeaMap : IEnumerable, IEnumerable<ISeaMapPiece>
    {
        int SizeX { get; }
        int SizeY { get; }
        ISeaMapPiece this[int x, int y] { get; }
        ISeaMapPiece this[int i] { get; }
        ISeaMapPiece LastShoot { get; }
    }

    
}
