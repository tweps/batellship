﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain
{
    public interface ISeaMapPiece : IEquatable<ISeaMapPiece>
    {
        int PositionX { get; }
        int PositionY { get; }
        BoardState State { get; }
        bool IsHit { get; }
    }
}
