﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BattleShip.Domain
{
    public enum BoardState
    {
        Unknown = 0,
        Mishit = 1,
        Hit = 2,
        HitAndSunken = 3,  
        NotHit = 4,
    }
}
