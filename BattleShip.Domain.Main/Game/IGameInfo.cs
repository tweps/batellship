﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain
{
    public interface IGameInfo
    {
        Guid Id { get; set; }
        string Description { get; set;  }
        DateTime? Created { get; set; }
        IPlayer[] Players { get; set; }
    }
}
