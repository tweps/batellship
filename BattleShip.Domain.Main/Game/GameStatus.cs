﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain
{
    public enum GameStatus
    {
        NotStarted = 0,
        ShipsArrangement = 1,
        WaitingForOpponent = 2,
        PlayerTurn = 3,
        OpponentTurn = 4,
        PlayerHasWinTheMatch = 5,
        PlayerHasLoseTheMatch = 6
    }
}
