﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain
{
    public interface IGameDefinitions
    {
        int BoardSizeX { get; }
        int BoardSizeY { get; }
        IEnumerable<ShipDefinition> ShipsDefinitions { get; }        
    }    
}
