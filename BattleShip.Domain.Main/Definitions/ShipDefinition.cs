﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BattleShip.Domain
{
    public struct ShipDefinition
    {
        public ShipDefinition(int masts, int amountOfShips)
        {
            Masts = masts;
            AmountOfShips = amountOfShips;
        }

        public int Masts;
        public int AmountOfShips;
    }
}
