﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BattleShip.Domain
{
    public interface IBattleShipGame
    {       
        IPlayerContext Get(Guid contextId);
        IPlayerContext CreateGame(IPlayer player);
        IQueryable<IPlayerContext> GetAllUserContexts(IPlayer player);
        IQueryable<IPlayerContext> GetAllActiveUserContexts(IPlayer player);
        IQueryable<IGameInfo> GetFreeToJoin(/*IPlayer player*/);
        IPlayerContext JoinToGame(Guid id, IPlayer player);
        IGameInfo GetGameInfo(Guid gameId);

        event EventHandler<PlayerHasNewGameEventArgs> PlayerHasNewGame;
        event EventHandler<PublicGameEventArgs> PublicGameOpned;
        event EventHandler<PublicGameEventArgs> PublicGameClosed;
    }
}
