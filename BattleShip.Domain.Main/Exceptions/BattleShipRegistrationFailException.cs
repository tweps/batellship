﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Exceptions
{
    public class BattleShipRegistrationFailException : BattleShipException
    {
        public BattleShipRegistrationFailException(string meassage) : base(meassage)
        {

        }
    }
}
