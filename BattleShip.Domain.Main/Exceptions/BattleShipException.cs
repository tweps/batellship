﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Exceptions
{
    public class BattleShipException : Exception
    {
        public BattleShipException(string message) : base(message)
        {

        }
    }
}
