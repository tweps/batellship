﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.Domain
{
    public class PublicGameEventArgs : EventArgs
    {
        public IGameInfo PublicGame { get; private set; }

        public PublicGameEventArgs(IGameInfo publicGame)
        {
            PublicGame = publicGame;
        }
    }
}
