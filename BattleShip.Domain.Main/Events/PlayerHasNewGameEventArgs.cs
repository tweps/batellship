﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.Domain
{
    public class PlayerHasNewGameEventArgs : EventArgs
    {
        public IPlayer Player { get; private set; }
        public IPlayerContext PlayerContext { get; private set; }

        public PlayerHasNewGameEventArgs(IPlayer player, IPlayerContext playerContext)
        {
            this.Player = player;
            this.PlayerContext = playerContext;
        }
    }
}
