﻿using System.Web;
using System.Web.Http.ModelBinding;
using BattleShip.Domain;

namespace BattleShip.WebClient.Authentication
{    
    public class PlayerModelBinder : IModelBinder        
    {
        public bool BindModel(System.Web.Http.Controllers.HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            if(HttpContext.Current!=null)
            {
                IPlayer player = PlayerIdentity.Create(HttpContext.Current.User);
                if (player != null)
                {
                    bindingContext.Model = player;
                    return true;
                }
            }

            bindingContext.Model = null;
            return true;
        }
    }
    
}