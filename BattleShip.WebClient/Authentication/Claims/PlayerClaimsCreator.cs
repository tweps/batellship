﻿using System.Collections.Generic;
using System.Security.Claims;

namespace BattleShip.WebClient.Authentication.Claims
{
    public static class PlayerClaimsCreator
    {
        public static IEnumerable<Claim> Create(string provider, ClaimsIdentity externalIdentity)
        {
            return new[]
            {
                new Claim(BattleShipClaimType.PlayerName, externalIdentity.FindFirst(ClaimTypes.Name).Value),
                new Claim(BattleShipClaimType.NameIdentifier, externalIdentity.FindFirst(ClaimTypes.Name).Value + "@" + provider)
            };            
        }

        public static IEnumerable<Claim> CreateGuest(string nickName)
        {
            return new[]
            {
                new Claim(BattleShipClaimType.PlayerName, nickName),
                new Claim(BattleShipClaimType.NameIdentifier, nickName + "@BattleShip.Guest"),
                new Claim(BattleShipClaimType.Provider, "BattleShip.Guest"),
            };  
        }

    }
}
