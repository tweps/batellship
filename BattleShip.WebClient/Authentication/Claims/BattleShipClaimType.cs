﻿using System.Security.Claims;

namespace BattleShip.WebClient.Authentication.Claims
{
    public static class BattleShipClaimType
    {
        private const string SchemaUri = "http://schema.battleship.com/ws/2014/01/claims/";

        //public static readonly string PlayerId = SchemaUri + "playerId";
        public static readonly string PlayerName = ClaimTypes.Name;
        public static readonly string NameIdentifier = ClaimTypes.NameIdentifier;
        public static readonly string Provider =  "http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider";

           
        //public static readonly string Avatar = SchemaUri + "avatar";
    }
}
