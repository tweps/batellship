﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using BattleShip.WebClient.Authentication.Claims;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;

namespace BattleShip.WebClient.Authentication
{
    public class BattleShipGuestOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;

        // TO DO
        private static readonly ConcurrentDictionary<string, ClaimsIdentity> guests = new ConcurrentDictionary<string, ClaimsIdentity>(StringComparer.InvariantCultureIgnoreCase);

        public BattleShipGuestOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }

        
        // ReSharper disable once CSharpWarnings::CS1998
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            string nickName = context.UserName;

            if (string.IsNullOrEmpty(nickName))
            {
                context.SetError("You can not use empty nick name");
                return;
            }

            if (nickName.Length > 16)
            {
                context.SetError("Your nick name is too long");
                return;
            }

            ClaimsIdentity oAuthIdentity = new ClaimsIdentity("Bearer");
            oAuthIdentity.AddClaims(PlayerClaimsCreator.CreateGuest(nickName));

            if (!guests.TryAdd(nickName, oAuthIdentity))
            {
                context.SetError("Choose another nick, this is already in use");
                return;
            }

            var properties = BattleShipGuestOAuthProvider.CreateAuthenticationProperties(oAuthIdentity);
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);           
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {            
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateAuthenticationProperties(ClaimsIdentity oAuthIdentity)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", oAuthIdentity.Name }
            };

            AuthenticationProperties properties = new AuthenticationProperties(data);
            return properties;

        }

        
    }
}