﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using BattleShip.Domain;
using BattleShip.WebClient.Authentication.Claims;

namespace BattleShip.WebClient.Authentication
{
    public class PlayerIdentity : IPlayer
    {
        private readonly IEnumerable<Claim> claims;

        public static PlayerIdentity Create(System.Security.Principal.IPrincipal principal)
        {
            if(principal!=null && principal.Identity.IsAuthenticated && principal is ClaimsPrincipal)
            {
                return Create(((ClaimsPrincipal)principal).Claims);
            }

            return null;
        }

        public static PlayerIdentity Create(IEnumerable<Claim> claims)
        {
            return new PlayerIdentity(claims);
        }


        private PlayerIdentity(IEnumerable<Claim> claims)
        {            
            this.claims = claims.ToArray();         
        }

        private string GetClaim(string claimType)
        {
            var claim = claims.FirstOrDefault(c=> c.Type == claimType);

            if(claim!=null)
            {
                return claim.Value;
            }

            return null;
        }

        #region IPlayer

        public string PlayerName
        {
            get { return GetClaim(BattleShipClaimType.PlayerName); }
        }

        public string AvatarUrl
        {
            get { return string.Empty; }
        }

        public string PlayerId
        {
            get { return GetClaim(BattleShipClaimType.NameIdentifier); } 
        }

        #endregion IPlayer

        public override int GetHashCode()
        {
            return PlayerId.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var player = obj as IPlayer;
            return player != null && Equals(player);
        }

        public bool Equals(IPlayer other)
        {
            return this.PlayerId == other.PlayerId;
        }
    }
}
