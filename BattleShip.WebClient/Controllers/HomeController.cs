﻿using BattleShip.WebClient.Models.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BattleShip.WebExtensions.Seo;

namespace BattleShip.WebClient.Controllers
{
    [AllowAnonymous]
    public partial class HomeController : Controller
    {        
        public virtual ActionResult Index(HashBang hashBang)
        {
            // if page should be rendered without js (classic, not SPA mode )
            if( hashBang!=null )
            {
                return View(this.Views.IndexNoJS, new HomeCmsModel { CmsPage = hashBang.Value.Trim('/') } );
            }
                      
            return View(this.Views.Index);            
        }
    }
}