﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using BattleShip.Domain;
using BattleShip.WebClient.Authentication;
using BattleShip.WebClient.Authentication.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System.Threading;
using System.Net;
using BattleShip.WebClient.Models.Authentication;


namespace BattleShip.WebClient.Controllers
{  
    [RoutePrefix("ApiAuthentication")]
    public class ApiAuthenticationController : ApiController
    {
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return Request.GetOwinContext().Authentication;
            }
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("Providers", Name = "Providers")]
        public IEnumerable<ExternalProviderModel> GetProviders()
        {
            const string returnUrl = "/";
            string redirectUri = new Uri(this.Request.RequestUri, returnUrl).AbsoluteUri;
            var providers = (from a in AuthenticationManager.GetExternalAuthenticationTypes()
                select new ExternalProviderModel
                {
                    AuthenticationType = a.AuthenticationType,
                    Caption = a.Caption,
                    Url = Url.Route("ExternalLogin", new
                    {
                        provider = a.AuthenticationType,
                        response_type = "token",
                        client_id = Startup.PublicClientId,
                        redirect_uri = redirectUri
                        //state = state
                    })

                }).ToList();

            return providers;
        }

        [AllowAnonymous]
        [OverrideAuthentication]
        [HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [HttpGet]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public IHttpActionResult ExternalLogin(string provider)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return new ChallengeResult(provider, this);
            }

            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;


            if (claimsIdentity == null)
            {
                return InternalServerError();
            }
            
            var oAuthIdentity = new ClaimsIdentity(PlayerClaimsCreator.Create(provider, claimsIdentity), OAuthDefaults.AuthenticationType);
            var properties = BattleShipGuestOAuthProvider.CreateAuthenticationProperties(oAuthIdentity);
           

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            AuthenticationManager.SignIn(properties, oAuthIdentity);

            return Ok();
        }

        [Route("UserInfo")]
        public UserInfoModel GetUserInfo()
        {
            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;

            if (claimsIdentity != null)
            {
                var player = PlayerIdentity.Create(claimsIdentity.Claims);
                if (player != null)
                {
                    return new UserInfoModel
                    {
                        Id = player.PlayerId,
                        Name = player.PlayerName
                    };
                }

            }

            NotFound();
            return null;
        }

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            AuthenticationManager.SignOut(OAuthDefaults.AuthenticationType);
            return Ok();
        }
        
        private class ChallengeResult : IHttpActionResult
        {
            public ChallengeResult(string loginProvider, ApiController controller)
            {
                LoginProvider = loginProvider;
                Request = controller.Request;
            }

            private string LoginProvider { get; set; }
            private HttpRequestMessage Request { get; set; }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                Request.GetOwinContext().Authentication.Challenge(LoginProvider);

                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                response.RequestMessage = Request;
                return Task.FromResult(response);
            }
        }
    }
}