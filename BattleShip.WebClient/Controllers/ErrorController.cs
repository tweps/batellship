﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BattleShip.WebExtensions.JsExceptions;
using Elmah;
using System.Net;
using BattleShip.WebClient.Models;


namespace BattleShip.WebClient.Controllers
{
    public partial class ErrorController : Controller
    {        
        [HttpPost]
        public virtual ActionResult LogJs([JsExceptionModelBinder]JavaScriptException jsException)
        {           
            ErrorSignal signal = ErrorSignal.FromCurrentContext();

            signal.Raise(jsException);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }        
	}

}