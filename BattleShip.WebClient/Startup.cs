﻿using Microsoft.Owin;
using Owin;


[assembly: OwinStartupAttribute(typeof(BattleShip.WebClient.Startup))]
namespace BattleShip.WebClient
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureSignalR(app);
        }
    }
}