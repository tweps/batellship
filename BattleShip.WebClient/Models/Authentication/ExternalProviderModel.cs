﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BattleShip.WebClient.Models.Authentication
{
    public class ExternalProviderModel
    {
        public string AuthenticationType { get; set; }
        public string Caption { get; set; }
        public string Url { get; set; }
    }
}