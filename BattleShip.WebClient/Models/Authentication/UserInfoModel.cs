﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BattleShip.WebClient.Models.Authentication
{
    public class UserInfoModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}