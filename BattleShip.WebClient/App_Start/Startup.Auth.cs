﻿using System;
using System.Collections.Generic;
using System.Linq;
using BattleShip.WebClient.Authentication;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;


namespace BattleShip.WebClient
{
    public partial class Startup
    {
        public static readonly string PublicClientId = "self";
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        static Startup()
        {                    
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/token"),
                Provider = new BattleShipGuestOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/ApiAuthentication/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                AllowInsecureHttp = true
            };
            
        }
        
        public void ConfigureAuth(IAppBuilder app)
        {          
            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            //app.UseCookieAuthentication(new CookieAuthenticationOptions
            //{
            //    AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
            //    AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Active,
            //    // LoginPath = null // When login path is not set, then id does not to redirection, unathorized is returned
            //    CookieSecure = CookieSecureOption.SameAsRequest  // for production .Always
            //});

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);

            // Uncomment the following lines to enable logging in with third party login providers
            /*
            app.UseMicrosoftAccountAuthentication(
                clientId: "000000004C11149E",
                clientSecret: "txynHKgONIjmhoEbQ2MItwc8HX5nzuf9");
            */
            //app.UseTwitterAuthentication(
            //    consumerKey: "",
            //    consumerSecret: "");

            app.UseFacebookAuthentication(
                appId: "1416111878639222",
                appSecret: "c56b0cc29e38fbab37f09ac6bb206052" );

            app.UseGoogleAuthentication();
        }
    }
}