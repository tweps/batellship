﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using BattleShip.Domain;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;

namespace BattleShip.WebClient
{
    public partial class Startup
    {
        public void ConfigureSignalR(IAppBuilder app)
        {
            var hubConfig = new HubConfiguration()
            {
                EnableDetailedErrors = false,
            };

            app.MapSignalR(hubConfig);
        }
        
    }
}
