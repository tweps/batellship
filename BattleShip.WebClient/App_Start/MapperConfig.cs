﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using System.Web.Mvc;

namespace BattleShip.WebClient
{
    public static class MapperConfig
    {
        public static void Initialize(IUnityContainer container)
        {
            Mapper.Initialize( m => {

                m.ConstructServicesUsing(type => container.Resolve(type));

                m.AddProfile<BattleShip.WebClient.Areas.GameHub.Mapping.GameHubMappingProfile>();
                m.AddProfile<BattleShip.WebClient.Areas.Game.Mapping.GameAreaMappingProfile>();
            });

			Mapper.AssertConfigurationIsValid();
        }        
    }
}