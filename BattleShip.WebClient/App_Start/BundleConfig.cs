﻿using System.Web;
using System.Web.Optimization;

namespace BattleShip.WebClient
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootmetro/css/bootmetro.css",
                      "~/Content/bootmetro/css/bootmetro-responsive.css",
                      "~/Content/bootmetro/css/bootmetro-icons.css",
                      "~/Content/bootmetro/css/bootmetro-ui-light.css",
                      "~/Content/bootmetro/css/datepicker.css",
                      "~/Content/BattleShip.css"));

            bundles.Add(new ScriptBundle("~/bundles/extLib").Include(
                        
                        "~/Scripts/knockout-{version}.js",
                        "~/Scripts/knockout.mapping-latest.js",
                        "~/Scripts/sammy-{version}.js",
                        "~/Scripts/amplify.js",
                        "~/Scripts/jquery.signalR-{version}.js"                                                              
            ));

            bundles.Add(new ScriptBundle("~/bundles/battleShip").Include(

                        // Common                        
                        "~/Scripts/battleShip/errors/appExceptionHandler.js",
                        "~/Scripts/battleShip/errors/trace.js",

                        "~/Scripts/battleShip/extensions/knockout.extension.js",
                        "~/Scripts/battleShip/extensions/amplify.extension.js",
                                                
                        "~/Scripts/battleShip/configs/viewIds.js",
                        "~/Scripts/battleShip/configs/url.js",
                        "~/Scripts/battleShip/configs/config-route.js",
                        "~/Scripts/battleShip/events/messages.js",
                        "~/Scripts/battleShip/events/messenger.js",

                        // Data
                        "~/Scripts/battleShip/data/security.js",

                        "~/Scripts/battleShip/data/services/service.game.js",
                        "~/Scripts/battleShip/data/services/service.portalhub.js",

                        "~/Scripts/battleShip/data/services/service.initSignalR.js",
                        "~/Scripts/battleShip/data/services/service.gameSignalR.js",
                        "~/Scripts/battleShip/data/services/service.portalhubSignalR.js",
                        "~/Scripts/battleShip/data/services/service.authentication.js",


                        "~/Scripts/battleShip/data/datacontext.js",

                        // Main window
                        "~/Scripts/battleShip/main/navigator.js",
                        "~/Scripts/battleShip/main/presenter.js",
                        "~/Scripts/battleShip/main/viewModelLocator.js",
                        "~/Scripts/battleShip/main/binder.js",
                        "~/Scripts/battleShip/main/route.js",
                        

                        // Game
                        "~/Scripts/battleShip/areas/game/configs/config-route.js",
                        "~/Scripts/battleShip/areas/game/configs/config-viewModelLocator.js",
                                               
                        "~/Scripts/battleShip/areas/game/models/model.js",
                        "~/Scripts/battleShip/areas/game/models/model.gameInfo.js",
                        "~/Scripts/battleShip/areas/game/models/model.seaMapPiece.js",
                        "~/Scripts/battleShip/areas/game/models/model.seaMap.js",
                        "~/Scripts/battleShip/areas/game/models/model.shipSelector.js",
                        "~/Scripts/battleShip/areas/game/models/model.shipPositions.js",
                        "~/Scripts/battleShip/areas/game/models/model.game.js",

                        "~/Scripts/battleShip/areas/game/mapper.js",

                        "~/Scripts/battleShip/areas/game/presenter.js",

                        "~/Scripts/battleShip/areas/game/viewModels/viewModel.planning.js",
                        "~/Scripts/battleShip/areas/game/viewModels/viewModel.shooting.js",
                        

                        // Portal
                        "~/Scripts/battleShip/areas/portal/configs/config-route.js",
                        "~/Scripts/battleShip/areas/portal/configs/config-viewModelLocator.js",

                        "~/Scripts/battleShip/areas/portal/model.js",

                        "~/Scripts/battleShip/areas/portal/maper.js",

                        "~/Scripts/battleShip/areas/portal/presenter.js",

                        "~/Scripts/battleShip/areas/portal/viewModels/viewmodel.gameSelector.js",
                        "~/Scripts/battleShip/areas/portal/viewModels/viewModel.gameMenu.js",
                        "~/Scripts/battleShip/areas/portal/viewModels/viewModel.accountInfo.js",
                        "~/Scripts/battleShip/areas/portal/viewModels/viewModel.signIn.js",
                                                                       
                        // Startup
                        "~/Scripts/battleShip/bootstraper.js"

                        ));

        }
    }
}
