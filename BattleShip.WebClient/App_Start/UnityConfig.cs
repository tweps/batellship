using System.Web.Http;
using System.Web.Mvc;
using BattleShip.WebClient.Areas.Game.Hubs;
using BattleShip.WebClient.IoC;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Practices.Unity;
using Unity.Mvc4;

namespace BattleShip.WebClient
{
    public static class UnityConfig
    {
        public static IUnityContainer Init()
        {
            var container = BuildUnityContainer();

            // MVC
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            // MVC Web.API
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);

            // SignalR
            var hubActivaotr = new UnitySignalRHubActivator(container);
            GlobalHost.DependencyResolver.Register(typeof (IHubActivator), () => hubActivaotr);

            // AutoMapper
            container.RegisterType<AutoMapper.IMappingEngine>(new InjectionFactory(_ => AutoMapper.Mapper.Engine));

            return container;
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            RegisterTypes(container);

            return container;
        }

        private class UnitySignalRHubActivator : IHubActivator
        {
            private readonly IUnityContainer _container;

            public UnitySignalRHubActivator(IUnityContainer container)
            {
                this._container = container;
            }

            public IHub Create(HubDescriptor descriptor)
            {
                return (IHub)_container.Resolve(descriptor.HubType);
            }
        }

        private static void RegisterTypes(IUnityContainer container)
        {
            UnityTypesRegister.RegisterDomain(container);

            // Web
            container.RegisterType<BattleShipHub>(new ContainerControlledLifetimeManager());
        }        
    }
}