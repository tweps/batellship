﻿using BattleShip.WebExtensions.Seo;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BattleShip.WebClient
{
    public class ModelBinderConfig
    {
        public static void Register(ModelBinderProviderCollection binderProviders, ModelBinderDictionary binders, IUnityContainer container)
        {
            binderProviders.Add(new BattleShip.WebClient.Areas.Game.ModelBinders.GameModelBinderProvider(container));


            binders.Add(typeof(HashBang), new HashBangBinder());
        }
    }
}