﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace BattleShip.WebClient
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));            

            // config.Filters.Add(new BattleShip.WebClient.Extensions.Elmah.ElmahErrorAttribute());

            /*
            
            // Use camel case for JSON data.
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                           
            config.EnableCors(new EnableCorsAttribute(
                origins: "*", headers: "*", methods: "*"));
            */


            // Web API routes
           config.MapHttpAttributeRoutes();           
        }
    }
}
