﻿var battleShip = battleShip || {};
battleShip.errors = battleShip.errors || {};

battleShip.errors = (function ($) {

    var onError = function (exception, url, line) {
           
        var stackTrace = "";

        try
        {
            var stackTraceArray = arguments.callee.trace();
            delete stackTraceArray[0];
            stackTrace = stackTraceArray.join("\n");                        
        }
        catch (e) {
            stackTrace = "?";
        }
        
        var msg = "exception: " + exception + "\nurl: " + url + "\nline: " + line + "\n" + "stack trace: \s " + stackTrace;

        var xhr = new XMLHttpRequest();
        xhr.open('POST', '/Error/LogJs', true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(msg);
       

        return true; //in debug mode = false
    };

    var showError = function (failReason) {

        try {

            var presenter = battleShip.main.presenter;
            if (presenter != null && presenter != 'undefined') {

                presenter.showErrorPopup(failReason)
            }
        }
        catch (e) {

        }
    };


    return {

        onGlobalError: onError,
        showError: showError,
    }

})(jQuery);

// Init
window.onerror = battleShip.errors.onGlobalError;