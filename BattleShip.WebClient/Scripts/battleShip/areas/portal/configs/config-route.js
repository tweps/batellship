﻿var battleShip = battleShip || {};
battleShip.portal = battleShip.portal || {};
battleShip.portal.config = battleShip.portal.config || {};

battleShip.portal.config.route = (function (ko, viewIds, url) {

    function getRouteData() {

        var routeData = [

            // Portal
            // - game room
            {
                routeType: 'get',
                routePath: url.gamesRoom, // url pattern:  '#!/game/main'
                getViewId: function () {
                    return viewIds.portal.gameSelector;  // 'portalGameSelectorView'
                },
                //#region Others parameters ...
                routeTarget: function () {
                    return "/portal/gameselector";
                },
                routeTargetParams: function () { return null; },
                //#endregion 
            },
            // SignIn
            {
                routeType: 'get',
                routePath: url.unauthorized, // url pattern: '!#/login'
                routeTarget: function () {
                    return "/portal/PlayerAccount/SignIn";
                },
                routeTargetParams: function () { return null; },
                getViewId: function () {
                    return viewIds.portal.signIn;
                },
            },
            // CMS
            {
                routeType: 'get',
                routePath: url.cmsPage,
                routeTarget: function (routeParams) {
                    return "/portal/cms/index/" + routeParams["pageName"];
                },
                routeTargetParams: function () { return null; },
                getViewId: function(routeParams) {
                     return viewIds.portal.cmsPage + "_" + routeParams["pageName"];
                },
            }
        ];

        return routeData;

    }

    return {

        getRouteData: getRouteData,
                
    };

})(ko, battleShip.config.viewIds, battleShip.config.url);