﻿var battleShip = battleShip || {};
battleShip.portal = battleShip.portal || {};
battleShip.portal.config = battleShip.portal.config || {};

battleShip.portal.config.viewModelLocator = (function () {

    function getCfg() {

        var data = [

            {
                ViewName: 'Portal_GameSelector',
                ViewModelCreator: function() {
                     return new battleShip.portal.viewModel.gameSelector();
                }                 
            },
            {
                ViewName: 'Portal_GameMenu',
                ViewModelCreator: function() {
                     return new battleShip.portal.viewModel.gameMenu();
                }
            },
            {
                ViewName: 'Portal_AccountInfo',
                ViewModelCreator: function() {
                     return new battleShip.portal.viewModel.accountInfo();
                }
            },
            {
                ViewName: 'Portal_SignIn',
                ViewModelCreator: function() {
                    return new battleShip.portal.viewModel.signIn();
                }
            }
        ];

        return data;

    };


    return {
        getCfg: getCfg        
    };

})();