﻿var battleShip = battleShip || {};
battleShip.portal = battleShip.portal || {};

battleShip.portal.presenter = (function ($, mainPresenter) {

    // const
    var NotStarted = 0,
        ShipsArrangement = 1,
        WaitingForOpponent = 2,
        PlayerTurn = 3,
        OpponentTurn = 4,
        PlayerHasWinTheMatch = 5,
        PlayerHasLoseTheMatch = 6;

    var getGameMenuTileClass = function (status, isActive) {

        var tileClass = "tile square text ";
        switch (status) {
            case NotStarted:
            case PlayerHasWinTheMatch:
            case PlayerHasLoseTheMatch:
            case WaitingForOpponent:
                tileClass += "bg-color-grayLight";
                break;

            case ShipsArrangement:
            case PlayerTurn:
                tileClass += "bg-color-red";
                break;

            case OpponentTurn:
                tileClass += "bg-color-greenDark";
                break;

            default:
                tileClass += "bg-color-blue";
        }

        if (isActive) {
            tileClass += " tileActive";
        }

        return tileClass;
    };

    var getGameSelectorTileClass = function (isActive) {

        var tileClass = "tile square text ";
        if (isActive) {
            tileClass += "bg-color-purple";
        } else {
            tileClass += "bg-color-gray";
        }

        return tileClass;
    };


    var showTileElementV = function (elem) {
        
        var $tile = $('.tile', elem);
        $tile.one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
            $tile.removeClass('tileAnimationSetInV');
        });
        $tile.addClass('tileAnimationSetInV');        
    };

    var hideTileElementV = function(elem) {

        var $tile = $('.tile', elem);                        
        $tile.one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
            $(elem).remove();
        });        
        $tile.addClass('tileAnimationSetOutV');        
    };

    var showTileElementH = function (elem) {

        var $tile = $(elem);
        $tile.one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
            $tile.removeClass('tileAnimationSetInH');
        });
        $tile.addClass('tileAnimationSetInH');
    };

    var hideTileElementH = function (elem) {

        var $tile = $(elem);
        $tile.one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
            $(elem).remove();
        });
        $tile.addClass('tileAnimationSetOutH');
    };


    var showPopup = function (title, bodyHtml) {
        mainPresenter.showPopup(title, bodyHtml);
    };

    var showErrorPopup = function (failReason) {
        mainPresenter.showErrorPopup(failReason);
    };

    return {
        getGameMenuTileClass: getGameMenuTileClass,
        getGameSelectorTileClass: getGameSelectorTileClass,
        showTileElementV: showTileElementV,
        hideTileElementV: hideTileElementV,
        showTileElementH: showTileElementH,
        hideTileElementH: hideTileElementH,
        showPopup: showPopup,
        showErrorPopup: showErrorPopup,
    };

})(jQuery, battleShip.main.presenter);