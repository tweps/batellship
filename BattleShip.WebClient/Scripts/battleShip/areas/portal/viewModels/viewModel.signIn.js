﻿var battleShip = battleShip || {};
battleShip.portal = battleShip.portal || {};
battleShip.portal.viewModel = battleShip.portal.viewModel || {};


(function (ko, datacontext, url, navigator) {

    battleShip.portal.viewModel.signIn = function () {

        var self = this;

        // Public properties

        self.providers = new ko.observableArray();
        self.guestName = new ko.observable();

        // Private functions

        function refresh() {
            datacontext.portal.playerAccount.login.singInProviders(self.providers);
        }

        // Public functions

        self.playAsGuest = function () {            
            datacontext.portal.playerAccount.login.signInAsGuest(self.guestName())
                .done(function() {
                    navigator.navigateTo(url.gamesRoom);
                });            
        };

        self.init = function () {
            refresh();
        };

        self.activate = function () {

        };

        self.deactivate = function () {
            return true;
        };

    };

})(ko, battleShip.data.datacontext, battleShip.config.url, battleShip.main.navigator);