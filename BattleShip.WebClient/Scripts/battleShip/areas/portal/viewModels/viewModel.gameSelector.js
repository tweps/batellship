﻿var battleShip = battleShip || {};
battleShip.portal = battleShip.portal || {};
battleShip.portal.viewModel = battleShip.portal.viewModel || {};

(function (ko, datacontext, events, presenter, navigator) {

    battleShip.portal.viewModel.gameSelector = function () {

        // Check if player can join to this game         
        var isGameActive = function (gameItem) {

            return gameItem.playerOneId() != playerId && gameItem.playerTwoId() != playerId;
        };


        // Public Properties
        var openGamesList = ko.observableArray();

        var searchInput = ko.observable();

        var noFreeToJoinInfo = ko.computed(function() {

            var onlyActive = openGamesList().filter(isGameActive);
            return onlyActive.length == 0;
        });

        // Commands:

        // Join To Game
        var onGameItemClick = function () {

            var gameItem = this;
            if (isGameActive(gameItem)) {
                $.when(datacontext.portal.playerGames.join(gameItem.gameId))
                    .done(function(item) {
                        refresh(false);
                        events.messenger.publish(events.messages.playerHasNewGame);
                        navigator.navigateToGame(item.playerContextId());
                    });

            } else {
                presenter.showPopup('Warring', 'You can not play with yourself !');
            }

            return false;
        };

        // Sort items by Date
        var sortByDateCommand = function () {
            dataContextParams.sort = sortByDate;
            refresh(false);
        };

        // Sort items by Name
        var sortByNameCommand = function () {

            dataContextParams.sort = sortByName;
            refresh(false);
        };

        // Creates new Game
        var startNewGameCommand = function () {
           
            $.when(datacontext.portal.playerGames.create(dataContextParams))
            .done(function(gameItem) {
                refresh(true);
                events.messenger.publish(events.messages.playerHasNewGame);
                navigator.navigateToGame(gameItem.playerContextId());
            });                                              
        };


        // Private

        var playerId = null;

        // Sort Comparator
        var sortByDate = function (a, b) {
            return a.created() < b.created();
        };

        // Sort Comparator
        var sortByName = function (a, b) {
            return a.playerOneName().localeCompare(b.playerOneName());
        };

        // Filtering
        var updateFilter = function (filetrValue) {

            if (filetrValue == '') {

                dataContextParams.filter = null;
            }
            else {

                var regex = new RegExp('^' + filetrValue, 'i')
                dataContextParams.filter = function (element) {

                    return regex.test(element.playerOneName());
                };
            }

            refresh(false);
        };

        var dataContextParams = function (forceRefresh) {

            return {
                results: openGamesList,
                forceRefresh: forceRefresh || false,
                filter: null,
                sort: sortByDate,
            };
        }(false);

        //Public 

        
        var refresh = function (forceRefresh) {

            dataContextParams.forceRefresh = forceRefresh;
            return datacontext.portal.publicGames.get(dataContextParams);
        };

        // Called when Routing set this VM as active
        var activate = function (routeData, callback) {

            $.when(datacontext.portal.playerAccount.get())
            .done(function (playerAccount) {
                    playerId = playerAccount.id();
            });
            

            $.when(refresh(true))
            .done(function () {
                datacontext.portal.publicGames.subscribeSync(dataContextParams);
            });
        };

        // Called when Routing deactivates this VM
        var deactivate = function () {

            var close = false;
            if (close) {
                datacontext.portal.publicGames.unsubscribeSync(dataContextParams);
            }

            return close;
        };


        var init = function (params) {

            searchInput.subscribe(updateFilter);
        };
       
        return {
            // Properties                       
            openGamesList: openGamesList,
            searchInput: searchInput,
            noFreeToJoinInfo: noFreeToJoinInfo,

            // Commands
            startNewGameCommand: startNewGameCommand,
            onGameItemClick: onGameItemClick,
            sortByDateCommand: sortByDateCommand,
            sortByNameCommand: sortByNameCommand,

            // Methods
            isGameActive: isGameActive,
            activate: activate,
            init: init,
            deactivate: deactivate,
        };

    };   
})(ko, battleShip.data.datacontext, battleShip.events, battleShip.portal.presenter, battleShip.main.navigator);



