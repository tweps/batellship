﻿var battleShip = battleShip || {};
battleShip.portal = battleShip.portal || {};
battleShip.portal.viewModel = battleShip.portal.viewModel || {};


(function (ko, model, datacontext, events) {

    battleShip.portal.viewModel.accountInfo = function () {

        var self = this;
        self.user = new model.playerAccount();
        self.isSignedIn = ko.computed(function () {
            return self.user.name() !== '';
        });

        function refresh() {
            datacontext.portal.playerAccount.get(self.user);                
        }

        self.signOut = function() {            
            $.when(datacontext.portal.playerAccount.login.signOut())
            .done(refresh);

            return false;
        };

        self.init = function () {
            events.messenger.subscribe(events.messages.signIn, refresh);
            events.messenger.subscribe(events.messages.signOut, refresh);
            refresh();
        };

        self.activate = function () {

        };

        self.deactivate = function () {
            return false;
        };

    };

})(ko, battleShip.portal.model, battleShip.data.datacontext, battleShip.events);