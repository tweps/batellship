﻿var battleShip = battleShip || {};
battleShip.portal = battleShip.portal || {};
battleShip.portal.viewModel = battleShip.portal.viewModel || {};

(function (ko, datacontext, events, url, security) {

    battleShip.portal.viewModel.gameMenu = function () {

        var visible = ko.observable(false);
        
        var playerGamesList = ko.observableArray();

        var gameRoomUrl = ko.observable(url.gamesRoom);

        var gamesRoomIsActive = ko.observable();

        var sortByDate = function (a, b) {

            return a.created() < b.created();
        };
        
        var playerGamesParams = function (forceRefresh) {

            this.results = playerGamesList;
            this.forceRefresh = forceRefresh || false;
            this.filter = null;
            this.sort = sortByDate;
        };        

        var refresh = function (forceRefresh) {
            datacontext.portal.playerGames.get(new playerGamesParams(forceRefresh));
        };

        var onBattelFinished = function (params) {

            var playerContextid = params[1];
            var toRemove = playerGamesList().filter(function (item) {
                return item.playerContextId() == playerContextid;
            });

            for (var i in toRemove) {
                var item = toRemove[i];
                var index = playerGamesList().indexOf(item);
                if (index > -1) {
                    playerGamesList.splice(index, 1);
                }
            }

            
        };

        var onActiveWindowChanged = function (args) {
            gamesRoomIsActive("/" + gameRoomUrl() == args[1]);
        }

        var activate = function (routeData, callback) {
            refresh(false);
        };

        var init = function (params) {
            
            events.messenger.subscribe(events.messages.playerHasNewGame, refresh);
            events.messenger.subscribe(events.messages.signIn, function () {
                visible(true);
                refresh(false);
            });
            events.messenger.subscribe(events.messages.signOut, function () {
                visible(false);
                refresh(false);
            });
            events.messenger.subscribe(events.messages.battelFinished, onBattelFinished);   
            events.messenger.subscribe(events.messages.activeWindowChanged, onActiveWindowChanged);

            if (security.isUserAuthenticated()) {
                visible(true);
                refresh(true);
            }
        };

        var deactivate = function () {

            return false;
        };
               

        // public
        return {
            // Properties
            playerGamesList: playerGamesList,            
            gamesRoomIsActive: gamesRoomIsActive,
            gameRoomUrl: gameRoomUrl,
            visible: visible,

            // Methods
            init: init,
            activate: activate,
            deactivate: deactivate,
        };        
    };
})(ko, battleShip.data.datacontext, battleShip.events, battleShip.config.url, battleShip.data.security);
