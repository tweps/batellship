﻿var battleShip = battleShip || {};
battleShip.mappers = battleShip.mappers || {};

battleShip.mappers.portalMapper = (function (model) {

    function dtoToPlayerAccount(dto, playerAccount) {

        playerAccount = playerAccount || new model.playerAccount();

        if (typeof dto.Name != 'undefined' && typeof dto.Id != 'undefined') {
            playerAccount.id(dto.Id);
            playerAccount.name(dto.Name);
        } else {
            playerAccount.id('');
            playerAccount.name('');
        }
        return playerAccount;
    };

    function dtoToExternalProvider(dto, externalProvider) {

        externalProvider = externalProvider || new model.externalProvider();
        externalProvider.authenticationType = dto.AuthenticationType;
        externalProvider.caption = dto.Caption;
        externalProvider.url = dto.Url;

        return externalProvider;
    }

    function dtoToGameItem(dto, gameItem) {

        gameItem.gameName(dto.gameName);
        gameItem.description(dto.description);
        gameItem.playerOneName(dto.playerOneName);
        gameItem.playerTwoName(dto.playerTwoName);
        gameItem.playerOneId(dto.playerOneId);
        gameItem.playerTwoId(dto.playerTwoId);
        gameItem.created(dto.created);
    };

    function dtoToPlayerGameItem(dto, playerGameItem) {

        playerGameItem = playerGameItem || new model.playerGameItem();        
        playerGameItem.playerContextId(dto.playerContextId);
        playerGameItem.status(dto.status);

        dtoToGameItem(dto, playerGameItem);

        return playerGameItem;
    };

    function dtoToOpenGameItem(dto, openGameItem) {

        openGameItem = openGameItem || new model.openGameItem();
        openGameItem.gameId = dto.gameId;
        dtoToGameItem(dto, openGameItem);

        return openGameItem;
    };


    return {

        playerAccount: {
            fromDto: dtoToPlayerAccount,
            getDtoId: function(dto) {
                return dto.Id;
            }
        },

        externalProvider: {
            fromDto: dtoToExternalProvider,
        },

        playerGame: {
            fromDto: dtoToPlayerGameItem,
            getDtoId: function (dto) {
                return dto.playerContextId;
            }
        },

        publicGame: {
            fromDto: dtoToOpenGameItem,
            getDtoId: function (dto) {
                return dto.gameId;
            }
        },
       
    };

})(battleShip.portal.model);