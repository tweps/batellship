﻿var battleShip = battleShip || {};
battleShip.portal = battleShip.portal || {};


battleShip.portal.model = (function ($, ko, events, url) {
        
    function playerAccount (name, id) {

        var self = this;
        self.name = ko.observable(name);
        self.id = ko.observable(id);
    };

    function externalProvider(authenticationType, caption, providerUrl) {

        var self = this;
        self.authenticationType = ko.observable(authenticationType);
        self.caption = ko.observable(caption);
        self.url = ko.observable(providerUrl);
    };

    function gameItem() {
        var self = this;

        self.gameName = ko.observable();
        self.description = ko.observable();
        self.playerOneName = ko.observable();        
        self.playerTwoName = ko.observable();
        self.playerOneId = ko.observable();
        self.playerTwoId = ko.observable();
        self.created = ko.observable();
    };

    function playerGameItem() {        
        gameItem.apply(this);
        
        var self = this;
        self.playerContextId = ko.observable();
        self.status = ko.observable();

        self.isActive = ko.observable(false);

        self.onGameItemClick = function (datacontext) {
            return self.playerContextId;
        };

        
        self.url = ko.computed(function () {
            return url.game.replace(":id", self.playerContextId());
        });

        events.messenger.subscribe(events.messages.activeWindowChanged, function (args) {
            self.isActive("/" + self.url() == args[1]);
        });

    };
    
    playerGameItem.prototype = Object.create(gameItem.prototype);     
    playerGameItem.prototype.constructor = playerGameItem;

    function openGameItem() {
        gameItem.apply(this);

        var self = this;
        self.gameId = null;        
    };

    openGameItem.prototype = Object.create(gameItem.prototype);
    openGameItem.prototype.constructor = openGameItem;
   
    return {
        playerAccount: playerAccount,
        externalProvider: externalProvider,
        playerGameItem: playerGameItem,
        openGameItem: openGameItem,
    };


})(jQuery,ko,battleShip.events, battleShip.config.url);

 