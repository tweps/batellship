﻿var battleShip = battleShip || {};
battleShip.mappers = battleShip.mappers || {};

battleShip.mappers.gameMapper = (function (ko, model) {

    function dtoToShipSelector(dtoArray, shipSelector) {

        shipSelector = shipSelector || new model.shipSelector();
        
        ko.mapping.fromJS(dtoArray)().forEach(function (d) {

            d.isSelected = ko.computed(function () {
                return shipSelector.selectedShipDefinition() === d;
            });

            shipSelector.definitions.push(d);
        });

        return shipSelector;
    };

    function dtoToSeaMap(dtoSeaMap, seaMap) {

        seaMap = seaMap || new model.seaMap();

        for (var x = 0; x < seaMap.pieces.length; x++) {
            for (var y = 0; y < seaMap.pieces[x].length; y++) {
                var state = model.boardPieceState.getByValue(dtoSeaMap.Pieces[x][y]);
                seaMap.pieces[x][y].state(state);
            }
        }

        if (dtoSeaMap.LastShoot != null) {
            seaMap.lastShoot(seaMap.pieces[dtoSeaMap.LastShoot.Item1][dtoSeaMap.LastShoot.Item2]);
        } else {
            seaMap.lastShoot(null);
        }

        return seaMap;
    };

    function dtoToGameInfo(dto, gameInfo, isSync) {

        var gameInfo = gameInfo || new model.gameInfo();

        gameInfo.status(dto.Status);
        gameInfo.gameTitle(dto.GameTitle);

        return gameInfo;
    }

    function dtoToGame(dto, game, isSync) {

        game.gameInfo = dtoToGameInfo(dto, game.gameInfo);

        if (typeof dto.PlayerSeaMap != 'undefined' && canBeUpdated(game.playerSeaMap, isSync)) {
            game.playerSeaMap = dtoToSeaMap(dto.PlayerSeaMap, game.playerSeaMap);
        }

        if (typeof dto.OpponentSeaMap != 'undefined' && canBeUpdated(game.opponentSeaMap, isSync)) {
            game.opponentSeaMap = dtoToSeaMap(dto.OpponentSeaMap, game.opponentSeaMap);
        }

        if (typeof dto.ShipsDefinitions != 'undefined' && canBeUpdated(game.shipSelector, isSync)) {
            game.shipDefinitions = dtoToShipSelector(dto.ShipsDefinitions, game.shipSelector);
        }

        return game;
    };

    function canBeUpdated(item, isSync) {

        if (isSync && item != null && item != 'undefined' && item.isModified != 'undefined') {
            // in sync (signalR) and data are modifed, then to not update
            return !item.isModified();
        }

        return true;
    }
    
    function getPlayerContextId(dto) {

        return dto.PlayerContextId;
    }


    return {

        game: {

            fromDto: dtoToGame,
            getDtoId: getPlayerContextId,
        },
    };


})(ko, battleShip.game.model);