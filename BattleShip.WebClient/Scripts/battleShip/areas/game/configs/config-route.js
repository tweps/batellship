﻿var battleShip = battleShip || {};
battleShip.game = battleShip.game || {};
battleShip.game.config = battleShip.game.config || {};

battleShip.game.config.route = (function (ko, viewIds, url) {


    function getRouteData() {

        var routeData = [

            {
                routeType: 'get',
                routePath: url.game,
                routeTarget: function (routeParams) {
                    return "/game/" + routeParams["id"]
                },
                routeTargetParams: function () { return null; },
                getViewId: function (routeParams) {
                    return viewIds.game + "_" + routeParams["id"];
                },
            },

            {
                routeType: 'post',
                routePath: url.submitShipPlacement,
                routeTarget: function (routeParams) {
                    return "/game/planning/submitShipPlacement/" + routeParams["id"]
                },
                routeTargetParams: function (routeParams) {
                    
                    var data = {
                        'id': routeParams["id"],
                        'ships': routeParams["ships"],
                    };

                    return data;
                },
                getViewId: function (routeParams) {
                    return viewIds.game + "_" + routeParams["id"];
                },
            }

        ];

        return routeData;
    }

    return {
        getRouteData: getRouteData,
    };

})(ko, battleShip.config.viewIds, battleShip.config.url);