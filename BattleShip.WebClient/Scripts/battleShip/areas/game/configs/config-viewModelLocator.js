﻿var battleShip = battleShip || {};
battleShip.game = battleShip.game || {};
battleShip.game.config = battleShip.game.config || {};

battleShip.game.config.viewModelLocator = (function () {

    function getCfg() {

        var data = [

            {
                ViewName: 'Game_Shooting',
                ViewModelCreator: function () { return new battleShip.game.viewModel.shootingViewModel(); }
            },
            {
                ViewName: 'Game_Planning',
                ViewModelCreator: function () { return new battleShip.game.viewModel.planningViewModel(); }
            }
        ];

        return data;

    };


    return {
        getCfg: getCfg
    };

})();