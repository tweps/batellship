﻿var battleShip = battleShip || {};
battleShip.game = battleShip.game || {};
battleShip.game.viewModel = battleShip.game.viewModel || {};

(function ($, model, datacontext, presenter, events) {

    battleShip.game.viewModel.shootingViewModel = function () {

        // private
        var self = this;
        var game = null;
        var playerContextId = null;

        var highlightedPieces = new Array();

        function checkIfGameFinished(status) {
            return model.gameStatus.PlayerHasLoseTheMatch == status || model.gameStatus.PlayerHasWinTheMatch == status;
        };

        function refreshHighlightedPieces(selected) {
            var highlighting, piece;
            
            // reset highlighting
            highlighting = model.boardPieceHighlighting.normal;
            for(var i=0; i<highlightedPieces.length; i++){                
                 self.opponentSeaMap.pieces[highlightedPieces[i].x][highlightedPieces[i].y].highlighting(highlighting);                
            };
            highlightedPieces.length = 0;

            var lastShootPiece = self.opponentSeaMap.lastShoot();
            if (lastShootPiece != null) {
                lastShootPiece.highlighting(model.boardPieceHighlighting.info);
            };

            if (selected != null && self.shootIsAllowed()) {
                highlighting = model.boardPieceHighlighting.warning;
                piece = self.opponentSeaMap.pieces[selected.x][selected.y];
                piece.highlighting(highlighting);

                highlightedPieces.push(piece);
            };
        };

        function lastShootBeforeChange(piece) {
            if (piece != null) {
                piece.highlighting(model.boardPieceHighlighting.normal);
            }
        };

        function lastShootAfterChange(piece) {
            if (piece != null) {
                piece.highlighting(model.boardPieceHighlighting.info);
            }
        };

        // public

        self.gameInfo = new model.gameInfo();

        self.playerSeaMap = new model.seaMap();

        self.opponentSeaMap = new model.seaMap();

        self.shootIsAllowed = ko.computed(function () {
            return self.gameInfo.status() == model.gameStatus.PlayerTurn;
        });       


        // subscriptions

        self.shootIsAllowed.subscribe(function () {
            refreshHighlightedPieces(self.playerSeaMap.selectedPiece());
        });

        self.playerSeaMap.lastShoot.subscribe(lastShootAfterChange);
        self.playerSeaMap.lastShoot.subscribe(lastShootBeforeChange, null, "beforeChange");
        self.opponentSeaMap.lastShoot.subscribe(lastShootAfterChange);
        self.opponentSeaMap.lastShoot.subscribe(lastShootBeforeChange, null, "beforeChange");


        self.opponentSeaMap.selectedPiece.subscribe(function (selected) {
            refreshHighlightedPieces(selected);
        });

        self.opponentSeaMap.subscriberClick.subscribe(function (seaPiece) {

            if (seaPiece!=null && self.shootIsAllowed()) {

                self.gameInfo.status(model.gameStatus.WaitingForServer);

                datacontext.game.shooting.shoot(game, seaPiece.x, seaPiece.y);
            }
        });

        
        // Methods

        self.init = function (initParams) {

            playerContextId = initParams["id"];
            game = new model.game(playerContextId, self.gameInfo, self.playerSeaMap, self.opponentSeaMap, null);

            datacontext.game.shooting.get(game);
        };

        self.activate = function() {
            
        };

        self.deactivate = function () {

            // if game end , then remove view.
            if (checkIfGameFinished(self.gameInfo.status())) {

                datacontext.game.shooting.archive(game);
                events.messenger.publish(events.messages.battelFinished, playerContextId);
                return true;
            }
            return false;
        };


    };
       
})(jQuery, battleShip.game.model, battleShip.data.datacontext, battleShip.game.presenter, battleShip.events);

