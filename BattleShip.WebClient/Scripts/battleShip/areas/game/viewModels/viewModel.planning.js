﻿var battleShip = battleShip || {};
battleShip.game = battleShip.game || {};
battleShip.game.viewModel = battleShip.game.viewModel || {};

(function ($, model, datacontext, url) {
        
    battleShip.game.viewModel.planningViewModel = function () {

        // private 
        var self = this;
        var game = null;
        var direction = null;
        var ships = new Array();
        var shipsPositions = new Array();
        var highlightedPieces = null;

        var searchShipIndex = function (x, y) {

            result = -1;

            ships.forEach(function (ship, index) {
                ship.masts.forEach(function (pos) {
                    if (pos.x === x && pos.y === y) {
                        result = index;
                    }
                })
            });

            return result;
        };

        function checkLimit(x, y) {

            if (typeof x == 'undefined' || typeof y == 'undefined')
                return false;

            if (x < 0 || x >= self.playerSeaMap.pieces.length)
                return false;

            if (y < 0 || y >= self.playerSeaMap.pieces[x].length)
                return false;

            return true;
        };

        function setBoardPieces(ship, setterFn) {

            ship.masts.forEach(function (mast) {

                if (checkLimit(mast.x, mast.y)) {
                    var piece = self.playerSeaMap.pieces[mast.x][mast.y];
                    if (piece != undefined) {
                        setterFn(piece);
                    }
                }
            });
        };

        function getMasts() {

            var shipDefinition = self.shipSelector.selectedShipDefinition();
            if (shipDefinition !== null && shipDefinition.AmountOfShips() > 0) {
                return shipDefinition.Masts();
            }

            return null;
        };

        function createShip(x, y, masts, direction) {

            if (masts != null && masts > 0) {

                var ship = new model.shipModel()
                for (var i = 0; i < masts; i++) {
                    ship.masts.push(new model.shipMastModel(x, y));

                    switch (direction) {
                        case 'North':
                            y--;
                            break;
                        case 'South':
                            y++;
                            break;
                        case 'East':
                            x++;
                            break;
                        case 'West':
                            x--;
                            break;
                    }
                }

                return ship;
            }

            return null;
        };

        function shipValidation(validatedShip) {

            var result = true;

            validatedShip.masts.forEach(function (validatedMast) {
                if (!checkLimit(validatedMast.x, validatedMast.y))
                    result = false;
            });

            if (result) {
                ships.forEach(function (ship) {
                    ship.masts.forEach(function (mast) {
                        validatedShip.masts.forEach(function (validatedMast) {
                            var distance = Math.sqrt(Math.pow((validatedMast.x - mast.x), 2) + Math.pow((validatedMast.y - mast.y), 2));
                            if (distance < 2) {
                                result = false;
                            }
                        });
                    });
                });
            }

            return result;
        };

        function refreshHighlightedPieces(selected) {

            if (highlightedPieces != null) {

                setBoardPieces(highlightedPieces, function (piece) {
                    piece.highlighting(model.boardPieceHighlighting.normal);
                });

                highlightedPieces = null;
            }

            if (selected != null) {

                var ship = null;
                var highlightingType = null;

                var index = searchShipIndex(selected.x, selected.y);
                if (index > -1) {

                    ship = ships[index];
                    highlightingType = model.boardPieceHighlighting.warning;

                } else {

                    var masts = getMasts();
                    ship = createShip(selected.x, selected.y, masts, direction);

                    if (ship == null) {
                        return;
                    }

                    if (shipValidation(ship)) {
                        highlightingType = model.boardPieceHighlighting.warning;
                    } else {
                        highlightingType = model.boardPieceHighlighting.error;
                    }

                }

                setBoardPieces(ship, function (piece) {
                    piece.highlighting(highlightingType);
                });

                highlightedPieces = ship;
            }
        };

        // public     
        self.gameInfo = new model.gameInfo();
        self.shipSelector = new model.shipSelector();
        self.playerSeaMap = new model.seaMap();

        self.playerSeaMap.subscriberClick.subscribe(function (seaPiece) {

            if (seaPiece == null)
                return;

            self.playerSeaMap.isModified(true);
            self.shipSelector.isModified(true);

            var shipHasBeenRemoved = false;

            var index = searchShipIndex(seaPiece.x, seaPiece.y);
            if (index > -1) {

                // remove ship if exists one in this location

                var ship = ships[index];

                ships = ships.slice(0, index).concat(ships.slice(index + 1, ships.length));
                shipsPositions = shipsPositions.slice(0, index).concat(shipsPositions.slice(index + 1, shipsPositions.length));

                setBoardPieces(ship, function (piece) {
                    piece.state(model.boardPieceState.unknown);
                });

                self.shipSelector.addShip(ship.masts.length);

            } else {

                // Add new ship

                var masts = getMasts();
                var ship = createShip(seaPiece.x, seaPiece.y, masts, direction);
                if (ship != null && shipValidation(ship)) {

                    ships.push(ship);
                    shipsPositions.push(new model.shipPositionModel(seaPiece.x, seaPiece.y, masts, direction));

                    setBoardPieces(ship, function (piece) {
                        if (piece != undefined) {
                            piece.state(model.boardPieceState.notHit);
                            piece.highlighting(model.boardPieceHighlighting.normal);
                        }
                    });
                    self.shipSelector.removeShip(masts);
                    self.playerSeaMap.selectPiece(null);
                }
            }

        });

        self.playerSeaMap.selectedPiece.subscribe(function (selected) {
            refreshHighlightedPieces(selected);
        });

        self.canStartGame = ko.computed(function () {

            var count = 0;
            ko.utils.arrayForEach(self.shipSelector.definitions(), function (def) {
                count += def.AmountOfShips();
            });

            return count === 0;
        });
        
        self.submitValue = ko.computed(function () {

            var value = null
            if (self.canStartGame()) {
                value = JSON.stringify(shipsPositions);
            }

            return value;
        });

        self.routingAction = ko.observable("");

        self.changeDirection = function () {

            switch (direction) {

                case "North":
                    next = "East";
                    break;
                case "East":
                    next = "South";
                    break;
                case "South":
                    next = "West";
                    break;
                case "West":
                default:
                    next = "North";
                    break;
            }

            direction = next;
            refreshHighlightedPieces(self.playerSeaMap.selectedPiece());
        };
 
        self.init = function (initParams) {

            var playerContextId = initParams["id"];
             
            self.routingAction(url.submitShipPlacement.replace(":id", playerContextId));

            game = new model.game(playerContextId, self.gameInfo, self.playerSeaMap, null, self.shipSelector);

            $.when(datacontext.game.planning.get(game))
            .done(function () {
                self.changeDirection();
                self.shipSelector.updateSelection();
            });

        };

        self.activate = function () {

        };

        self.deactivate = function () {

            return false;
        };

    };
                
   
})(jQuery, battleShip.game.model, battleShip.data.datacontext, battleShip.config.url);

