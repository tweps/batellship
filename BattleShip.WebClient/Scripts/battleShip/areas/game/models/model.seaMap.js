﻿var battleShip = battleShip || {};
battleShip.game = battleShip.game || {};
battleShip.game.model = battleShip.game.model || {};

(function (model, ko) {

    // private 

    function subscriber() {

        var self = this;

        var subscribers = [];

        self.subscribe = function (fn) {
            subscribers.push(fn);
        };

        self.unsubscribe = function (fn) {
            subscribers = subscribers.filter(function (el) {
                if (fn !== el) {
                    return el;
                }
            });
        };

        self.execute = function(src) {
            subscribers.forEach(function(el) {
                el(src);
            });
        };

    }

    // public

    battleShip.game.model.seaMap = function() {

        var self = this;
        

        var seaMapSize = 10;

        self.pieces = new Array(seaMapSize);
        for (var i = 0; i < self.pieces.length; i++) {
            self.pieces[i] = new Array(seaMapSize);
            for (j = 0; j < self.pieces[i].length; j++) {
                self.pieces[i][j] = new model.seaMapPiece(i, j);
                self.pieces[i][j].state(model.boardPieceState.unknown);
            }
        }

        self.isModified = ko.observable(false);

        self.selectedPiece = ko.observable(null);

        self.selectPiece = function() {
            self.selectedPiece(this);
        };

        self.deselectPiece = function() {
            self.selectedPiece(null);
        };

        self.subscriberClick = new subscriber();

        self.click = function() {
            var selectedPiece = self.selectedPiece();
            self.subscriberClick.execute(selectedPiece);
        };

        self.update = function(seaMapModel, highlightDifferences) {
            for (var x = 0; x < self.pieces.length; x++) {
                for (var y = 0; y < self.pieces[x].length; y++) {
                    var state = model.boardPieceState.getByValue(seaMapModel.Pieces[x][y]);

                    if (highlightDifferences) {

                        var highlight = null;
                        if (self.pieces[x][y].state().value == seaMapModel.Pieces[x][y])
                            highlight = BattleShipGame.boardPieceHighlighting.normal;
                        else
                            highlight = BattleShipGame.boardPieceHighlighting.warning;

                        self.pieces[x][y].highlighting(highlight);
                    }

                    self.pieces[x][y].state(state);
                }
            }
        };

        self.lastShoot = ko.observable();
    };

})(battleShip.game.model,ko);
