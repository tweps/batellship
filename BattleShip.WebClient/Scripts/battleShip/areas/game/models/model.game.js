﻿var battleShip = battleShip || {};
battleShip.game = battleShip.game || {};
battleShip.game.model = battleShip.game.model || {};


(function (ko, model) {

    model.game = function (playerContextId, gameInfo, playerSeaMap, opponentSeaMap, shipSelector) {

		var self = this;
		self.playerContextId = playerContextId;
		self.gameInfo = gameInfo;
		self.playerSeaMap = playerSeaMap;
		self.opponentSeaMap = opponentSeaMap;
		self.shipSelector = shipSelector;
	};

})(ko, battleShip.game.model);

