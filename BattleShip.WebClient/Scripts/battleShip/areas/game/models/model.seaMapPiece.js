﻿var battleShip = battleShip || {};
battleShip.game = battleShip.game || {};
battleShip.game.model = battleShip.game.model || {};


(function (model) {

    battleShip.game.model.seaMapPiece = function(x, y) {

        var self = this;

        self.x = x;
        self.y = y;        
        self.state = ko.observable(model.boardPieceState.unknown);
        self.highlighting = ko.observable(model.boardPieceHighlighting.normal);        
    };


})(battleShip.game.model);

