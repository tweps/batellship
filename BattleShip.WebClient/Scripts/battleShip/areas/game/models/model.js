﻿var battleShip = battleShip || {};
battleShip.game = battleShip.game || {};

battleShip.game.model = (function (model, $) {

    model.boardPieceState = {
        unknown: { value: 0, },
        mishit: { value: 1, },
        hit: { value: 2, },
        hitAndSunken: { value: 3, },
        notHit: { value: 4, },

        getByValue: function (value) {
            for (var boardState in model.boardPieceState) {
                if (typeof model.boardPieceState[boardState] != 'undefined' && model.boardPieceState[boardState].value === value) {
                    return model.boardPieceState[boardState];
                }
            }

            return null;
        }

    };
    
    model.boardPieceHighlighting = {
        normal: { value: 0, },
        warning: { value: 1, },
        error: { value: 2, },
        info: { value: 3 }
    };

    model.gameStatus = {
        NotStarted: 0,
        ShipsArrangement: 1,
        WaitingForOpponent: 2,
        PlayerTurn: 3,
        OpponentTurn: 4,
        PlayerHasWinTheMatch: 5,
        PlayerHasLoseTheMatch: 6,
        WaitingForServer: 99,
    };

    return model;
    
})(battleShip.game.model || {}, jQuery);