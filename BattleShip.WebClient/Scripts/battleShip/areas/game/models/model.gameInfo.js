﻿var battleShip = battleShip || {};
battleShip.game = battleShip.game || {};
battleShip.game.model = battleShip.game.model || {};


(function (ko, model) {

    battleShip.game.model.gameInfo = function () {

        var self = this;

        self.gameTitle = new ko.observable();
        self.status = new ko.observable(model.gameStatus.WaitingForServer);
    };

})(ko, battleShip.game.model);

