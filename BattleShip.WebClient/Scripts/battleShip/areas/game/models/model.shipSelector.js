﻿var battleShip = battleShip || {};
battleShip.game = battleShip.game || {};
battleShip.game.model = battleShip.game.model || {};

battleShip.game.model.shipSelector = (function (ko) {

    function shipSelector() {

        var self = this;

        self.isModified = ko.observable(false);

        self.selectedShipDefinition = ko.observable(null);
        self.direction = ko.observable("n");
                        
        self.definitions = ko.observableArray();
        
        self.onShipDefinitionClick = function () {

            if (this.AmountOfShips() == 0) {
                self.selectedShipDefinition(null);
            } else {
                self.selectedShipDefinition(this);
            }

            return true;
        };
        
        this.updateSelection();
    };

    shipSelector.init = function() {

    };
        
    shipSelector.prototype.addShip = function (masts) {
                
        var self = this;
        this.definitions().forEach(function (def) {

            if (def.Masts() == masts )
            {
                def.AmountOfShips(def.AmountOfShips() + 1);
                self.selectedShipDefinition(def);
                return;
            }

        });
    };

    shipSelector.prototype.removeShip = function (masts) {

        var self = this;
        this.definitions().forEach(function (def) {

            if (def.Masts() == masts && def.AmountOfShips() > 0) {
                def.AmountOfShips(def.AmountOfShips() - 1);
                self.updateSelection();
                return;
            }
        });

    };

    shipSelector.prototype.updateSelection = function() {

        var selected = this.selectedShipDefinition;
        if (selected() !== null && selected().AmountOfShips() > 0) {
            return;
        }

        var nextToSelect = null;

        ko.utils.arrayForEach(this.definitions(), function(def) {
            if (nextToSelect === null && def.AmountOfShips() > 0) {
                nextToSelect = def;
            }
        });

        selected(nextToSelect);
    };

    return shipSelector;
        
})(ko);
