﻿var battleShip = battleShip || {};
battleShip.game = battleShip.game || {};


battleShip.game.model = (function (model, $) {

    model.shipMastModel = function (x, y) {
        this.x = x;
        this.y = y;
    };

    model.shipModel = function () {
        this.masts = new Array();
    };

    model.shipPositionModel = function (x, y, masts, direction) {

        this.x = x;
        this.y = y;
        this.masts = masts;
        this.direction = direction;
    };

    return model;
    
})(battleShip.game.model || {}, jQuery);
