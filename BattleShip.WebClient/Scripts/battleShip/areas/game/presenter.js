﻿var battleShip = battleShip || {};
battleShip.game = battleShip.game || {};

battleShip.game.presenter = (function ($, mainPresenter, model) {
   
    var getBoardPieceClass = function (state, highlighting) {

        var cssClass;
        var stateDef = model.boardPieceState;
        switch (state) {
            case stateDef.mishit:
                cssClass = "battelship-boardPiece-Mishit";
                break;
            case stateDef.hit:
                cssClass = "battelship-boardPiece-Hit";
                break;
            case stateDef.hitAndSunken:
                cssClass = "battelship-boardPiece-HitAndSunken";
                break;
            case stateDef.notHit:
                cssClass = "battelship-boardPiece-NoHit";
                break;    
            case stateDef.unknown:
            default :
                cssClass = "";
                break;
        }

        var highlightingDef = model.boardPieceHighlighting;
        switch (highlighting) {                        
            case highlightingDef.warning:
                cssClass += " battelship-boardPiece-Selected";
                break;
            case highlightingDef.error:
                cssClass += " battelship-boardPiece-Error";
                break;
            case highlightingDef.info:
                cssClass += " battelship-boardPiece-Info";
                break;
            case highlightingDef.normal:            
            default:
                break;
        }
        
        return cssClass;
    };
   
    var showPopup = function (title, bodyHtml) {
        mainPresenter.showPopup(title, bodyHtml);
    };

    var showErrorPopup = function (failReason) {
        mainPresenter.showErrorPopup(failReason);
    };

    return {
        getBoardPieceClass: getBoardPieceClass,
        showPopup: showPopup,
        showErrorPopup: showErrorPopup,
    };

})(jQuery, battleShip.main.presenter, battleShip.game.model);