﻿var battleShip = battleShip || {};
battleShip.data = battleShip.data || {};
battleShip.data.services = battleShip.data.services || {};

battleShip.data.services.initSignalR = (function (events, errors, security) {

    var init = function() {

        events.messenger.subscribe(events.messages.signIn, function() {

            $.connection.hub.logging = true;
            $.connection.hub.start()
                .fail(function() {
                    errors.showError("Error when starting async communication");
                })
                .done(function() {
                    events.messenger.publish(events.messages.signalRConnected);
                });

        });
        
        events.messenger.subscribe(events.messages.signOut, function () {

            events.messenger.publish(events.messages.signalRDisconnecting);

            $.connection.hub.stop();
        });
        
    };

    init();

    return {

    };

})(battleShip.events, battleShip.errors, battleShip.data.security);