﻿var battleShip = battleShip || {};
battleShip.data = battleShip.data || {};
battleShip.data.services = battleShip.data.services || {};

battleShip.data.services.game = (function (amplify) {

    var initAmplify = function () {

        amplify.request.define("getGamePlanningData", "ajax", {
            url: "/game/planning/IndexJson/{id}",
            dataType: 'json',
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            decoder: 'customDecoder',
        });

        amplify.request.define("getGameShootingData", "ajax", {
            url: "/game/shooting/IndexJson/{id}",
            dataType: 'json',
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            decoder: 'customDecoder',
        });


        amplify.request.define("postGameShootingShoot", "ajax", {
            url: "/game/shooting/Shoot/{id}",
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataMap: JSON.stringify,
            decoder: 'customDecoder',
        });
    };

    function getGamePlanningData(callbacks, playerContextId) {

        var data = {
            id: playerContextId,
        };

        amplify.request({
            resourceId: 'getGamePlanningData',
            success: callbacks.success,
            error: callbacks.error,
            data: data
        });
    };

    function getGameShootingData(callbacks, playerContextId) {

        var data = {
            id: playerContextId
        };

        amplify.request({
            resourceId: 'getGameShootingData',
            success: callbacks.success,
            error: callbacks.error,
            data: data
        });
    };

    function postGameShootingShoot(callbacks, playerContextId, x, y) {

        var data = {
            id: playerContextId,
            x: x,
            y: y,
        };

        amplify.request({
            resourceId: 'postGameShootingShoot',
            success: callbacks.success,
            error: callbacks.error,
            data: data,
        });
    };

    initAmplify();

    return {
        // init: initAmplify,
        getGamePlanningData: getGamePlanningData,
        getGameShootingData: getGameShootingData,
        postGameShootingShoot: postGameShootingShoot,
    };
})(amplify)