﻿var battleShip = battleShip || {};
battleShip.data = battleShip.data || {};
battleShip.data.services = battleShip.data.services || {};

battleShip.data.services.authentication = (function (amplify) {

	var initAmplify = function () {

		amplify.request.define("getUserInfo", "ajax", {
			url: "/ApiAuthentication/UserInfo",
			dataType: 'json',
			type: 'GET',
			contentType: 'application/json; charset=utf-8',
			decoder: 'customDecoder',
		});

		amplify.request.define("LogOut", "ajax", {
		    url: "/ApiAuthentication/LogOut",
		    dataType: '',
		    type: 'POST',
		    contentType: 'application/json; charset=utf-8',
		    decoder: 'customDecoder',
		});

		amplify.request.define("getProviders", "ajax", {
		    url: "/ApiAuthentication/Providers",
		    dataType: 'json',
		    type: 'GET',
		    contentType: 'application/json; charset=utf-8',
		    decoder: 'customDecoder',
		});

		amplify.request.define("playAsGuest", "ajax", {
		    url: "/token",
		    dataType: 'json',
		    type: 'POST',
		    contentType: 'application/json; charset=utf-8',
		    decoder: 'customDecoder',
		});

	
	};

	function getUserInfo(callbacks) {
		
		amplify.request({
			resourceId: 'getUserInfo',
			success: callbacks.success,
			error: callbacks.error,
		});
	};

	function playAsGuest(callbacks, nickName) {

	    var data = {
	        grant_type: "password",
	        username: nickName,
	        password: ""
	    };

	    amplify.request({
	        resourceId: 'playAsGuest',
	        success: callbacks.success,
	        error: callbacks.error,
            data: data,
	    });
	}

	function getProviders(callbacks) {

	    amplify.request({
	        resourceId: 'getProviders',
	        success: callbacks.success,
	        error: callbacks.error,
	    });
	};

	function logOut(callbacks) {

	    amplify.request({
	        resourceId: 'LogOut',
	        success: callbacks.success,
	        error: callbacks.error,
	    });
	};


	initAmplify();

	return {		
	    getUserInfo: getUserInfo,
	    logOut: logOut,
	    getProviders: getProviders,
	    playAsGuest: playAsGuest,
	};
})(amplify)