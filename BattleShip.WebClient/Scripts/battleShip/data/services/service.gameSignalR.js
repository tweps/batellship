﻿var battleShip = battleShip || {};
battleShip.data = battleShip.data || {};
battleShip.data.services = battleShip.data.services || {};

battleShip.data.services.gameSignalR = (function (amplify, events, security) {

    var battleShipHub = null;
    var callbacks = $.Callbacks();

    var recivedUpdate = function (playerContextId, data) {

        var dto = JSON.parse(data);
        callbacks.fire(playerContextId, dto);
    };

    var registerOnUpdateRecive = function (callback) {

        callbacks.add(callback);
    };

    var init = function () {

        battleShipHub = $.connection['battleShipHub'];
        battleShipHub.client.update = recivedUpdate;

        events.messenger.subscribe(events.messages.signalRConnected, function () {

            var header = security.getSecurityHeaders();
            
            battleShipHub.server.registerPlayer(header.Authorization);
        });

    };

    init();

    return {
        registerOnUpdateRecive: registerOnUpdateRecive,
    };

})(amplify,battleShip.events,battleShip.data.security);