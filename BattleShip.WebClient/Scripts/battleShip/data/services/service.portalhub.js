﻿var battleShip = battleShip || {};
battleShip.data = battleShip.data || {};
battleShip.data.services = battleShip.data.services || {};

battleShip.data.services.portalhub = (function (amplify) {

    var init = function() {

        var apiUrl = '/gamehubapi/'

        amplify.request.define('getGame', 'ajax', {
            url: apiUrl + 'opengames',
            dataType: 'json',
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            decoder: 'customDecoder',
            // cache: true
            // cache: 60000 // 1 minute
            // cache: 'persist'
        });

        amplify.request.define('getOpenGames', 'ajax', {
            url: apiUrl + 'opengames',
            dataType: 'json',
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            decoder: 'customDecoder',
            // cache: true
            // cache: 60000 // 1 minute
            // cache: 'persist'
        });

        amplify.request.define('getPlayerGames', 'ajax', {
            url: apiUrl + 'PlayerGames',
            dataType: 'json',
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            decoder: 'customDecoder',
            // cache: true
            // cache: 60000 // 1 minute
            // cache: 'persist'
        });

        amplify.request.define('joinToOpenGame', 'ajax', {
            url: apiUrl + 'PlayerGames/{id}',
            dataType: 'json',
            type: 'PUT',
            contentType: 'application/json; charset=utf-8',
            decoder: 'customDecoder',
        });

        amplify.request.define('createNewGame', 'ajax', {
            url: apiUrl + 'PlayerGames',
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            decoder: 'customDecoder',
        });
    };

    function getGame(callbacks, data) {
     
        return amplify.request({
            resourceId: 'getGame',
            success: callbacks.success,
            error: callbacks.error,
        });
    }

    function getOpenGames(callbacks) {
        return amplify.request({
            resourceId: 'getOpenGames',
            success: callbacks.success,
            error: callbacks.error
        });
    }

    function getPlayerGames(callbacks) {
        return amplify.request({
            resourceId: 'getPlayerGames',
            success: callbacks.success,
            error: callbacks.error
        });
    }

    function getPlayerGame(callbacks, data) {
        return amplify.request({
            resourceId: 'getPlayerGames',
            data: data,
            success: callbacks.success,
            error: callbacks.error
        });
    }

    function joinToOpenGame(callbacks, data) {

        return amplify.request({
            resourceId: 'joinToOpenGame',
            data: data,
            success: callbacks.success,
            error: callbacks.error
        });        
    }

    function createNewGame(callbacks) {

        var results = new Array();

        results.push(amplify.request({
            resourceId: 'createNewGame',
            success: callbacks.success,
            error: callbacks.error
        }));

        return results;
    }

    init();

    var dataservice = {

        getGame: getGame,
        getOpenGames: getOpenGames,        
        getPlayerGames: getPlayerGames,
        getPlayerGame: getPlayerGame,
        joinToOpenGame: joinToOpenGame,
        createNewGame: createNewGame
    };

    return dataservice;

})(amplify);
