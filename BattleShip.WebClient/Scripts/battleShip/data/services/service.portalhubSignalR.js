﻿var battleShip = battleShip || {};
battleShip.data = battleShip.data || {};
battleShip.data.services = battleShip.data.services || {};

battleShip.data.services.portalhubSignalR = (function (events) {

    var portalHub = null;
    var callbacksAdd = $.Callbacks();
    var callbacksRemove = $.Callbacks();

    var onAdd = function (data) {

        var dto = JSON.parse(data);
        callbacksAdd.fire(dto);
    };

    var onRemove = function (data) {

        var dto = JSON.parse(data);
        callbacksRemove.fire(dto);
    };

    var init = function () {

        portalHub = $.connection['gameNotificationHub'];

        portalHub.client.add = onAdd;
        portalHub.client.remove = onRemove;

        events.messenger.subscribe(events.messages.signalRConnected, function () {            
            portalHub.server.subscribeGameNotification();
        });

        events.messenger.subscribe(events.messages.signalRDisconnecting, function () {            
            portalHub.server.unubscribeGameNotification();
        });
    };

    init();

    return {

        subscribe: function (callbackAdd, callbackRemove) {
            callbacksAdd.add(callbackAdd);
            callbacksRemove.add(callbackRemove);
        },

        unsubscribe: function (callbackAdd, callbackRemove) {
            callbacksAdd.remove(callbackAdd);
            callbacksRemove.remove(callbackRemove);
        },
    };

})(battleShip.events);