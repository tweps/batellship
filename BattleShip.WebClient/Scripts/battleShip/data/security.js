﻿var battleShip = battleShip || {};
battleShip.data = battleShip.data || {};

battleShip.data.security = (function (events) {

    var token = null;

    function parseQueryString(queryString) {

        if (typeof queryString.indexOf == 'undefined') {
            return queryString;
        }
        
        var hashIndex = queryString.indexOf("#");
        if ( hashIndex >= 0) {
            queryString = queryString.substr(hashIndex+1);
        }

        if (queryString.length == 0) {
            return {};
        }


        var data = {},
            pairs, pair, separatorIndex, escapedKey, escapedValue, key, value;

        if (queryString == null) {
            return data;
        }

        pairs = queryString.split("&");

        for (var i = 0; i < pairs.length; i++) {
            pair = pairs[i];
            separatorIndex = pair.indexOf("=");

            if (separatorIndex === -1) {
                escapedKey = pair;
                escapedValue = null;
            } else {
                escapedKey = pair.substr(0, separatorIndex);
                escapedValue = pair.substr(separatorIndex + 1);
            }

            key = decodeURIComponent(escapedKey);
            value = decodeURIComponent(escapedValue);

            data[key] = value;
        }

        return data;
    }   
    
    function getSecurityHeaders() {

        if (token != null) {
            return { "Authorization": "Bearer " + token };
        }

        return {};
    }

    // public 

    return {
        initOAuth: function(path) {
            var fragment = parseQueryString(path || "");
            if (typeof fragment.access_token != 'undefined') {
                token = fragment.access_token;
                events.messenger.publish(events.messages.signIn);
                return true;
            }
            return false;
        },

        clearToken: function() {
            token = null;
            events.messenger.publish(events.messages.signOut);
        },

        isUserAuthenticated: function() {
            return token != null;
        },

        getSecurityHeaders: getSecurityHeaders,
    };

})(battleShip.events);