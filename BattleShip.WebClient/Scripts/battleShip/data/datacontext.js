﻿var battleShip = battleShip || {};
battleShip.data = battleShip.data || {};
battleShip.mappers = battleShip.mappers || {};

battleShip.data.datacontext = (function ($, services, mappers, events, errors, security) {

    var self = this;

    var cache = {};

    var cacheNamePlayerItems = "cachedPlayerItems";
    var cacheNamePublicItems = "cachePublicItems";
    var cacheNamePublicSync = "cacheNamePublicSync";
    var cacheNameGameItems = "cacheGameContextsItems";
    var cacheNamePlayerAccount = "cacheNamePlayerAccoun";

    var cacheClear = function () {
        cache[cacheNamePlayerItems] = [];
        cache[cacheNamePublicItems] = [];
        cache[cacheNameGameItems] = [];
        cache[cacheNamePublicSync] = [];
        cache[cacheNamePlayerAccount] = null;
    };

    function onError(exception, status, deferred) {

        var msg;
        if (exception != null) {
            msg = exception;
            if (typeof exception.Message != 'undefined')
                msg = exception.Message;
            if( typeof exception.error != 'undefined')
                msg = exception.error;
        } else {
            msg = "Unknown Error";
        }
        
        errors.showError(msg);
        deferred.reject(exception);
    };

    function itemsToArray(items, targetObservableArray, filterFunction, sortFunction) {

        if (!targetObservableArray) return;

        var tempArray = new Array();
        for (var id in items) {
            var obj = items[id];
            if (obj != null)
                tempArray.push(obj);
        }

        if (filterFunction) {
            tempArray = tempArray.filter(filterFunction);
        }

        if (sortFunction) {
            tempArray = tempArray.sort(sortFunction);
        }

        targetObservableArray(tempArray);
    };

    function mapToContext(items, dtoList, mapper) {

        var memo = {};
        for (var i = 0; i < dtoList.length; i++) {
            var dto = dtoList[i];
            var id = mapper.getDtoId(dto);
            var existingItem = items[id];
            memo[id] = mapper.fromDto(dto,existingItem);
        }

        return memo;
    }

    function itemsLoad(cacheName, service, serviceData, mapper, targetObservableArray, forceRefresh, filter, sort) {

        return $.Deferred(function (def) {

            if (forceRefresh || !targetObservableArray) {

                var callbacks = {
                    success: function (dtoList) {

                        cache[cacheName] = mapToContext(cache[cacheName], dtoList, mapper);
                        itemsToArray(cache[cacheName], targetObservableArray, filter, sort);
                        def.resolve(targetObservableArray);
                    },

                    error: function (exception, status) { onError(exception, status, def) },
                };

                service(callbacks, serviceData);
                   
            } else {

                itemsToArray(cache[cacheName], targetObservableArray, filter, sort);
                def.resolve(targetObservableArray);
            }
        });
    }

    function syncItemAddUpdateOrDelete(cacheName, service, serviceData, mapper, itemId) {

        return $.Deferred(function (def) {

            var items = cache[cacheName] || [];

            var calllbacks = {
                success: function (dto) {
             
                    var id = mapper.getDtoId(dto);

                    if (id == null || typeof(id)=='undefined') {
                        // delete item
                        if (itemId && items[itemId]) {
                            delete items[itemId];
                            def.resolve(null);
                        }
                    } else {

                        // add or update item
                        var obj = mapper.fromDto(dto, items[id]);
                        items[id] = obj;
                        def.resolve(items[id]);
                    }
                },

                error: function (exception, status) { onError(exception, status, def); },
            };

            service(calllbacks, serviceData);
        });
    }

    function syncGameMenuWithGameItem(playerContextId) {
   
        var playerItem = cache[cacheNamePlayerItems][playerContextId];
        var gameItem = cache[cacheNameGameItems][playerContextId];
        var isNew = false;

        if (typeof (playerItem) == 'undefined') {
            isNew = true;
        };

        // If posible then update game menu item base on already loaded data
        // If not then recive data from server
        if (isNew || playerItem.status() <=2 || typeof(gameItem)=='undefined' || gameItem==null) {

            var serviceData = {
                id: playerContextId,
            };

            $.when(syncItemAddUpdateOrDelete(cacheNamePlayerItems, services.portalhub.getPlayerGame, serviceData, mappers.portalMapper.playerGame))
            .done(function () {
                if (isNew) {
                    events.messenger.publish(events.messages.playerHasNewGame);
                }
            });

        } else {
           
            playerItem.status(gameItem.gameInfo.status());
        }
    };

    function onGameUpdate(playerContextId, dto) {
        
        // Update BattelShip game data 
        var mapper = mappers.gameMapper.game;
        var id = mapper.getDtoId(dto);
        var item = cache[cacheNameGameItems][id];

        if(typeof(item)!='undefined' && item!=null){
        
            cache[cacheNameGameItems][id] = mapper.fromDto(dto, item, true);            
        }

        syncGameMenuWithGameItem(id);
    };
     
    function onAddPublicGame(dto) {

        var mapper = mappers.portalMapper.publicGame;
        if (cache[cacheNamePublicSync].length > 0) {

            var id = mapper.getDtoId(dto);
            var publicGame = mapper.fromDto(dto, cache[cacheNamePublicItems][id]);
            cache[cacheNamePublicItems][id] = publicGame;

            cache[cacheNamePublicSync].forEach(function (params) {

                var index = params.results.indexOf(publicGame);
                if (index < 0) {
                    params.results.push(publicGame);
                    params.results.sort(params.sort);
                }
            });
        };
    };

    function onRemovePublicGame(dto) {
        
        var mapper = mappers.portalMapper.publicGame;
        if (cache[cacheNamePublicSync].length > 0) {

            var id = mapper.getDtoId(dto);
            var publicGame = cache[cacheNamePublicItems][id];

            if (publicGame != null && publicGame != 'undefined') {

                cache[cacheNamePublicSync].forEach(function (params) {
                    params.results.remove(publicGame);
                });
            
                delete cache[cacheNamePublicItems][publicGame.gameId];
                delete publicGame;
            };                        
        };
       
    };

    var init = function () {

        cacheClear();

        services.gameSignalR.registerOnUpdateRecive(onGameUpdate);
        services.portalhubSignalR.subscribe(onAddPublicGame, onRemovePublicGame);
    };
    
    return {

        init: init,

        portal: {

            playerGames: {

                get: function (params) {

                    return itemsLoad(cacheNamePlayerItems, services.portalhub.getPlayerGames, null, mappers.portalMapper.playerGame ,
                                      params.results, params.forceRefresh, params.filter, params.sort
                                    );
                },
                
                join: function (gameId) {

                    return $.Deferred(function (def) {

                        var serviceData = {
                            id: gameId
                        };

                        $.when(syncItemAddUpdateOrDelete(cacheNamePlayerItems, services.portalhub.joinToOpenGame, serviceData, mappers.portalMapper.playerGame))
                        .fail(function (exception) {
                            def.reject(exception);
                        })
                        .done(function (playerGameItem) {

                            def.resolve(playerGameItem);

                            $.when(syncItemAddUpdateOrDelete(cacheNamePublicItems, services.portalhub.getOpenGames, serviceData, mappers.portalMapper.publicGame, gameId))
                            .done(function (publicGameItem) {                               
                            });                            
                        });                        
                    });
                },

                create: function () {

                    return syncItemAddUpdateOrDelete(cacheNamePlayerItems, services.portalhub.createNewGame, null, mappers.portalMapper.playerGame );
                },
            },

            publicGames: {

                get: function (params, gameId) {

                    if (gameId != 'undefined' && gameId != null) {

                        var serviceData = {
                            id: gameId,
                        };
                        return syncItemAddUpdateOrDelete(cacheNamePublicItems, services.portalhub.getOpenGames, serviceData, mappers.portalMapper.publicGame);
                    }

                    return itemsLoad(cacheNamePublicItems, services.portalhub.getOpenGames, null, mappers.portalMapper.publicGame,
                                     params.results, params.forceRefresh, params.filter, params.sort 
                                    );
                },

                subscribeSync: function (params) {

                    if (cache[cacheNamePublicSync].indexOf(params) > -1) {
                        return;
                    }
                    cache[cacheNamePublicSync].push(params);
                },

                unsubscribeSync: function (params) {
                    cache[cacheNamePublicSync] = cache[cacheNamePublicSync].filter(function (el) {
                        return el === params;
                    });
                },
            },

            playerAccount: {

                get: function (userInfo) {

                    return $.Deferred(function (def) {

                        if (userInfo == null && cache[cacheNamePlayerAccount] != null) {
                            def.resolve(cache[cacheNamePlayerAccount]);
                            return;
                        }

                        if (security.isUserAuthenticated()) {

                            var callbacks = {
                                success: function (dto) {
                                    var obj = mappers.portalMapper.playerAccount.fromDto(dto, userInfo);
                                    cache[cacheNamePlayerAccount] = obj;
                                    def.resolve(obj);
                                },
                                error: function (exception, status) {
                                    onError(exception, status, def);
                                },
                            };

                            services.authentication.getUserInfo(callbacks);
                        } else {
                            def.resolve(mappers.portalMapper.playerAccount.fromDto({}, userInfo));
                        }

                    });
                },

                login: {
                    
                    signOut: function () {

                        return $.Deferred(function (def) {

                            var callbacks = {
                                success: function () {
                                    cacheClear();
                                    security.clearToken();
                                    def.resolve();
                                },
                                error: function (exception, status) {
                                    onError(exception, status, def);
                                },
                            };

                            services.authentication.logOut(callbacks);
                        });
                    },

                    signInAsGuest: function (nickName) {

                        return $.Deferred(function (def) {

                            var callbacks = {
                                success: function (data) {

                                    if (security.initOAuth(data)) {
                                        def.resolve();
                                    } else {
                                        def.reject();
                                    }
                                },
                                error: function (exception, status) {
                                    onError(exception, status, def);
                                },
                            };

                            services.authentication.playAsGuest(callbacks, nickName);
                        });
                    },
                    
                    singInProviders: function (observableArray) {

                        return $.Deferred(function (def) {

                            var callbacks = {
                                success: function (data) {
                                    observableArray.removeAll();
                                    for (var i = 0; i < data.length; i++) {
                                        observableArray.push(mappers.portalMapper.externalProvider.fromDto(data[i]));
                                    }
                                    def.resolve(observableArray);
                                },
                                error: function (exception, status) {
                                    onError(exception, status, def);
                                },
                            };

                            services.authentication.getProviders(callbacks);
                        });
                    },
                }                
            }
        },

        game: {

            planning: {

                get: function (game) {

                    return $.Deferred(function (def) {

                        cache[cacheNameGameItems][game.playerContextId] = game;
                        var serviceData = game.playerContextId;

                        $.when(syncItemAddUpdateOrDelete(cacheNameGameItems, services.game.getGamePlanningData, serviceData, mappers.gameMapper.game))
                        .done(function () {

                            syncGameMenuWithGameItem(game.playerContextId);

                            def.resolve();
                        });

                    });                    
                }

            },

            shooting: {

                get: function (game) {

                    return $.Deferred(function (def) {

                        cache[cacheNameGameItems][game.playerContextId] = game;
                        var serviceData = game.playerContextId;

                        $.when(syncItemAddUpdateOrDelete(cacheNameGameItems, services.game.getGameShootingData, serviceData, mappers.gameMapper.game))
                        .done(function () {

                            syncGameMenuWithGameItem(game.playerContextId);

                            def.resolve();
                        });

                    });                    
                } ,

                shoot: function (game, x, y) {

                    return $.Deferred(function (def) {

                        var service = function (callbacks) {

                            return services.game.postGameShootingShoot(callbacks, game.playerContextId, x, y);
                        };

                        $.when(syncItemAddUpdateOrDelete(cacheNameGameItems, service, null, mappers.gameMapper.game))
                        .done(function () {

                            syncGameMenuWithGameItem(game.playerContextId);

                            def.resolve();
                        });
                    });                    
                },

                archive: function (game) {

                    delete cache[cacheNameGameItems][game.playerContextId];
                    delete cache[cacheNamePlayerItems][game.playerContextId];

                },
            }
        },

    };


})(jQuery, battleShip.data.services, battleShip.mappers, battleShip.events, battleShip.errors, battleShip.data.security);