﻿var battleShip = battleShip || {};
battleShip.main = battleShip.main || {};

battleShip.main.viewModelLocator = (function ($, ko) {

    var viewModelLocator = {};


    function getVM(viewName) {

        if (viewName==null || viewModelLocator[viewName] == 'undefined') {
            return null;
        }

        var vm = viewModelLocator[viewName]();

        return vm;
    };

    function init() {
        
        for (var i = 0; i < arguments.length; i++) {
            var data = arguments[i];
            for (j = 0; j < data.length; j++) {

                var vmCfg = data[j];
                viewModelLocator[vmCfg.ViewName] = vmCfg.ViewModelCreator;
            }
        }        
    };

    return {
        init : init,
        getVM: getVM
    }

})(jQuery,ko);