﻿var battleShip = battleShip || {};
battleShip.main = battleShip.main || {};

battleShip.main.binder = (function ($, ko, viewIds, viewModelLocator) {

    var bind = function (viewNode, viewName, initParams ) {

        var vm = viewModelLocator.getVM(viewName);

        if (vm != null) {

            vm.init(initParams);
            ko.applyBindings(vm, viewNode);
        }
    };

    var unbind = function (viewNode) {

        ko.cleanNode(viewNode);
    };

    var activate = function(viewNode, params) {

        if (typeof (viewNode) == 'undefined') {
            return;
        }

        var vm = ko.dataFor(viewNode);
        if (vm != null && vm!='undefined') {
            vm.activate(params);
        }
    };

    var deactivate = function (viewNode, forceDeleteView) {

        if (typeof (viewNode) == 'undefined' || viewNode==null) {
            return false;
        }

        var deleteView = true;
        var vm = ko.dataFor(viewNode);      
        if(vm!=null){
            deleteView = vm.deactivate(forceDeleteView);
        }

        deleteView = deleteView || forceDeleteView;

        if (deleteView) {
            unbind(viewNode);
        }

        return deleteView;
    };


    var init = function () {
        
    };

    return {

        init: init,
        bind: bind,
        unbind: unbind,
        activate: activate,
        deactivate: deactivate,
    };

})(jQuery, ko, battleShip.config.viewIds, battleShip.main.viewModelLocator);