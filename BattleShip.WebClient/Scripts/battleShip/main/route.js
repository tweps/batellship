﻿var battleShip = battleShip || {};
battleShip.main = battleShip.main || {};

battleShip.main.route = (function ($, ko, Sammy, config, presenter, binder, events, errors, security, navigator) {
    
    function getViewNode(routeData, routeContext) {

        return $.Deferred(function (def) {

            var viewId = routeData.getViewId(routeContext.params);
            var view = presenter.getView(viewId);
            var forceReLoad = routeContext.verb == "post";

            if (view != null) {

                if (!forceReLoad) {

                    def.resolve(view);
                    return;
                }

                binder.unbind(view);
                presenter.deleteView(viewId);
            }

            var htmlSrcUrl = routeData.routeTarget(routeContext.params);
            var data = routeData.routeTargetParams(routeContext.params);

            var onSuccess = function(data, textStatus, jqXHR) {

                var viewName = jqXHR.getResponseHeader("ViewName");

                view = presenter.createView(viewId, data);

                binder.bind(view, viewName, routeContext.params);

                def.resolve(view);
            };
            var onError = function (response) {

                if (response.status == 401) { // unauthorized                    
                    navigator.navigateToLoginPage();
                } else {
                    errors.showError(response.statusText);
                }

                def.reject();
            };

            $.ajax(htmlSrcUrl, {
                type: routeContext.verb.toUpperCase(),
                data: data,
                headers: security.getSecurityHeaders()
            }).success(onSuccess).error(onError);
        });
    }

    var sammy = new Sammy.Application(function () {

    });

    var registerRoute = function (routeData) {

        sammy.route(routeData.routeType, routeData.routePath, function (context) {                           

            $.when(getViewNode(routeData, context))
            .done(function (view) {
                                              
                binder.activate(view, context.params);

                var deletePreviousView = binder.deactivate(presenter.getActiveView());
                                
                presenter.transitionTo(view, deletePreviousView);

                if (this.title) {
                    this.title(routeData.title);
                }

                events.messenger.publish(events.messages.activeWindowChanged, context.path);
            });            
        });        
    };

    var resetView = function () {

        var views = presenter.getAllViews();
        for (i = 0; i < views.length; i++) {
            binder.deactivate(views[i], true);
            presenter.deleteView(views[i].id);
        }
        sammy.refresh();
    }
        
    var init = function (viewRoot, routingsCfg, startUrl) {

        //#region General Settings
        sammy.raise_errors = true;
        sammy.element_selector = '#' + viewRoot;
        sammy.run_interval_every = 100;
        sammy.disable_push_state = true;
        //#endregion

        //#region OAuth
        var patternOAuth = /#access_token.*/;
        sammy.before(patternOAuth, function (context) {           
            security.initOAuth(context.path);
            context.redirect(startUrl);
            return false;
        }).get(patternOAuth, function (context) { });

        events.messenger.subscribe(events.messages.signOut, resetView);
        //#endregion

        //#region Setup Routings

        for (var i = 0; i < routingsCfg.length; i++) {
            var routeData = routingsCfg[i];
            $.each(routeData, function (index, routeData) {
                registerRoute(routeData);
            });
        }
                
        //#endregion

        sammy.run(startUrl);        
    };


    return {
        init: init,
    };

})(jQuery, ko, Sammy, battleShip.config, battleShip.main.presenter, battleShip.main.binder, battleShip.events, battleShip.errors, battleShip.data.security, battleShip.main.navigator);