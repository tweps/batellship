﻿var battleShip = battleShip || {};
battleShip.main = battleShip.main || {};

battleShip.main.presenter = (function ($) {

    var
        viewRoot = "",
        transitionOptions = {
            ease: 'swing',
            fadeOut: 100,
            floatIn: 500,
            offsetLeft: '20px',
            offsetRight: '-20px'
        },

        init = function (viewRootToSet) {
            viewRoot = '#' + viewRootToSet;
        },

        getView = function (viewId) {

            var nodes = $('#' + viewId);
            if (nodes.length > 1) {
                throw Error("Too many nodes with the same id");
            }

            if (nodes.length == 1) {

                return nodes[0];
            }

            return null;
        },

        getActiveView = function () {

            var nodes = $('.view-active');
            if (nodes.length > 1) {
                throw Error("Too many nodes with the same id");
            }

            if (nodes.length == 1) {

                return nodes[0];
            }

            return null;
        },

        getAllViews = function(){

            return $(".view");
        },

        deleteView = function (viewId) {

            var $nodes = $('#' + viewId);

            $nodes.remove();
        },

        createView = function (viewId, html) {

            var newNode = $('<div/>', {
                id: viewId,
            }).appendTo(viewRoot);

            var viewNode = newNode[0];

            $(viewNode).addClass('view').hide().html(html);

            return viewNode;
        },

        resetViews = function () {

            $('.view').css({
                marginLeft: transitionOptions.offsetLeft,
                marginRight: transitionOptions.offsetRight,
                opacity: 0,
            });

        },

        entranceThemeTransition = function (view) {
            $(view).css({
                display: 'block',
                visibility: 'visible',
            }).addClass('view-active').animate({
                marginRight: 0,
                marginLeft: 0,
                opacity: 1
            }, transitionOptions.floatIn, transitionOptions.ease);
        },

        transitionTo = function (view, deletePreviousView) {

            toggleActivity(true);

            var $activeView = $('.view-active');
            if ($activeView.length) {

                if ($activeView.length === 1 && $activeView[0].id === view.id) {
                    toggleActivity(false);
                    return;
                }

                $activeView.fadeOut(transitionOptions.fadeOut, function () {
                    resetViews();
                    entranceThemeTransition(view);
                });

                if (deletePreviousView) {
                    $activeView.remove();
                } else {
                    $('.view').removeClass('view-active');
                }
            } else {
                resetViews();
                entranceThemeTransition(view);
            }

            toggleActivity(false);
        },

        toggleActivity = function (show) {

            var $loader = $('#loader');
            if (show) {
                $loader.show();
            } else {
                $loader.hide();
            }
        },


        setActiveNavigationMenu = function () {

            $('#menuMain li').removeClass('active');

            var url = '/' + window.location.hash;

            if (url.indexOf('/#!/game/') == 0) {
                url = '/#!/game/main';
            }

            $('#menuMain a[href="' + url + '"]').parent('li').addClass('active');
        };


    // Shared Functionality
    var showPopup = function (title, bodyHtml) {
        
        $("#mainModalInfoLabel").html(title);
        $("#mainModalInfoBody").html(bodyHtml);

        $("#mainModalInfo").modal('show');
    };

    var showErrorPopup = function (failReason) {
        showPopup("Error", failReason);
    };

    return {

        init: init,
        getView: getView,
        getAllViews: getAllViews,
        deleteView: deleteView,
        createView: createView,
        getActiveView: getActiveView,
        transitionTo: transitionTo,
        toggleActivity: toggleActivity,

        showPopup: showPopup,
        showErrorPopup: showErrorPopup,
        setActiveNavigationMenu: setActiveNavigationMenu,
    };

})(jQuery);

window.onhashchange = battleShip.main.presenter.setActiveNavigationMenu;