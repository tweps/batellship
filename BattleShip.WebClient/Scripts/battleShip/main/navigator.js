﻿var battleShip = battleShip || {};
battleShip.main = battleShip.main || {};

battleShip.main.navigator = (function (url) {

    function setUrl(dst) {
        window.location = dst;
    };

    return {
        navigateToGame: function(gameId) {
            setUrl(url.game.replace(':id', gameId));
        } ,
        navigateToLoginPage: function() {
            setUrl(url.unauthorized);
        },
        navigateTo: function(dstUrl) {
            setUrl(dstUrl);
        }
    };

})(battleShip.config.url);