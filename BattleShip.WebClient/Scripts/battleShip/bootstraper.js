﻿var battleShip = battleShip || {};


battleShip.bootstraper = (function ($, battleShip) {
      
    return {
        run: function () {
                            
            battleShip.main.presenter.toggleActivity(true);

            battleShip.data.datacontext.init();

            battleShip.main.presenter.init(battleShip.config.viewIds.main.viewRoot);


            battleShip.main.binder.init();

            battleShip.main.viewModelLocator.init( battleShip.portal.config.viewModelLocator.getCfg(),
                                                   battleShip.game.config.viewModelLocator.getCfg()                                                   
                );

            var routingsCfg = new Array();           
            routingsCfg.push(battleShip.portal.config.route.getRouteData());
            routingsCfg.push(battleShip.game.config.route.getRouteData());

            battleShip.main.route.init(battleShip.config.viewIds.main.viewRoot, routingsCfg, "/" + battleShip.config.url.startUrl);

            battleShip.main.binder.bind(document.getElementById(battleShip.config.viewIds.main.menuRoot), "Portal_GameMenu");
            battleShip.main.binder.bind(document.getElementById(battleShip.config.viewIds.portal.accountInfo), "Portal_AccountInfo");
 
            battleShip.main.presenter.toggleActivity(false);
        }
    };

})(jQuery, battleShip);


$(document).ready(function () {
    battleShip.bootstraper.run();
});