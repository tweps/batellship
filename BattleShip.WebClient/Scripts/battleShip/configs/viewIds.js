﻿var battleShip = battleShip || {};
battleShip.config = battleShip.config || {};

battleShip.config.viewIds = (function () {

    return {
        game: "battleShipGameView",
        portal: {
            gameMenu: "portalGameMenuView",
            gameSelector: "portalGameSelectorView",
            cmsPage: "cmsPage",
            signIn: "signInView",
            accountInfo: "accountInfoView",
        },
        main: {
            viewRoot: "main",
            menuRoot: "leftArea",                        
        },
    };

})();