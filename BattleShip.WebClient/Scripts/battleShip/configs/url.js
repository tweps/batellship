﻿var battleShip = battleShip || {};
battleShip.config = battleShip.config || {};

battleShip.config.url = (function () {
    
    var prefix = "#!/";

    var gamesRoom = prefix + "game/main";
    var game = prefix + "game/:id";
    var submitShipPlacement = "game/submitShipPlacement/:id";

    var cmsPage = prefix + ":pageName";
    
    var unauthorized = prefix + "login";

    return {
        
        gamesRoom: gamesRoom,
        game: game,
        submitShipPlacement: submitShipPlacement,
        cmsPage: cmsPage,

        startUrl: gamesRoom,
        unauthorized: unauthorized,
    };

})();


