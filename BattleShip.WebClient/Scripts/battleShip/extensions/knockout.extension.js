﻿ko.bindingHandlers.multiple = {

    // This will be called when the binding is first applied to an element
    // Set up any initial state, event handlers, etc. here
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        return;
    },

    // This will be called once when the binding is first applied to an element,
    // and again whenever the associated observable changes value.
    // Update the DOM element based on the supplied values here.
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var value = valueAccessor();
        var allBindings = allBindingsAccessor();
        
        var count = value();
        var htmlTemplate = $(element).html();

        var outputHtml = "";
        for (i = 0; i < count; i++) {
            outputHtml += htmlTemplate;
        }

        $(element).html(outputHtml);
    }

}