﻿amplify.request.decoders.customDecoder =
        function (data, status, xhr, success, error) {
            if (status === "success") {
                success(data, status);
            } else if (status === "fail" || status === "error") {
                try {
                    error(JSON.parse(xhr.responseText), xhr.status);
                } catch (er) {
                    error(xhr.responseText, status);
                }
            }
        };

amplify.subscribe("request.before.ajax", function (resource, settings, ajaxSettings, xhr) {

    var headers = battleShip.data.security.getSecurityHeaders();
    if (typeof headers.Authorization != 'undefined') {
        xhr.setRequestHeader('Authorization', headers.Authorization);
    }
});