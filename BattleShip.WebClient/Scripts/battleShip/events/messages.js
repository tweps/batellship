﻿var battleShip = battleShip || {};
battleShip.events = battleShip.events || {};


battleShip.events.messages = {

    playerHasNewGame: "playerHasNewGame",
    battelFinished: "battelFinished",
    activeWindowChanged: "activeWindowChanged",
    signIn: "signIn",
    signOut: "signOut",
    signalRConnected: "signalRConected",
    signalRDisconnecting: "signalRConected",

};