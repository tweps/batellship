﻿var battleShip = battleShip || {};
battleShip.events = battleShip.events || {};

battleShip.events.messenger = (function (amplify) {

    var publish = function (topic) {
        
        amplify.publish(topic, arguments);
    };

    var subscribe = function (topic, callback) {
        amplify.subscribe(topic, callback);
    };

    return {
        publish: publish,
        subscribe: subscribe
    };

})(amplify);

