﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BattleShip.WebClient.Extensions.Filters;

namespace BattleShip.WebClient.Areas.Portal.Controllers
{
    [ViewModelLocatorData("Portal_GameMenu")]
    public partial class GameMenuController : BaseController
    {     
        [AllowAnonymous]
        public virtual ActionResult Index()
        {
            return View();
        }

    }
}
