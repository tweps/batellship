﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BattleShip.WebClient.Extensions.Filters;

namespace BattleShip.WebClient.Areas.Portal.Controllers
{
    public partial class PlayerAccountController : BaseController
    {
        [AllowAnonymous]
        [ViewModelLocatorData("Portal_SignIn")]
        public virtual ActionResult SignIn()
        {
            return View(Views.SignIn);
        }

        [AllowAnonymous]
        [ViewModelLocatorData("Portal_AccountInfo")]
        public virtual ActionResult Info()
        {
            return View(Views.AccountInfo);
        }
	}
}