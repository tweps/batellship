﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BattleShip.WebClient.Areas.Portal.Controllers
{
    [AllowAnonymous]
    public partial class CmsController : BaseController
    {        
        public virtual ActionResult Index(string id)
        {
            return View(id);
        }
	}
}