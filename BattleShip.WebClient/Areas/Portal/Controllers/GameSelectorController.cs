﻿using BattleShip.Domain;
using BattleShip.WebClient.Extensions.Filters;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BattleShip.WebClient.Areas.Portal.Controllers
{    
    [ViewModelLocatorData("Portal_GameSelector")]
    public partial class GameSelectorController : BaseController
    {        
        public virtual ActionResult Index()
        {            
            return View();
        }        
    }
}
