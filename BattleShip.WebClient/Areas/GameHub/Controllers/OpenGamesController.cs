﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using BattleShip.Domain;
using BattleShip.WebClient.Areas.GameHub.Models;
using BattleShip.WebClient.Areas.GameHub.Mapping;
using AutoMapper;
using BattleShip.WebClient.Authentication;

namespace BattleShip.WebClient.Areas.GameHub.Controllers
{    
    public class OpenGamesController : GameHubBaseApiControler
    {
		private readonly IBattleShipGame _game;
		private readonly IMappingEngine _mapper;
        
        public OpenGamesController(IMappingEngine mapper, IBattleShipGame game)
        {
            this._game = game;
			this._mapper = mapper;
        }

       
        /// <summary>
        /// Gets list of free to join games
        /// </summary>
        /// <returns></returns>
        public IQueryable<OpenGameItem> Get(/*[ModelBinder(typeof(PlayerModelBinder))] IPlayer player*/)
        {                        
            var query = _game.GetFreeToJoin(/*player*/).Select(_mapper.Map<IGameInfo, OpenGameItem>);
                        
            return query.AsQueryable();
        }

        /// <summary>
        /// Get game info
        /// </summary>
        /// <returns></returns>
        public OpenGameItem Get(/*[ModelBinder(typeof(PlayerModelBinder))] IPlayer player,*/ Guid id)
        {            
            var result = _game.GetGameInfo(id);
            
            if(result==null)
            {                
                var response = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent("Game doesn’t exists"),
                    ReasonPhrase = "Game ID not found"
                };

                throw new HttpResponseException(response);
            }
            

			return _mapper.Map<IGameInfo, OpenGameItem>(result);                   
        }

    }
}
