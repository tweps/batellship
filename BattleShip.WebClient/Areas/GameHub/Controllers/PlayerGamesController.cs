﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using BattleShip.Domain;
using BattleShip.WebClient.Areas.GameHub.Models;
using System.Web.Http.ModelBinding;
using AutoMapper;
using BattleShip.WebClient.Authentication;


namespace BattleShip.WebClient.Areas.GameHub.Controllers
{
    public class PlayerGamesController : GameHubBaseApiControler
    {
        protected readonly IBattleShipGame gameManager;
		private readonly IMappingEngine mapper;
        
		public PlayerGamesController(IMappingEngine mapper, IBattleShipGame gameManager)
		{
			this.gameManager = gameManager;
			this.mapper = mapper;
		}

       
		/// <summary>
		/// Get Player started games
		/// </summary>
		/// <param name="player"></param>
		/// <returns></returns>
        public IQueryable<PlayerGameItem> Get([ModelBinder(typeof(PlayerModelBinder))] IPlayer player)
        {
            var query = gameManager.GetAllActiveUserContexts(player).Select(Mapper.Map<IPlayerContext, PlayerGameItem>);

		    var test = query.ToArray();
                        
            return query.AsQueryable();
        }

        /// <summary>
        /// Get Player game info
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public PlayerGameItem Get([ModelBinder(typeof(PlayerModelBinder))] IPlayer player, Guid id)
        {
            var playerContext = gameManager.Get(id);

            if(playerContext==null)
            {
                var response = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent("Game doesn’t exists"),
                    ReasonPhrase = "Game ID not found"
                };

                throw new HttpResponseException(response);
            }

            var playerGameItem = Mapper.Map<IPlayerContext, PlayerGameItem>(playerContext);

            return playerGameItem;
        }


        /// <summary>
        /// Create New Game
        /// </summary>        
        public PlayerGameItem Post([ModelBinder(typeof(PlayerModelBinder))] IPlayer player)
        {           
            var playerContext = gameManager.CreateGame(player);
            
			return mapper.Map<IPlayerContext, PlayerGameItem>(playerContext);
        }


        /// <summary>
        /// Join to to the open game
        /// </summary>	        
        public PlayerGameItem Put([ModelBinder(typeof(PlayerModelBinder))] IPlayer player, Guid id)
        {
            var playerContext = gameManager.JoinToGame(id, player);

            if(playerContext==null)
            {
                var response = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent("It wasn’t possible join to this game. Please try with other one."),
                    ReasonPhrase = "Game is no longer open"
                };

                throw new HttpResponseException(response);
            }

            return mapper.Map<IPlayerContext, PlayerGameItem>(playerContext);
        }

    }
}
