﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using BattleShip.WebExtensions.Errors;

namespace BattleShip.WebClient.Areas.GameHub.Controllers
{    
    [Authorize]
    [FormatExceptionFilter]
    public class GameHubBaseApiControler : ApiController
    {
    }
}