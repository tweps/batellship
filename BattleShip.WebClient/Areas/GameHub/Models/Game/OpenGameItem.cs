﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BattleShip.WebClient.Areas.GameHub.Models
{
    public class OpenGameItem : GameItemBase
    {
        [JsonProperty(PropertyName = "gameId")]
        public Guid GameId { get; set; }        
    }
}