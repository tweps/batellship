using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BattleShip.WebClient.Areas.GameHub.Models
{
    public abstract class GameItemBase
    {
        [JsonProperty(PropertyName = "gameName")]
        public string GameName { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }

        [JsonProperty(PropertyName = "playerOneName")]
        public string PlayerOneName { get; set; }

        [JsonProperty(PropertyName = "playerTwoName")]
        public string PlayerTwoName { get; set; }

        [JsonProperty(PropertyName = "playerOneId")]
        public string PlayerOneId { get; set; }

        [JsonProperty(PropertyName = "playerTwoId")]
        public string PlayerTwoId { get; set; }

        [JsonProperty(PropertyName = "created")]
        public DateTime Created { get; set; }
    }
}