﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BattleShip.WebClient.Areas.GameHub.Models
{    
    public class PlayerGameItem : GameItemBase
    {
        [JsonProperty(PropertyName = "playerContextId")]
        public Guid PlayerContextId { get; set; }

        [JsonProperty(PropertyName = "status")]
        public int Status { get; set; }
    }
}