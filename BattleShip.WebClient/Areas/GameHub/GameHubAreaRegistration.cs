using System.Web.Mvc;
using System.Web.Http;
using BattleShip.WebClient.Areas.GameHub.Mapping;
using AutoMapper;

namespace BattleShip.WebClient.Areas.GameHub
{    
    public class GameHubAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return GameHubArea.AreaName;
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.Routes.MapHttpRoute(
                    name: "GameHubApi",
                    routeTemplate: "GameHubApi/{controller}/{id}",
                    defaults: new { id = RouteParameter.Optional }
                );

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();
        }
    }
    
}
