﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AutoMapper;
using BattleShip.Domain;
using BattleShip.WebClient.Areas.GameHub.Models;



namespace BattleShip.WebClient.Areas.GameHub.Mapping
{
    public class PlayerContextToGameItemConverter : ITypeConverter<IPlayerContext, PlayerGameItem>
    {
        protected readonly IBattleShipGame game;

        public PlayerContextToGameItemConverter(IBattleShipGame game)
        {
            this.game = game;
        }

     
        public PlayerGameItem Convert(ResolutionContext context)
        {
            IPlayerContext playerContext = (IPlayerContext)context.SourceValue;                        
            IGameInfo gameInfo = game.GetGameInfo(playerContext.GameId);

            PlayerGameItem result = new PlayerGameItem
            {
                PlayerContextId = playerContext.Id,
                Status = (int)playerContext.Status
            };
            
            Mapper.Map<IGameInfo, GameItemBase>(gameInfo, (GameItemBase)result);
            
            return result;
        }
    }
}