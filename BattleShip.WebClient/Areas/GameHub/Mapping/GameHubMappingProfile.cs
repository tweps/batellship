﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AutoMapper;
using BattleShip.Domain;
using BattleShip.WebClient.Areas.GameHub.Models;
using Microsoft.Practices.Unity;

namespace BattleShip.WebClient.Areas.GameHub.Mapping
{	
    public class GameHubMappingProfile : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "GameHubMapping";
            }
        }

		
        protected override void Configure()
        {
            
            Mapper.CreateMap<IGameInfo, GameItemBase>()
                .ForMember(dst => dst.GameName, opt => opt.ResolveUsing(src => "Game " + src.Id))
                .ForMember(dst => dst.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dst => dst.PlayerOneName, opt => opt.ResolveUsing(src => GetPlayerName(src, 0)))
                .ForMember(dst => dst.PlayerTwoName, opt => opt.ResolveUsing(src => GetPlayerName(src, 1)))
                .ForMember(dst => dst.PlayerOneId, opt => opt.ResolveUsing(src => GetPlayerId(src, 0)))
                .ForMember(dst => dst.PlayerTwoId, opt => opt.ResolveUsing(src => GetPlayerId(src, 1)))
				.ForMember(dst => dst.Created, opt => opt.MapFrom( src => src.Created))
                ;
            
			Mapper.CreateMap<IGameInfo, OpenGameItem>()
			.ConvertUsing((src) =>
				{
					var dst = (OpenGameItem)(Mapper.Map<IGameInfo, GameItemBase>(src, new OpenGameItem()));
					dst.GameId = src.Id;

					return dst;
				}
			);
						
            Mapper.CreateMap<IPlayerContext, PlayerGameItem>()
                .ConvertUsing<PlayerContextToGameItemConverter>()
                ;
        }

        private static string GetPlayerProperty(IGameInfo gameInfo, int playerIndex, Func<IPlayer,string> propertyFunc)
        {
            return gameInfo.Players.Length > playerIndex ? propertyFunc(gameInfo.Players[playerIndex]): null;
        }

        private static string GetPlayerName(IGameInfo gameInfo, int playerIndex)
        {
            return GetPlayerProperty(gameInfo, playerIndex, (p) => p.PlayerName);            
        }

        private static string GetPlayerId(IGameInfo gameInfo, int playerIndex)
        {
            return GetPlayerProperty(gameInfo, playerIndex, (p) => p.PlayerId);
        }

		private static string GetUserAvatar(IGameInfo gameInfo, int playerIndex)
		{
			return gameInfo.Players.Length > playerIndex ? gameInfo.Players[playerIndex].AvatarUrl : null;
		}

        private string TranslateGameStatus(GameStatus status)
        {
            string text;
            switch (status)
            {
                case GameStatus.NotStarted:
                    text = "Not started yet";
                    break;
                case GameStatus.ShipsArrangement:
                    text = "Ships Arrangement";
                    break;
                case GameStatus.WaitingForOpponent:
                    text = "Waiting For Second Player";
                    break;
                case GameStatus.PlayerTurn:
                    text = "Your Turn";
                    break;
                case GameStatus.OpponentTurn:
                    text = "Opponent Turn";
                    break;
                case GameStatus.PlayerHasLoseTheMatch:
                    text = "Game Ended - You Lost";
                    break;
                case GameStatus.PlayerHasWinTheMatch:
                    text = "Game Ended - You Won";
                    break;
                default:
                    text = status.ToString();
                    break;
            }

            return text;
        }
    }
}