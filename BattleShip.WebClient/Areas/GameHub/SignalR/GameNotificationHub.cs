﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BattleShip.WebClient.Extensions;
using BattleShip.WebClient.Extensions.SignalR;
using Microsoft.AspNet.SignalR;
using BattleShip.Domain;
using AutoMapper;
using BattleShip.WebClient.Areas.GameHub.Models;
using Newtonsoft.Json;
using Microsoft.AspNet.SignalR.Hubs;

namespace BattleShip.WebClient.Areas.GameHub.SignalR
{
    [HubName("gameNotificationHub")]
    public class GameNotificationHub : BattleShipSignalRHub
    {
        private const string gameNotificationGroupName = "gameNotificationGroupName";

        private readonly IMappingEngine mapper;

        public GameNotificationHub(IMappingEngine mapper, IBattleShipGame game)
        {
            this.mapper = mapper;
            game.PublicGameOpned += OnPublicGameOpned;
            game.PublicGameClosed += OnPublicGameClosed;
        }
       
        private void OnPublicGameOpned(object sender, PublicGameEventArgs e)
        {
            this.Clients.All.Add(GetJsonDataToSend(e));         
        }

        private void OnPublicGameClosed(object sender, PublicGameEventArgs e)
        {
            this.Clients.All.Remove(GetJsonDataToSend(e));
        }

        private string GetJsonDataToSend(PublicGameEventArgs e)
        {
            var data = mapper.Map<IGameInfo, OpenGameItem>(e.PublicGame);
            string json = JsonConvert.SerializeObject(data);

            return json;
        }

        public void SubscribeGameNotification()
        {
            this.Groups.Add(this.Context.ConnectionId, gameNotificationGroupName);
        }

        public void UnubscribeGameNotification()
        {
            this.Groups.Remove(this.Context.ConnectionId, gameNotificationGroupName);
        }         
    }
};