﻿using BattleShip.Domain;
using BattleShip.WebClient.Areas.Game.Routing;
using System.Web.Mvc;

namespace BattleShip.WebClient.Areas.Game
{
    public class GameAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return GameArea.AreaName;
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            var planningStates = new[] { GameStatus.NotStarted, GameStatus.ShipsArrangement };
            var shootingStates = new[] { GameStatus.WaitingForOpponent, GameStatus.PlayerTurn, GameStatus.OpponentTurn, GameStatus.PlayerHasLoseTheMatch, GameStatus.PlayerHasWinTheMatch };

            context.MapRoute("Game_1",
                 "Game/{controller}/{action}/{id}",
                  new { action = "Index" }
            );

            var game = DependencyResolver.Current.GetService<IBattleShipGame>();

            context.MapRoute("Game_2",
                             "Game/{id}",
                             new { controller = "planning", action = "Index", id = UrlParameter.Optional },
                             new { id = new GameStatusRouteConstraint(game) { Accepted = planningStates } }
            );

            context.MapRoute("Game_3",
                             "Game/{id}",
                             new { controller = "shooting", action = "Index", id = UrlParameter.Optional },
                             new { id = new GameStatusRouteConstraint(game) { Accepted = shootingStates } }
            );
        }
    }
}