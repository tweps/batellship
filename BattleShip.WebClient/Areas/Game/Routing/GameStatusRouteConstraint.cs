﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using BattleShip.Domain;

namespace BattleShip.WebClient.Areas.Game.Routing
{
	public class GameStatusRouteConstraint : IRouteConstraint
	{
		protected readonly IBattleShipGame gameManager;

        public GameStatusRouteConstraint(IBattleShipGame gameManager)
		{
            this.gameManager = gameManager;
		}
		
		public IEnumerable<GameStatus> Accepted { get; set; }

		public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
		{
			Guid contextId;
			if(Guid.TryParse(values[parameterName].ToString(), out contextId))
			{				 
				IPlayerContext playerContext = gameManager.Get(contextId);

				if (playerContext != null && Accepted.Contains(playerContext.Status))
				{
					return true;
				}
			}
			return false;
		}
	}
}