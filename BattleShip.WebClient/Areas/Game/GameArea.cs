﻿using BattleShip.WebClient.Areas.Game.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BattleShip.WebClient.Areas.Game
{	
    public class GameArea
    {
		public static readonly string PlayerContextParameterName = "playerContext";
        public static readonly string AreaName = "Game";
        public static readonly string GameEndPointName = "gameHub";

        
        static GameArea()
        {  
            
            var attValues = typeof(BattleShipHub).GetCustomAttributes(typeof(Microsoft.AspNet.SignalR.Hubs.HubNameAttribute), false);
            if (attValues.Length > 0)
            {
                GameEndPointName = ((Microsoft.AspNet.SignalR.Hubs.HubNameAttribute)attValues[0]).HubName;
            }
           
        }
    }	
}

