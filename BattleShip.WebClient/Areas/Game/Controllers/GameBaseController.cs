﻿using BattleShip.WebClient.Areas.Game.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BattleShip.WebExtensions.Errors;

namespace BattleShip.WebClient.Areas.Game.Controllers
{
    [Authorize]
    [PlayerContextCheck]
    [FormatHandleError]    
    public partial class GameBaseController : Controller
    {
    }
}