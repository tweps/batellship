﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using AutoMapper;

using BattleShip.Domain;
using BattleShip.WebClient.Areas.Game.Filters;
using BattleShip.WebClient.Areas.Game.Models;
using BattleShip.WebClient.Areas.Game.Routing;
using BattleShip.WebClient.Extensions.Filters;


namespace BattleShip.WebClient.Areas.Game.Controllers
{	   
	
    [ViewModelLocatorData("Game_Planning")]
    public partial class PlanningController : GameBaseController
    {
		private readonly IMappingEngine mapper;
        private readonly IGameDefinitions gameDefinitions;

        public PlanningController(IMappingEngine mapper, IGameDefinitions gameDefinitions)
		{
			this.mapper = mapper;
            this.gameDefinitions = gameDefinitions;
		}


		public virtual ActionResult Index(IPlayerContext playerContext)
        {
            return View(GetModelData(playerContext));
        }

        public virtual ActionResult IndexJson(IPlayerContext playerContext)
        {            
            return Json(GetModelData(playerContext), JsonRequestBehavior.AllowGet);
        }


        private GamePlanningModel GetModelData(IPlayerContext playerContext)
        {
			GamePlanningModel model = mapper.Map<IPlayerContext, GamePlanningModel>(playerContext);
            model.ShipsDefinitions = mapper.Map<IEnumerable<ShipDefinition>, IEnumerable<ShipDefinitionModel>>(gameDefinitions.ShipsDefinitions);

            return model;
        }


        [HttpPost]
        public virtual ActionResult SubmitShipPlacement(IPlayerContext playerContext, GamePlanningSubmitModel planningBoard)
        {            
            // to do - transaction / locking
		
            foreach (var proposeShip in planningBoard.ships)
            {
                var ship = playerContext.PlayerBoard.ConstructShip(proposeShip.x, proposeShip.y, proposeShip.masts, proposeShip.direction);
                
                if (!playerContext.PlayerBoard.CanAddShip(ship))
                {
                    throw new ArgumentException("Ships are breaking allowed rules");
                }
                
                playerContext.PlayerBoard.Add(ship);                
            }

            playerContext.PlayerBoard.ConfirmShipPlacement();            
            return Redirect("/game/shooting/index/" + playerContext.Id);
        }

    }
}
