﻿using BattleShip.Domain;
using BattleShip.WebClient.Areas.Game.Filters;
using BattleShip.WebClient.Areas.Game.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BattleShip.WebClient.Areas.Game.Routing;
using AutoMapper;
using BattleShip.WebClient.Extensions.Filters;

namespace BattleShip.WebClient.Areas.Game.Controllers
{    
    [ViewModelLocatorData("Game_Shooting")]
    public partial class ShootingController : GameBaseController
    {
		private readonly IMappingEngine mapper;

		public ShootingController(IMappingEngine mapper)
		{
			this.mapper = mapper;
		}

		public virtual ActionResult Index(IPlayerContext playerContext)
        {
			var model = mapper.Map<IPlayerContext, GameShootingModel>(playerContext);
            return View(model);
        }

		public virtual ActionResult IndexJson(IPlayerContext playerContext)
        {
			var model = mapper.Map<IPlayerContext, GameShootingModel>(playerContext);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

		[HttpPost]
		public virtual ActionResult Shoot(IPlayerContext playerContext, int x, int y)
		{
            var boardState = playerContext.OponentBoard.Shoot(x, y);
			
			var model = mapper.Map<IPlayerContext, GameShootingModel>(playerContext);
            return Json(model);			
		}
    }
}
