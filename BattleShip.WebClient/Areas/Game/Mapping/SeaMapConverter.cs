﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AutoMapper;
using BattleShip.Domain;
using BattleShip.WebClient.Areas.Game.Models;

namespace BattleShip.WebClient.Areas.Game.Mapping
{
    public class SeaMapConverter : ITypeConverter<ISeaMap, SeaMapModel>
    {
        public SeaMapModel Convert(ResolutionContext context)
        {           
            ISeaMap src = (ISeaMap)context.SourceValue;
            SeaMapModel dst = new SeaMapModel();

            dst.Pieces = new int[src.SizeX][];
            for (int x = 0; x < src.SizeX; x++)
            {
                dst.Pieces[x] = new int[src.SizeY];
                for (int y = 0; y < src.SizeY; y++)
                {
                    dst.Pieces[x][y] = (int)src[x, y].State;
                }
            }

            if (src.LastShoot != null)
            {
                dst.LastShoot = new Tuple<int, int>(src.LastShoot.PositionX, src.LastShoot.PositionY);
            }

            return dst;
        }
    }
}