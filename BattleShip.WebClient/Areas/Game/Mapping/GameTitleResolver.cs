﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AutoMapper;
using BattleShip.Domain;

namespace BattleShip.WebClient.Areas.Game.Mapping
{
    public class GameTitleResolver : ValueResolver<IPlayerContext, string>
    {
        private IBattleShipGame _game;

        public GameTitleResolver(IBattleShipGame game)
        {
            this._game = game;
        }

        protected override string ResolveCore(IPlayerContext source)
        {
            var gameInfo = _game.GetGameInfo(source.GameId);

            return gameInfo.Description;            
        }
    }
}