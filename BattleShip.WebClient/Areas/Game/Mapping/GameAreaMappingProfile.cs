﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using AutoMapper;
using BattleShip.Domain;
using BattleShip.WebClient.Areas.Game.Models;

namespace BattleShip.WebClient.Areas.Game.Mapping
{
    public class GameAreaMappingProfile : Profile
    {
        public override string ProfileName
        {
            get
            {
                return "GameAreaMapping";
            }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<ISeaMap, SeaMapModel>().ConvertUsing<SeaMapConverter>();

            Mapper.CreateMap<IPlayerContext, GamePlanningModel>()
               .ForMember(dest => dest.PlayerContextId, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.GameTitle, opt => opt.ResolveUsing<GameTitleResolver>())
               .ForMember(dest => dest.PlayerSeaMap, opt => opt.MapFrom(src => src.PlayerBoard.SeaMap))
               .ForMember(dest => dest.ShipsDefinitions, opt => opt.Ignore())
               ;

            Mapper.CreateMap<IPlayerContext, GameShootingModel>()
               .ForMember(dest => dest.PlayerContextId, opt => opt.MapFrom(src => src.Id))
               .ForMember(dest => dest.GameTitle, opt => opt.ResolveUsing<GameTitleResolver>())
               .ForMember(dest => dest.PlayerSeaMap, opt => opt.MapFrom(src => src.PlayerBoard.SeaMap))
               .ForMember(dest => dest.OpponentSeaMap, opt => opt.MapFrom(src => src.OponentBoard.SeaMap))
               ;

            Mapper.CreateMap<ShipDefinition, ShipDefinitionModel>();
        }
    }
}