﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using BattleShip.Domain;
using BattleShip.WebClient.Authentication;


namespace BattleShip.WebClient.Areas.Game.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class PlayerContextCheckAttribute : ActionFilterAttribute//, IAuthenticationFilter
    {            	
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {        
            if (!filterContext.ActionParameters.ContainsKey(GameArea.PlayerContextParameterName))
            {
                throw new InvalidOperationException("missing " + GameArea.PlayerContextParameterName + " parameter");
            }

            IPlayerContext context = filterContext.ActionParameters[GameArea.PlayerContextParameterName] as IPlayerContext;            
            if (context == null)
            {
                throw new InvalidOperationException();
            }

            IPlayer currentPlayer = PlayerIdentity.Create(filterContext.HttpContext.User);
            if (!currentPlayer.Equals(context.Player))
            {
                throw new SecurityException("Player is not allowed to manage this game");
            }            
        }

       
    }
}