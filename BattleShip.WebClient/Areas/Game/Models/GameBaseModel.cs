﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace BattleShip.WebClient.Areas.Game.Models
{
    public abstract class GameBaseModel
    {
        public Guid PlayerContextId { get; set; }

        public int Status { get; set; }

        public string GameTitle { get; set; }

    }
}