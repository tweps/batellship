﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BattleShip.WebClient.Areas.Game.Models
{        
    public class ShipDefinitionModel
    {       
        public int Masts { get; set; }
        
        public int AmountOfShips { get; set; }
    }
}