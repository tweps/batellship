﻿using BattleShip.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BattleShip.WebClient.Areas.Game.Models
{
    public class GamePlanningModel : GameBaseModel
    {
        [DisplayName("Your board")]        
        public SeaMapModel PlayerSeaMap { get; set; }
                   
        [DisplayName("Ships to add")]        
        public IEnumerable<ShipDefinitionModel> ShipsDefinitions { get; set; }
    }
}