﻿using BattleShip.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BattleShip.WebClient.Areas.Game.Models
{    
    public class SeaMapModel
    {                
        public int[][] Pieces { get; set; } 
        public Tuple<int,int> LastShoot {get; set; }
    }    
}