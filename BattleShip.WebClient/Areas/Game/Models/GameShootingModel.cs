﻿using BattleShip.Domain;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace BattleShip.WebClient.Areas.Game.Models
{
    public class GameShootingModel : GameBaseModel
    {
        [DisplayName("Your board")]
        public SeaMapModel PlayerSeaMap { get; set; }

        [DisplayName("Opponent board")]
        public SeaMapModel OpponentSeaMap { get; set; }
    }
}