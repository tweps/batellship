﻿using BattleShip.Domain;
using BattleShip.WebExtensions.JsonBindable;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BattleShip.WebClient.Areas.Game.Models
{
    public class ShipPositionModel
    {
        //[JsonProperty]
        public int x { get; set; }
        public int y  { get; set; }
        public int masts  { get; set; }
        public ShipDirection direction { get; set; }
    }

    [JsonPropertyModelBinder]
    public class GamePlanningSubmitModel
    {        
        public IEnumerable<ShipPositionModel> ships { get; set; }
    }
    
}