﻿using BattleShip.Domain;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace BattleShip.WebClient.Areas.Game.ModelBinders
{
    public class GameModelBinderProvider : IModelBinderProvider
    {		
        private static readonly Type playerContextType = typeof(IPlayerContext);
        private readonly IUnityContainer container;

        public GameModelBinderProvider(IUnityContainer container)
        {
            this.container = container; 
        }
                                
        public IModelBinder GetBinder(Type modelType)
        {
            if (modelType == playerContextType)
            {
                return container.Resolve<PlayerContextModelBinder>();                
            }
            
            return null;         
        }
        
    }
}