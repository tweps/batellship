﻿using BattleShip.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BattleShip.WebClient.Areas.Game.ModelBinders
{
    public class PlayerContextModelBinder : IModelBinder
    {
        protected readonly IBattleShipGame gameManager;

		public PlayerContextModelBinder(IBattleShipGame gameManager)
		{
			this.gameManager = gameManager;
		}

        
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            IPlayerContext playerContext = null;

            if (controllerContext.RouteData.Values.ContainsKey("id"))
            {
                Guid contextId;
                string contextIdValue = controllerContext.RouteData.Values["id"].ToString();
                if (Guid.TryParse(contextIdValue, out contextId))
                {
                    playerContext = gameManager.Get(contextId);
                }
            }

            return playerContext;
        }
    }
}