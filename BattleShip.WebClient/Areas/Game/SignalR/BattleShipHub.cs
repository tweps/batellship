using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using BattleShip.WebClient.Extensions;
using BattleShip.WebClient.Extensions.SignalR;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using BattleShip.Domain;
using Newtonsoft.Json;
using AutoMapper;
using BattleShip.WebClient.Areas.Game.Models;
using System.Web.Mvc;


namespace BattleShip.WebClient.Areas.Game.Hubs
{   
    [HubName("battleShipHub")]
    public class BattleShipHub : BattleShipSignalRHub
    {
        private readonly IBattleShipGame _game;

        private readonly Dictionary<IPlayer, string> registedPlayers = new Dictionary<IPlayer, string>();
        private readonly Dictionary<IPlayerContext, string> contextConnectionGrp = new Dictionary<IPlayerContext, string>();

        public BattleShipHub(IBattleShipGame game)
        {
            this._game = game;
            this._game.PlayerHasNewGame += OnPlayerHasNewGame;
        }

        private void OnPlayerHasNewGame(object sender, PlayerHasNewGameEventArgs e )
        {
            RegisterPlayerContext(e.Player, e.PlayerContext);
            OnPlayerContextRefreshNotification(e.PlayerContext,null);
        }

        private void RegisterPlayerContext(IPlayer player, IPlayerContext playerContext)
        {
            if (registedPlayers.ContainsKey(player))
            {
                contextConnectionGrp.Add(playerContext, registedPlayers[player]);
                playerContext.RefreshNotification += OnPlayerContextRefreshNotification;
            }
        }

        private void OnPlayerContextRefreshNotification(object sender, EventArgs e)
        {
            Debug.Assert(sender != null);

            IPlayerContext playerContext = sender as IPlayerContext;

            if (playerContext == null)
            {
                throw new ArgumentException("Sender isn't correct type:" + sender.GetType().FullName);
            }

            string connectionGrp;
            if(contextConnectionGrp.TryGetValue(playerContext, out connectionGrp))
            {
                var result = Mapper.Map<IPlayerContext, GameShootingModel>(playerContext);
                var data = JsonConvert.SerializeObject(result);

                this.Clients.Group(connectionGrp).Update(playerContext.Id, data);
            }  
        }

        public override Task OnConnected()
        {           
            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            //RegisterPlayer();
            return base.OnReconnected();
        }

        public override Task OnDisconnected()
        {
            UnRegisterPlayer();
            return base.OnDisconnected();
        }

        
        public void RegisterPlayer(string authorization)
        {
            IPlayer player = GetPlayer(authorization);

            if(player==null)
            {
                return;
            }

            string connectionGrp;
            // TO DO - not thread safe
            if(!registedPlayers.TryGetValue(player, out connectionGrp ))
            {
                connectionGrp = "playerGr_" + Guid.NewGuid().ToString();
                registedPlayers.Add(player, connectionGrp);

                _game.GetAllUserContexts(player).Each((playerContext) => RegisterPlayerContext(player, playerContext));
            }
                       
            this.Groups.Add(this.Context.ConnectionId, connectionGrp);                                                
        }

        public void UnRegisterPlayer()
        {
            // to do cleanup
        }

     
    }
}