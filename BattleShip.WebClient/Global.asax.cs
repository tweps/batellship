﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.Practices.Unity;
using Elmah;

namespace BattleShip.WebClient
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            IUnityContainer container = UnityConfig.Init();
            MapperConfig.Initialize(container);
            
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);            
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);                       
            ModelBinderConfig.Register(ModelBinderProviders.BinderProviders, ModelBinders.Binders, container);            
        }

        private void ErrorLog_Filtering(object sender, ExceptionFilterEventArgs e)
        {
            if (e.Exception is System.Threading.ThreadAbortException)
            {
                e.Dismiss();
            }
        }
    }
}
