﻿using System.Web.Mvc;

namespace BattleShip.WebClient.Extensions.Filters
{
    public class ViewModelLocatorData : ActionFilterAttribute, IResultFilter 
    {
        public string ViewName { get; set; }

        public ViewModelLocatorData(string viewName)
        {
            this.ViewName = viewName;
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
             base.OnResultExecuting(filterContext);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {           
            filterContext.HttpContext.Response.AppendHeader("ViewName", ViewName);
            base.OnResultExecuted(filterContext);
        }
    }
}