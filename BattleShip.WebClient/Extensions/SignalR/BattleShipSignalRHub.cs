﻿using BattleShip.Domain;
using BattleShip.WebClient.Authentication;
using Microsoft.AspNet.SignalR;

namespace BattleShip.WebClient.Extensions.SignalR
{
    public class BattleShipSignalRHub : Hub
    {
        protected IPlayer GetPlayer(string authorization)
        {
            IPlayer player = null;

            const string bearerPrefix = "Bearer ";
            if (!string.IsNullOrEmpty(authorization) && authorization.StartsWith(bearerPrefix))
            {
                string token = authorization.Remove(0, bearerPrefix.Length);
                var ticket = Startup.OAuthOptions.AccessTokenFormat.Unprotect(token);

                if (ticket.Identity.IsAuthenticated)
                {
                    player = PlayerIdentity.Create(ticket.Identity.Claims);
                }
            }

            return player;
        }
    }
}