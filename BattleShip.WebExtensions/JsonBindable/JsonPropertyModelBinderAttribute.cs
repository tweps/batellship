﻿using System.Web.Mvc;

namespace BattleShip.WebExtensions.JsonBindable
{
    public class JsonPropertyModelBinderAttribute : CustomModelBinderAttribute
    {
        public override IModelBinder GetBinder()
        {
            return new JsonPropertyModelBinder();
        }
    }
}