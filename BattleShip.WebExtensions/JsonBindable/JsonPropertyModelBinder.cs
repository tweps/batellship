﻿using System.ComponentModel;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace BattleShip.WebExtensions.JsonBindable
{
    public class JsonPropertyModelBinder : DefaultModelBinder
    {
        protected override object GetPropertyValue(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor, IModelBinder propertyBinder)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName).AttemptedValue;
            var result = JsonConvert.DeserializeObject(value, propertyDescriptor.PropertyType);
            return result;
        }
        
    }
}