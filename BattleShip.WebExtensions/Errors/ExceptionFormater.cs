﻿using System;
using BattleShip.Domain.Exceptions;

namespace BattleShip.WebExtensions.Errors
{
    public class ExceptionFormater
    {
        public static ClientSideExceptionData Format(Exception exception)
        {
            if (exception is BattleShipException)
            {
                return new ClientSideExceptionData 
                {
                    Content = (((BattleShipException)exception).Message),
                    ReasonPhrase = "BattleShipExcpetion"
                };
            }

            
            return new ClientSideExceptionData
                {
                    Content = "A server error occurred. Please try again or press 'reload' in your browser.",
                    ReasonPhrase = "Internal Exception"
                };
            
        }
        
        public class ClientSideExceptionData
        {
            public string Content { get; set; }
            public string ReasonPhrase { get; set; }
        }

    }
}