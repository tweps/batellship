﻿using System.Web.Mvc;

namespace BattleShip.WebExtensions.Errors
{
    public class FormatHandleErrorAttribute : System.Web.Mvc.HandleErrorAttribute
    {
        public override void OnException(System.Web.Mvc.ExceptionContext filterContext)
        {
            if(filterContext.ExceptionHandled)
            {
                return;
            }

            var clientError = ExceptionFormater.Format(filterContext.Exception);
            //filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.InternalServerError, clientError.Content);
            filterContext.Result = new ContentResult()
            {
                Content = clientError.Content,
                ContentType = "text/plain"
            };
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = 500;
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;

            filterContext.ExceptionHandled = true;
        }
    }
}