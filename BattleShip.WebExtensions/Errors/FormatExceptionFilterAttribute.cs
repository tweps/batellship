﻿using System.Net;
using System.Net.Http;

namespace BattleShip.WebExtensions.Errors
{
    public class FormatExceptionFilterAttribute : System.Web.Http.Filters.ExceptionFilterAttribute
    {
        public override void OnException(System.Web.Http.Filters.HttpActionExecutedContext context)
        {
            var clientError = ExceptionFormater.Format(context.Exception);

            context.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new StringContent(clientError.Content),
                ReasonPhrase = clientError.ReasonPhrase
            };
        }
    }
}