﻿using System;

namespace BattleShip.WebExtensions.JsExceptions
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable"), Serializable]
    public class JavaScriptException : Exception
    {
        public JavaScriptException() : base()
        {

        }

        public JavaScriptException(string meassage) : base(meassage)
        {

        }
    }
}
