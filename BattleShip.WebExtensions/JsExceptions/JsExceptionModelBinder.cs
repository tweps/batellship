﻿using System.IO;
using System.Web.Mvc;

namespace BattleShip.WebExtensions.JsExceptions
{
    public class JsExceptionModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {            
            string body;
            using (var stream = new StreamReader(controllerContext.HttpContext.Request.InputStream))
            {
                body = stream.ReadToEnd();
            }

            return new JavaScriptException(body);
        }
    }
}

