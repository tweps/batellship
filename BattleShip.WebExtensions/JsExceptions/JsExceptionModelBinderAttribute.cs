﻿using System;
using System.Web.Mvc;

namespace BattleShip.WebExtensions.JsExceptions
{
    [AttributeUsage(AttributeTargets.Parameter)]
    public class JsExceptionModelBinderAttribute : CustomModelBinderAttribute
    {
        public override IModelBinder GetBinder()
        {
            return new JsExceptionModelBinder();
        }
    }
}