﻿using System.Web.Mvc;

namespace BattleShip.WebExtensions.Seo
{
    public class HashBangBinder : IModelBinder
    {
        private const string hashBangParameterName = "_escaped_fragment_";

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            string value = controllerContext.HttpContext.Request.Params.Get(hashBangParameterName);
            if (!string.IsNullOrEmpty(value))
            {
                return new HashBang(value);
            }

            return null;
        }
    }
}