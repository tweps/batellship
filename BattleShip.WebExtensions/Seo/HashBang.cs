﻿namespace BattleShip.WebExtensions.Seo
{
    /// <summary>
    /// the crawler (google roobots) is modify each AJAX URL such as
    /// www.example.com/ajax.html#!key=value
    /// to
    /// www.example.com/ajax.html?_escaped_fragment_=key=value
    /// 
    /// This class is returning value of assigned to _escaped_fragment_
    /// </summary>
    public class HashBang
    {
        public HashBang(string value)
        {
            this.Value = value;
        }

        public string Value { get; private set; }
    }
}