﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocatorTemplate xmlns:vm="using:ProjectForTemplates.ViewModel"
                                   x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"
*/

using System;
using Microsoft.Practices.ServiceLocation;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;

using BattleShip.Common;
using BattleShip.ViewModel.Board;
using BattleShip.ViewModel.ShipSelector;
using BattleShip.ViewModel.Messeges;

using BattleShip.Domain;
using BattleShip.Domain.Repositories;
using BattleShip.Domain.Game;
using BattleShip.ViewModel.Pages;
using BattleShip.UILogic;
using BattleShip.DesignViewModels;


namespace BattleShip.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            ISimpleIoc ioc = SimpleIoc.Default;
           
            /* Domain */
            ioc.Register<IGame>(() => new PlayerContextRepository(), true);
            ioc.Register<IGameDefinitions>(() => GameDefinitions.Instance);

            /* Model */
            INavigationService navigationService = new NavigationService();
            ioc.Register<INavigationService>(()=>navigationService);

            ioc.Register<MessegeToken>();

            
            if (ViewModelBase.IsInDesignModeStatic)
            {
                /* Sub Views */
                ioc.Register<BoardViewModel>(() => new BoardDesignViewModel());
                ioc.Register<ShipSelectorViewModel>(()=> new ShipSelectorDesignViewModel());

                /* Pages */
                ioc.Register<GamePlaningPageViewModel>(()=>new GamePlaningPageDesignViewModel());
                ioc.Register<GameShootingPageViewModel>(()=>new GameShootingPageDesignViewModel());
                ioc.Register<HubPageViewModel>(()=>new HubPageDesignViewModel());
            }
            else
            {
                /* Pages */
                ioc.Register<GamePlaningPageViewModel>();
                ioc.Register<GameShootingPageViewModel>();
                ioc.Register<HubPageViewModel>();                
            }
        }

                
        public ShipSelectorViewModel ShipSelector
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ShipSelectorViewModel>();
            }
        }
        
        public BoardViewModel Board
        {
            get
            {
                return ServiceLocator.Current.GetInstance<BoardViewModel>();
            }
        }

        
        public GamePlaningPageViewModel GamePlaning
        {
            get
            {
                return ServiceLocator.Current.GetInstance<GamePlaningPageViewModel>();
            }
        }

       
        public GameShootingPageViewModel GameShooting
        {
            get
            {
                return SimpleIoc.Default.GetInstance<GameShootingPageViewModel>();
            }
        }
        

        public HubPageViewModel GameList
        {
            get
            {
                return SimpleIoc.Default.GetInstance<HubPageViewModel>();
            }
        }

        
        public static INavigationService Navigation
        {
            get
            {
                return ServiceLocator.Current.GetInstance<INavigationService>();
            }
        }
       
        /// <summary>
        /// Cleans up all the resources.
        /// </summary>
        public static void Cleanup()
        {
        }
    }
}