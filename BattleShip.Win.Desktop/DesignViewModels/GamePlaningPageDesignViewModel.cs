﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BattleShip.ViewModel.Board;
using BattleShip.ViewModel.Pages;
using BattleShip.Domain.Board;


namespace BattleShip.DesignViewModels
{
    public class GamePlaningPageDesignViewModel : GamePlaningPageViewModel
    {
        public GamePlaningPageDesignViewModel() : base(null)
        {
            PlaningSectionVisible = true;
            PlayerBoard = new PlaningBoardDesignViewModel();
            ShipSelector = new ShipSelectorDesignViewModel();
        }
    }


    public class PlaningBoardDesignViewModel : PlaningBoardViewModel
    {
        public PlaningBoardDesignViewModel()
            : base(new PlayerBoard(null, Fakes.SeaMap), null)
        {
           
        }
    }    
}
