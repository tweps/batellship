﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BattleShip.Domain.Map;
using BattleShip.Domain;
using BattleShip.Domain.Game;
using BattleShip.Domain.Identifications;

namespace BattleShip.DesignViewModels
{
    public static class Fakes
    {
        public static ISeaMapEdit SeaMap
        {
            get
            {
                return new SeaMapFake();
            }
        }

        public static IPlayerContext Context
        {
            get
            {
                return new PlayerContext
                {                    
                    Player = new Player { PlayerName = "Player One" },
                    Oponent = new Player { PlayerName = "Player Two" },                    
                };                
            }
        }

        public static IGame Respository
        {
            get
            {
                return new PlayerContextRepositoryFake();
            }
        }

        
        private class SeaMapFake : SeaMap
        {
            public SeaMapFake()
            {
                this.Add(ShipFabric.Instance.Construct(2, 2, 3, ShipDirection.East).Masts);
                this.Add(ShipFabric.Instance.Construct(5, 5, 2, ShipDirection.South).Masts);

                this.Shoot(3, 2);
                this.Shoot(5, 5);
                this.Shoot(5, 6);
                this.Shoot(5, 7);
                this.Shoot(8, 8);
            }
        }


        private class PlayerContextRepositoryFake : IGame
        {
            public IPlayerContext Get(Guid contextId)
            {
                return null;
            }

            public IPlayerContext MatchMake(IPlayer player)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<IPlayerContext> GetAllUserContexts(IPlayer player)
            {
                return new[] 
                { 
                    new PlayerContext
                    {
                        Player = new Player { PlayerName = "Player One" },
                        Oponent = new Player { PlayerName = "Player Two" },
                    }
                 };
            }

            public IEnumerable<IGameInfo> GetFreeToJoin(IPlayer player)
            {
                return new[] {
                    new GameInfo
                    {
                        Description = "Player One is waiting for oponent"
                    }
                };
            }

            public IPlayerContext JoinToGame(Guid id, IPlayer player)
            {
                throw new NotImplementedException();
            }

            public IGameInfo GetGameInfo(Guid gameId)
            {
                return new GameInfo
                {
                    Created = DateTime.Now,
                    Description = "Player One vs Player Two"
                };
            }
        }


    }
}
