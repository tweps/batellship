﻿using BattleShip.UILogic.Model;
using BattleShip.ViewModel.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.DesignViewModels
{
    public class HubPageDesignViewModel : HubPageViewModel
    {
        public HubPageDesignViewModel()
            : base(null, Fakes.Respository)
        {
            LoadContentAsync();
        }
    }    
}
