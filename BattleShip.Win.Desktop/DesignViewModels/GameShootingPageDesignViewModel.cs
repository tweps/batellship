﻿using BattleShip.Domain.Board;
using BattleShip.ViewModel.Board;
using BattleShip.ViewModel.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.DesignViewModels
{
    public class GameShootingPageDesignViewModel : GameShootingPageViewModel
    {
        public GameShootingPageDesignViewModel() : base()
        {
            this.context = Fakes.Context;
            this.context.UpdateStatus(Domain.GameStatus.PlayerTurn);

            PlayerBoard = new PlayerBoardDesignViewModel();
            OponentBoard = new OponentBoardDesignViewModel();

            Refresh();
        }
    }

    public class PlayerBoardDesignViewModel : PlayerBoardViewModel
    {
        public PlayerBoardDesignViewModel() : base( new PlayerBoard(null, Fakes.SeaMap))
        {
        }
    }

    public class OponentBoardDesignViewModel : OponentBoardViewModel
    {
        public OponentBoardDesignViewModel() : base( new OponentBoard(null, Fakes.SeaMap))
        {
        }
    }


}
