﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BattleShip.Domain;
using BattleShip.Domain.Map;
using BattleShip.ViewModel.Board;
using BattleShip.Domain.Map.Pieces;

namespace BattleShip.DesignViewModels
{
    public class BoardDesignViewModel : BoardViewModel
    {
        public BoardDesignViewModel()
            : base(Fakes.SeaMap)
        {
            if (!IsInDesignMode)
            {
                throw new InvalidOperationException("This class is allowed to use only in design mode");
            }

            
            this.BoardPieces[21].Highlighting = UILogic.Model.HighlightingType.NotAllowed;
            this.BoardPieces[22].Highlighting = UILogic.Model.HighlightingType.NotAllowed;
            this.BoardPieces[23].Highlighting = UILogic.Model.HighlightingType.NotAllowed;

            this.BoardPieces[41].Highlighting = UILogic.Model.HighlightingType.Allowed;
            this.BoardPieces[42].Highlighting = UILogic.Model.HighlightingType.Allowed;
            this.BoardPieces[43].Highlighting = UILogic.Model.HighlightingType.Allowed;
            
        }       
    }   
}
