﻿using BattleShip.UILogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace BattleShip.Controls
{
    public class GameItemTemplateSelector : DataTemplateSelector
    {
        public DataTemplate ItemTwoImagesDataTemplate { get; set; }
        public DataTemplate ItemOneImageDataTemplate { get; set; }
        public DataTemplate ItemSymbolDataTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            if (item is GameItemTwoImages)
            {
                return ItemTwoImagesDataTemplate;
            }

            if (item is GameItemOneImage)
            {
                return ItemOneImageDataTemplate;
            }

            return ItemSymbolDataTemplate;
        }
    }
}
