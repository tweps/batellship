﻿using BattleShip.UILogic.Model;
using BattleShip.ViewModel;
using BattleShip.ViewModel.Board;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace BattleShip.ValueConverters
{
    public class BoardPieceHighlightingConverter : IValueConverter
    {
        
        public Brush NormalPiece { get; set; }
        public Brush HighlightedFirstLevel { get; set; }
        public Brush HighlightedSecondLevel { get; set; }

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            HighlightingType highlighting = (HighlightingType)value;

            Brush scb;

            switch (highlighting)
            {
                case HighlightingType.NotAllowed:
                    scb = HighlightedSecondLevel;
                    break;

                case HighlightingType.Allowed:
                    scb = HighlightedFirstLevel;
                    break;

                default:
                    scb = NormalPiece;
                    break;
            }

            return scb;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
