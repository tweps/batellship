﻿using BattleShip.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace BattleShip.ValueConverters
{
    public class BoardStateToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            BoardState state = (BoardState)value;

            string result = null;
            switch (state)
            {
                case BoardState.NotHit:
                    result = "\uE003";
                    break;

                case BoardState.Hit:
                    result = "\uE002";
                    break;

                case BoardState.HitAndSunken:
                    result = "\uE005";
                    break;

                case BoardState.Mishit:
                    result = "\uE072";
                    break;

                case BoardState.Unknown:
                    result = null;
                    break;

                default:
                    throw new InvalidOperationException("Unknown value " + state);
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
