﻿using BattleShip.UILogic.ViewModel.Pages;
using BattleShip.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace BattleShip.Views
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();
            this.mainPageFrame.Navigated += mainPageFrame_Navigated;            
        }
        
        /// Application wide Frame control to be used in Navigation Service
        public Frame AppFrame
        {
            get
            {
                return this.mainPageFrame;                
            }
        }

        void mainPageFrame_Navigated(object sender, NavigationEventArgs e)
        {
            var page = e.Content as Page;
            if (page != null && page.DataContext != null)
            {
                this.DataContext = page.DataContext;
            }                   
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {            
            this.mainPageFrame.Navigate(typeof(BattleShip.Views.HubPage));
        }

        private void OnBackButtonClick(object sender, RoutedEventArgs e)
        {                        
            if (mainPageFrame.CanGoBack)
            {
                mainPageFrame.GoBack();
            }            
        }
    }
}
