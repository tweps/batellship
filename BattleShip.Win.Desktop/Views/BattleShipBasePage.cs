﻿using BattleShip.Common;
using BattleShip.UILogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.Views
{
    [Windows.Foundation.Metadata.WebHostHidden]
    public class BattleShipBasePage : LayoutAwarePage
    {
        protected override void LoadState(object navigationParameter, Dictionary<string, object> pageState)
        {
            base.LoadState(navigationParameter, pageState);

            INavigable navigableViewModel = this.DataContext as INavigable;
            if (navigableViewModel != null)
            {
                navigableViewModel.Activate(navigationParameter, pageState);
            }
        }

        protected override void SaveState(Dictionary<string, object> pageState)
        {
            base.SaveState(pageState);

            INavigable navigableViewModel = this.DataContext as INavigable;
            if (navigableViewModel != null)
            {
                navigableViewModel.Deactivate(pageState);
            }
        }
    }
}
