﻿using BattleShip.UILogic;
using BattleShip.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace BattleShip.Common
{
    public class NavigationService : INavigationService
    {
        private Frame _mainFrame;

        //public event NavigatingCancelEventHandler Navigating;

        public void InitializeFrame(Frame frame)
        {
            _mainFrame = frame;
        }

        public bool Navigate(NavigationPage page)
        {
            return _mainFrame.Navigate(GetPage(page));            
        }

        public bool Navigate(NavigationPage page, object parameter)
        {
            return _mainFrame.Navigate(GetPage(page), parameter);
        }

        public void GoBack()
        {
            if (_mainFrame.CanGoBack)
            {
                _mainFrame.GoBack();
            }
        }

        private Type GetPage(NavigationPage page)
        {
            Type pageType;
            switch (page)
            {
                case NavigationPage.GameList:
                    pageType = typeof(HubPage);
                    break;

                case NavigationPage.GamePlaning:
                    pageType = typeof(GamePlaningPage);
                    break;

                case NavigationPage.GameBattel:
                    pageType = typeof(GameShootingPage);
                    break;

                default:
                    throw new InvalidOperationException(page.ToString());
            }

            return pageType;
        }
    }
}
