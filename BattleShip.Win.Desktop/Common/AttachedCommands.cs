﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace BattleShip.Common
{
    public class AttachedCommands
    {
        #region ItemClick

        public static ICommand GetItemClickCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(CommandProperty);
        }

        public static void SetItemClickCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(CommandProperty, value);
        }

        // Using a DependencyProperty as the backing store for Command.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.RegisterAttached("ItemClickCommand", typeof(ICommand), typeof(AttachedCommands), new PropertyMetadata(null, ItemClickCommandPropertyChanged));

        private static void ItemClickCommandPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // Attach click handler
            (d as ListViewBase).ItemClick += AttachedCommands_ItemClick;
        }

        private static void AttachedCommands_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Get GridView
            var itemView = (sender as ListViewBase);

            // Get command
            ICommand command = GetItemClickCommand(itemView);

            // Execute command
            command.Execute(e.ClickedItem);
        }

        #endregion ItemClick

        #region RightTapped

        public static ICommand GetRightTappedCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(RightTappedCommandProperty);
        }

        public static void SetRightTappedCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(RightTappedCommandProperty, value);
        }

        // Using a DependencyProperty as the backing store for RightTappedCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RightTappedCommandProperty =
            DependencyProperty.RegisterAttached("RightTappedCommand", typeof(ICommand), typeof(AttachedCommands), new PropertyMetadata(null, RightTappedCommandPropertyChanged));

        private static void RightTappedCommandPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // Attach click handler
            (d as ListViewBase).RightTapped += AttachedCommands_RightTapped;
        }

        static void AttachedCommands_RightTapped(object sender, Windows.UI.Xaml.Input.RightTappedRoutedEventArgs e)
        {
            DependencyObject d = sender as DependencyObject;

            var x = e.OriginalSource as FrameworkElement;
            if (x != null)
            {
                ICommand command = GetRightTappedCommand(d);
                command.Execute(x.DataContext);
                e.Handled = true;
            }           
        }

       

        #endregion RightTapped

        #region PointerEnetered

        public static ICommand GetPointerEneteredCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(PointerEneteredCommand);
        }

        public static void SetPointerEneteredCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(PointerEneteredCommand, value);
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PointerEneteredCommand =
            DependencyProperty.RegisterAttached("PointerEneteredCommand", typeof(ICommand), typeof(AttachedCommands), new PropertyMetadata(null, PointerEneteredCommandPropertyChanged));


        private static void PointerEneteredCommandPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as UIElement).PointerEntered += AttachedCommands_PointerEntered;
        }

        private static void AttachedCommands_PointerEntered(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            DependencyObject d = sender as DependencyObject;

            var x = e.OriginalSource as FrameworkElement;
            if (x != null)
            {
                ICommand command = GetPointerEneteredCommand(d);
                command.Execute(x.DataContext);
            }
        }


        #endregion PointerEnetered

        #region PointerMoved

        public static ICommand GetPointerMovedCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(PointerMovedCommand);
        }

        public static void SetPointerMovedCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(PointerMovedCommand, value);
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PointerMovedCommand =
            DependencyProperty.RegisterAttached("PointerMovedCommand", typeof(ICommand), typeof(AttachedCommands), new PropertyMetadata(null, PointerMovedCommandPropertyChanged));


        private static void PointerMovedCommandPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as UIElement).PointerMoved += AttachedCommands_PointerMoved;
        }

        private static void AttachedCommands_PointerMoved(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            DependencyObject d = sender as DependencyObject;

            var x = e.OriginalSource as FrameworkElement;
            if (x != null)
            {
                ICommand command = GetPointerMovedCommand(d);
                command.Execute(x.DataContext);
            }
        }


        #endregion PointerMoved

        #region PointerExited

        public static ICommand GetPointerExitedCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(PointerExitedCommand);
        }

        public static void SetPointerExitedCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(PointerExitedCommand, value);
        }

        // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PointerExitedCommand =
            DependencyProperty.RegisterAttached("PointerExitedCommand", typeof(ICommand), typeof(AttachedCommands), new PropertyMetadata(null, PointerExitedCommandPropertyChanged));


        private static void PointerExitedCommandPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            (d as UIElement).PointerExited += AttachedCommands_PointerExited;
        }

        private static void AttachedCommands_PointerExited(object sender, Windows.UI.Xaml.Input.PointerRoutedEventArgs e)
        {
            DependencyObject d = sender as DependencyObject;

            var x = e.OriginalSource as FrameworkElement;
            if (x != null)
            {
                ICommand command = GetPointerExitedCommand(d);
                command.Execute(x.DataContext);
            }
        }


        #endregion PointerExited

    }
}
