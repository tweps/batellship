﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleShip.Domain.Game;

namespace BattleShip.Domain.Repositories
{
    public class PlayerContextRepository 
    {
        public static readonly PlayerContextRepository Instance = new PlayerContextRepository();

        private PlayerContextRepository()
        {
        }

        private readonly Dictionary<Guid, PlayerContext> store = new Dictionary<Guid, PlayerContext>();


        public PlayerContext Create(IPlayer player)
        {
            PlayerContext playerContext = new PlayerContext { Player = player };

            store.Add(playerContext.Id, playerContext);

            return playerContext;
        }


        public PlayerContext Get(Guid contextId)
        {
            PlayerContext context;
            store.TryGetValue(contextId, out context);

            return context;
        }

    }
}
