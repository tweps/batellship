﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleShip.Domain.Game;


namespace BattleShip.Domain.Repositories
{
    public class GameRepository
    {
        public static readonly GameRepository Instance = new GameRepository();

        private readonly List<GameDuel> gamesList = new List<GameDuel>();

        private GameRepository()
        {
        }

        public GameDuel CreateNew()
        {
            GameDuel game = new GameDuel();
            gamesList.Add(game);

            return game;
        }

        public GameDuel GetGame(Guid id)
        {
            return gamesList.FirstOrDefault((g) => g.Id == id);
        }
       
        public IQueryable<GameDuel> GetAllGames()
        {
            return gamesList.AsQueryable<GameDuel>();
        }

        public IQueryable<GameDuel> GetAllNotStartedGames()
        {
            return GetAllGames().Where(NotStartedGameCondition).AsQueryable<GameDuel>();
        }

        private bool NotStartedGameCondition(GameDuel game)
        {
            return game.State != null &&
                (game.State.PlayerOneGameStatus == GameStatus.NotStarted
                ||
                 game.State.PlayerTwoGameStatus == GameStatus.NotStarted
                );                
        }        
    }
}
