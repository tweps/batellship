﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleShip.Domain.Map;
using BattleShip.Domain.Exceptions;


namespace BattleShip.Domain.Board
{
    public class OponentBoard : GameBoard, IOponentBoard 
    {
        private readonly SeaMapCovered filteredSeaBoardMap;        

        public OponentBoard(IPlayerContext context, ISeaMapEdit seaBoardMap)
            : base(context, seaBoardMap)
        {
            this.context = context;
            this.filteredSeaBoardMap = new SeaMapCovered(base.seaBoardMap);            
        }

        public override ISeaMap SeaMap
        {
            get
            {
                return filteredSeaBoardMap;
            }
        }

        public BoardState Shoot(int positionX, int positionY)
        {
            if (context.Status != GameStatus.PlayerTurn )
            {
                throw new BattleShipException("The shoot has been made in not player turn");
            }

            BoardState result = seaBoardMap.Shoot(positionX, positionY);

            context.Notify();

            return result;
        }                      
    }
}
