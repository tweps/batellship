﻿using BattleShip.Domain.Map.Pieces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Board
{
    public class GameBoard 
    {
        protected IPlayerContext context;
        protected List<IShip> ShipList;
        protected internal ISeaMapEdit seaBoardMap;

        public GameBoard(IPlayerContext context, ISeaMapEdit seaBoardMap)
        {
            this.context = context;
            this.seaBoardMap = seaBoardMap;
            ShipList = new List<IShip>();            
        }

        public virtual ISeaMap SeaMap
        {
            get
            {
                return (ISeaMap)seaBoardMap;
            }
        }

        public virtual void Add(IShip ship)
        {
            ShipList.Add(ship);
        }

        public virtual void Remove(IShip ship)
        {
            ShipList.Remove(ship);
        }
                
        protected IShip FindNearestShip(int positionX, int positionY)
        {
            for (int x = positionX - 1; x <= positionX + 1; x++)
            {
                if (x >= 0 && x < seaBoardMap.SizeX)
                {
                    for (int y = positionY - 1; y <= positionY + 1; y++)
                    {
                        if (y >= 0 && y < seaBoardMap.SizeY)
                        {
                            ShipMastPiece shipMast = seaBoardMap[x, y] as ShipMastPiece;
                            if (shipMast != null)
                            {
                                return shipMast.Ship;
                            }
                        }
                    }
                }
            }

            return null;
        }
                
        public virtual bool AllShipsAreSunken
        {
            get
            {
                return ShipList.All((s) => s.IsSunken);
            }
        }
    }
}
