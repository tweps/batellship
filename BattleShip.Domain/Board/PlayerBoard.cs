﻿using BattleShip.Domain.Exceptions;
using BattleShip.Domain.Map;
using BattleShip.Domain.Map.Pieces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Board
{
    public class PlayerBoard : GameBoard, IPlayerBoard
    {
        public PlayerBoard(IPlayerContext context, ISeaMapEdit seaBoardMap)
            : base(context, seaBoardMap)
        {            
            
        }

        public IShip GetShip(int positionX, int positionY)
        {
            ShipMastPiece shipMast = seaBoardMap[positionX, positionY] as ShipMastPiece;
            if (shipMast != null)
            {
                return shipMast.Ship;
            }

            return null;
        }

        public override void Add(IShip ship)
        {
            CheckStatus();
            if (!CanAddShip(ship))
            {
                throw new BattleShipException("Ship can not be added");
            }
            
            seaBoardMap.Add(ship.Masts);
            base.Add(ship);
        }

        public override void Remove(IShip ship)
        {            
            seaBoardMap.Remove(ship.Masts);
            base.Remove(ship);
        }
              
        public bool CanAddShip(IShip ship)
        {            
            foreach (var point in ship.Masts)
            {
                if (FindNearestShip(point.PositionX, point.PositionY) != null)
                {
                    return false;
                }

                if (point.PositionX < 0 || point.PositionX > (seaBoardMap.SizeX-1))
                {
                    return false;
                }

                if (point.PositionY < 0 || point.PositionY > (seaBoardMap.SizeY-1))
                {
                    return false;
                }
            }

            return true;
        }

        public void ConfirmShipPlacement()
        {
            CheckStatus();
            context.Notify();
        }

        private void CheckStatus()
        {
            if(context.Status != GameStatus.ShipsArrangement)
            {
                throw new BattleShipException("Ship can be added / confirmed only in Ship Arrangement Mode");
            }
        }

        public IEnumerable<IShip> GetAllShips()
        {
            return this.ShipList;
        }

        public IShip ConstructShip(int x, int y, int masts, ShipDirection direction)
        {
            return ShipFabric.Instance.Construct(x, y, masts, direction);
        }
    }

   
}
