﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Game
{
    public class GameDefinitions : IGameDefinitions
    {
        private static readonly GameDefinitions _Instance = new GameDefinitions();
        public static GameDefinitions Instance
        {
            get
            {
                return _Instance;
            }
        }

        private GameDefinitions()
        {
            shipsDefinitionsList = new List<ShipDefinition>();
            
            shipsDefinitionsList.Add(new ShipDefinition(4, 1));
            shipsDefinitionsList.Add(new ShipDefinition(3, 2));
            shipsDefinitionsList.Add(new ShipDefinition(2, 3));
            shipsDefinitionsList.Add(new ShipDefinition(1, 4));
            
            // shipsDefinitionsList.Add(new ShipDefinition(1, 1));

            BoardSizeX = 10;
            BoardSizeY = 10;
        }
        
        public int BoardSizeX { get; private set; }
        public int BoardSizeY { get; private set; }

        private List<ShipDefinition> shipsDefinitionsList;
        public IEnumerable<ShipDefinition> ShipsDefinitions
        {
            get
            {
                return shipsDefinitionsList.AsEnumerable<ShipDefinition>();
            }
        }                
    }


}
