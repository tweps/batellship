﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Game
{
    public class GameInfo : IGameInfo
    {
        public Guid Id { get; set; }        
        public string Description { get; set; }
        public DateTime? Created { get; set; }
        public IPlayer[] Players { get; set; }        
    }
}
