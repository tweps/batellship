﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.Domain
{
    public interface ISeaMapEdit : ISeaMap
    {
        void Add(ISeaMapPiece piec);
        void Add(IEnumerable<ISeaMapPiece> pieces);
        void Remove(ISeaMapPiece piec);
        void Remove(IEnumerable<ISeaMapPiece> pieces);
        BoardState Shoot(int positionX, int positionY);
    }
}
