﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BattleShip.Domain.Board;
using BattleShip.Domain.Game.States;
using BattleShip.Domain.Map;
using BattleShip.Domain.Exceptions;


namespace BattleShip.Domain.Game
{
    public class GameDuel : IGameContext, IGameMediator
    {        
        public GameDuel()
        {
            this.Id = Guid.NewGuid();
            this.playrsContexts = new IPlayerContext[2];
            
            this.SeaMaps = new ISeaMapEdit[playrsContexts.Count()];
            for (int i = 0; i < SeaMaps.Count(); i++)
            {
                this.SeaMaps[i] = new SeaMap();
            }

            this.State = new GameStateShipArrangements(this);
            this.Created = DateTime.UtcNow;
        }

        private DateTime? Created;
        private IPlayerContext[] playrsContexts;
        private ISeaMapEdit[] SeaMaps;
        
        public Guid Id { get; private set; }
        
        #region IGameContext

        public ISeaMapEdit SeaMapPlayerOne
        {
            get
            {
                return SeaMaps[0];
            }
        }

        public ISeaMapEdit SeaMapPlayerTwo
        {
            get
            {
                return SeaMaps[1];
            }
        }

        public GameState State { get; set; }

        #endregion IGameContext

        #region IGameMediator

        public void Notify(IPlayerContext playerContext)
        {
            State.Next(this, GetSender(playerContext));

            if (playrsContexts[0] != null)
            {
                playrsContexts[0].UpdateStatus(State.PlayerOneGameStatus);
            }
            if (playrsContexts[1] != null)
            {
                playrsContexts[1].UpdateStatus(State.PlayerTwoGameStatus);
            }
        }

        #endregion IGameMediator

        public bool HasAllPlayers
        {
            get
            {
                return playrsContexts != null
                    && playrsContexts.Any()
                    && playrsContexts.All((p)=>p!=null);
            }
        }

        public void Register(PlayerContext playerContext)
        {
            if (playerContext == null || playerContext.Player == null)
            {
                throw new ArgumentNullException("Player has to be assigned");
            }

            bool hasBeenRegistred = false;
            for (int i = 0; i < playrsContexts.Count() && !hasBeenRegistred; i++)
            {
                if (playrsContexts[i] == null)
                {
                    playrsContexts[i] = playerContext;

                    playerContext.Coordinator = this;                    
                    playerContext.PlayerBoard = new PlayerBoard(playerContext, SeaMaps[i]);
                    int oponentMap = i + 1 == playrsContexts.Count() ? 0 : i + 1;
                    playerContext.OponentBoard = new OponentBoard(playerContext, SeaMaps[oponentMap]);

                    hasBeenRegistred = true;
                }
            }

            if (!hasBeenRegistred)
            {
                throw new BattleShipRegistrationFailException("Game already have full set of players");
            }

            // Check if game has assigned all players
            if (playrsContexts.All((c) => c != null))
            {
                Created = DateTime.UtcNow;
                playrsContexts[0].Oponent = playrsContexts[1].Player;
                playrsContexts[1].Oponent = playrsContexts[0].Player;
            }

            Notify(playerContext);
        }
       
        private StateSignalSender GetSender(IPlayerContext playerContext)
        {            
            for (int i = 0; i < playrsContexts.Count(); i++)
            {
                if (playrsContexts[i] == playerContext)
                {
                    return (StateSignalSender)i;
                }
            }

            throw new BattleShipException("Player's context is not assigned to game");
        }

        public IEnumerable<IPlayerContext> GetPlayerContext(IPlayer player)
        {
            var result = playrsContexts.Where((c) => c != null && c.Player.Equals(player));
            return result;
        }

        public IEnumerable<IPlayer> GetPlayers()
        {
            var result = from c in playrsContexts
                   where c != null
                   select c.Player;

            return result;
        }

        public IGameInfo GenerateInfo()
        {
            var players = from c in playrsContexts
                          where c!=null && c.Player!=null
                          select c.Player;

            var info = new GameInfo
            {
                Id = this.Id,
                Created = this.Created,     
                Players = players.ToArray()
            };
            
            
            if(playrsContexts[0]==null)
            {
                info.Description = "It is an Empty Game";
                return info;
            }

            if(playrsContexts[0].Oponent == null || playrsContexts[1].Oponent == null)
            {
                //info.Description = string.Concat("Player ", playrsContexts[0].Player.PlayerName, " is waiting for oponent");
                info.Description = string.Concat("New Game");
                return info;
            }

            // to do - add game counter
            string description = string.Concat(playrsContexts[0].Player.PlayerName, " vs ", playrsContexts[1].Player.PlayerName);
            
            if (State.PlayerOneGameStatus == GameStatus.PlayerHasWinTheMatch || State.PlayerTwoGameStatus == GameStatus.PlayerHasWinTheMatch )
            {
                description += " *";
            }
            
            info.Description = description;

            return info;           
        }
    }
}
