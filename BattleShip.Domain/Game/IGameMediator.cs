﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain
{
    public interface IGameMediator
    {
        Guid Id { get; }
        void Notify(IPlayerContext playerContext);
    }
}
