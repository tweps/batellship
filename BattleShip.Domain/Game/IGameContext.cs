﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleShip.Domain.Game.States;

namespace BattleShip.Domain
{
    public interface IGameContext
    {
        ISeaMapEdit SeaMapPlayerOne { get; }
        ISeaMapEdit SeaMapPlayerTwo { get; }
        GameState State { get; set; }
    }
}
