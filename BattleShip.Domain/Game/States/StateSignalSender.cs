﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Game.States
{
    public enum StateSignalSender
    {
        PlayerOne = 0,
        PlayerTwo = 1
    }
}
