﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Game.States
{
    public class GameStateBattle : GameState
    {
        private bool isFirstPlayerTurn;

        public GameStateBattle(IGameContext gameContext)
        {
            isFirstPlayerTurn = true;
            UpdatePlayersStatus();            
        }

        public override void Next(IGameContext gameContext, StateSignalSender sender)
        {
            ISeaMap seaMap = sender == StateSignalSender.PlayerTwo ? gameContext.SeaMapPlayerOne : gameContext.SeaMapPlayerTwo;

            if (AreAllShipsSunken(seaMap))
            {
                gameContext.State = new GameStateFinished(sender);
                return;
            }

            isFirstPlayerTurn = !isFirstPlayerTurn;
            UpdatePlayersStatus();
        }


        // ReSharper disable once ParameterTypeCanBeEnumerable.Local
        private bool AreAllShipsSunken(ISeaMap seaMap)
        {
            return seaMap.All(s => s.State != BoardState.NotHit);
        }

        private void UpdatePlayersStatus()
        {
            if (isFirstPlayerTurn)
            {               
                PlayerOneGameStatus = GameStatus.PlayerTurn;
                PlayerTwoGameStatus = GameStatus.OpponentTurn;
            }
            else
            {
                PlayerOneGameStatus = GameStatus.OpponentTurn;
                PlayerTwoGameStatus = GameStatus.PlayerTurn;
            }

        }     
    }
}
