﻿using BattleShip.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Game.States
{
    public class GameStateShipArrangements : GameState
    {                
        public GameStateShipArrangements(IGameContext gameContext)
        {
           
        }

        public override void Next(IGameContext gameContext, StateSignalSender sender)
        {
            switch (sender)
            {
                case StateSignalSender.PlayerOne:
                    PlayerOneGameStatus = SetState(PlayerOneGameStatus);
                    break;

                case StateSignalSender.PlayerTwo:
                    PlayerTwoGameStatus = SetState(PlayerTwoGameStatus);
                    break;
            }
            
            if (PlayerOneGameStatus == GameStatus.WaitingForOpponent && PlayerTwoGameStatus == GameStatus.WaitingForOpponent)
            {
                gameContext.State = new GameStateBattle(gameContext);
            }
        }

        private static GameStatus SetState(GameStatus current )
        {
            if (current == GameStatus.NotStarted)
                return GameStatus.ShipsArrangement;

            if (current == GameStatus.ShipsArrangement)
                return GameStatus.WaitingForOpponent;

            throw new BattleShipException("Illegal game state");
        }
       
    }
}
