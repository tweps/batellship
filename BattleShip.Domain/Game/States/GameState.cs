﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace BattleShip.Domain.Game.States
{    
    public abstract class GameState
    {
        public abstract void Next(IGameContext gameContext, StateSignalSender sender);
        
        public GameStatus PlayerOneGameStatus { get; protected set; }
        public GameStatus PlayerTwoGameStatus { get; protected set; }
    }
}
