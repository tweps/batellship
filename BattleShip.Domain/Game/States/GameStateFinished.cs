﻿using BattleShip.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Game.States
{
    public class GameStateFinished : GameState
    {
        public GameStateFinished(StateSignalSender winner)
        {
            switch (winner)
            {
                case StateSignalSender.PlayerOne:
                    PlayerOneGameStatus = GameStatus.PlayerHasWinTheMatch;
                    PlayerTwoGameStatus = GameStatus.PlayerHasLoseTheMatch;
                    break;

                case StateSignalSender.PlayerTwo:
                    PlayerOneGameStatus = GameStatus.PlayerHasLoseTheMatch;
                    PlayerTwoGameStatus = GameStatus.PlayerHasWinTheMatch;
                    break;

                default:
                    throw new BattleShipException("It wasn't possible to set winner, :" + winner.ToString());
            }     
        }

        public override void Next(IGameContext gameContext, StateSignalSender sender)
        {
            return;
        }        
    }
}
