﻿using BattleShip.Domain.Board;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Game
{
    public class PlayerContext : IPlayerContext
    {                
        public PlayerContext()
        {
            this.Id = Guid.NewGuid();
        }

        public IGameMediator Coordinator { get; set; }

        #region IPlayerContext

        public Guid Id { get; private set; }
      
        public Guid GameId
        {
            get             
            {
                if (Coordinator == null)
                {
                    return Guid.Empty;
                }
                return Coordinator.Id;
            }
        }

        public IPlayer Player { get; set; }
        public IPlayer Oponent { get; set; }

        public IPlayerBoard PlayerBoard { get; set; }
        public IOponentBoard OponentBoard { get; set; }

        public GameStatus Status { get; private set; }

        public virtual void UpdateStatus(GameStatus lastStatus)
        {
            this.Status = lastStatus;
            this.RefreshNotification(this, new EventArgs { });
        }

        public void Notify()
        {            
            Coordinator.Notify(this);
        }

        public event EventHandler RefreshNotification = delegate{};

        #endregion IPlayerContext        
    }
}
