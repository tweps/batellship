﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Map
{    
    internal class SeaMapPiecesEnumerator : IEnumerator<ISeaMapPiece>, IEnumerator
    {
        private ISeaMap seaBoardMap;
        private Tuple<int, int> index = null;

        public SeaMapPiecesEnumerator(ISeaMap seaBoardMap)
        {
            this.seaBoardMap = seaBoardMap;            
        }

        public ISeaMapPiece Current
        {
            get
            {
                return seaBoardMap[index.Item1, index.Item2];
            }
        }

        object IEnumerator.Current
        {
            get
            {
                var src = (IEnumerator<ISeaMapPiece>)this;
                return (object)src.Current;
            }
        }

        public bool MoveNext()
        {
            if (index == null)
            {
                index = new Tuple<int, int>(0, 0);
                return true;
            }

            if (index.Item1 < seaBoardMap.SizeX - 1)
            {
                index = new Tuple<int, int>(index.Item1 + 1, index.Item2);
                return true;
            }

            if (index.Item2 < seaBoardMap.SizeY - 1)
            {
                index = new Tuple<int, int>(0, index.Item2 + 1);
                return true;
            }

            return false;
        }

        public void Reset()
        {
            this.index = null;
        }

        public void Dispose()
        {
            this.seaBoardMap = null;
        }
    }

}
