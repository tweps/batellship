﻿using BattleShip.Domain.Map.Pieces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Map
{
    public class SeaMapPieceNotHitFilter : ISeaMapPiece
    {       
        public ISeaMapPiece src;
        
        public SeaMapPieceNotHitFilter(ISeaMapPiece boardPiece)
        {
            this.src = boardPiece;           
        }

        public int PositionX
        {
            get { return src.PositionX; }
        }

        public int PositionY
        {
            get { return src.PositionY; }
        }

        public bool IsHit
        {
            get { return src.IsHit; }
        }

        public BoardState State
        {
            get
            {
                if (!src.IsHit)
                {
                    return BoardState.Unknown;
                }

                return src.State;
            }
        }

        public bool Equals(ISeaMapPiece other)
        {
            return SeaMapPieceComparer.Instance.Equals(this, other);
        }

        public override bool Equals(object obj)
        {
            if (obj is ISeaMapPiece)
            {
                return Equals((ISeaMapPiece)obj);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return SeaMapPieceComparer.Instance.GetHashCode(this);
        }
    }
}
