﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using BattleShip.Domain.Map.Pieces;

namespace BattleShip.Domain.Map
{
    public class SeaMap : ISeaMapEdit
    {
        private readonly ISeaMapPiece[,] Points;

        public SeaMap()
        {
            // ReSharper disable once CoVariantArrayConversion
            Points = new SeaMapPiece[10, 10];
            FillEmptyBoardPieces(CreateDefaultBoardPiece);
            LastShoot = null;
        }

        #region ISeaMap

        public int SizeX
        {
            get
            {
                return Points.GetUpperBound(0) + 1;
            }
        }

        public int SizeY
        {
            get
            {
                return Points.GetUpperBound(1) + 1;
            }
        }

        public virtual ISeaMapPiece this[int x, int y]
        {
            get
            {
                return Points[x, y];
            }
        }

        public ISeaMapPiece this[int i]
        {
            get 
            {
                return this[i / SizeX, i % SizeX];
            }
        }

        public ISeaMapPiece LastShoot { get; protected set; }

        #endregion ISeaMap

        #region ISeaMapEdit

        public void Add(IEnumerable<ISeaMapPiece> pieces)
        {
            foreach (ISeaMapPiece piece in pieces)
            {
                Add(piece);
            }
        }

        public void Remove(IEnumerable<ISeaMapPiece> pieces)
        {
            foreach (SeaMapPiece piece in pieces)
            {
                ResetToDefault(piece);
            }
        }

        public virtual void Add(ISeaMapPiece piece)
        {
            Points[piece.PositionX, piece.PositionY] = piece;            
        }

        public virtual void Remove(ISeaMapPiece piece)
        {
            ResetToDefault(piece);
        }

        public BoardState Shoot(int x, int y)
        {
            SeaMapPiece piece = Points[x, y] as SeaMapPiece;
            BoardState state = piece.Shoot();
            LastShoot = piece;
            return state;
        }

        #endregion ISeaMapEdit

        protected virtual void ResetToDefault(ISeaMapPiece piece)
        {
            Add(CreateDefaultBoardPiece(piece.PositionX, piece.PositionY));
        }

        protected ISeaMapPiece CreateDefaultBoardPiece(int x, int y)
        {
            return new WatterPiece(x, y);
        }

        protected void FillEmptyBoardPieces(Func<int, int, ISeaMapPiece> constructor)
        {
            for (int x = 0; x <= Points.GetUpperBound(0); x++)
            {
                for (int y = 0; y <= Points.GetUpperBound(1); y++)
                {
                    if (Points[x, y] == null)
                    {
                        Add(constructor(x, y));
                    }
                }
            }
        }        
        
        #region IEnumerable

        public IEnumerator GetEnumerator()
        {
            var source =(IEnumerable<ISeaMapPiece>)this;
            return source.GetEnumerator();
        }

        #endregion IEnumerable

        #region IEnumerable<IBoardPiece>

        IEnumerator<ISeaMapPiece> IEnumerable<ISeaMapPiece>.GetEnumerator()
        {
            return new SeaMapPiecesEnumerator(this);
        }

        #endregion IEnumerable                   
    }
}
