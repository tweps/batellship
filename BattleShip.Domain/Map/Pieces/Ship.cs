﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Map.Pieces
{
    public class Ship : IShip
    {
        public Ship()
        {
            HasRecognizedSize = false;
        }

        protected List<ShipMastPiece> masts = new List<ShipMastPiece>();

        public IEnumerable<ISeaMapPiece> Masts 
        {
            get
            {
                return masts;
            }
        }

        public virtual bool IsSunken
        {
            get { return Masts.All(m => m.IsHit) && HasRecognizedSize; }
        }
        
        public bool HasRecognizedSize { get; set; }
                         
        public ShipMastPiece AddShipMast(int x, int y)
        {
            ShipMastPiece shipMastPiece = new ShipMastPiece(this, x, y);
            masts.Add(shipMastPiece);
            
            return shipMastPiece;
        }        
    }


}
