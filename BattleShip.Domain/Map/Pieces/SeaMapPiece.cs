﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Map.Pieces
{
    public abstract class SeaMapPiece : ISeaMapPiece
    {
        protected SeaMapPiece(int x, int y)
        {
            PositionX = x;
            PositionY = y;            
        }

        public int PositionX { get; private set; }
        public int PositionY { get; private set; }
         
        public bool IsHit { get; protected set; }

        public abstract BoardState Shoot();


        private BoardState _State = BoardState.Unknown;

        public virtual BoardState State
        {
            get { return _State; } 
            protected set { _State = value; }
        }

       
        public bool Equals(ISeaMapPiece other)
        {
            return SeaMapPieceComparer.Instance.Equals(this, other);
        }

        public override bool Equals(object obj)
        {
            if (obj is ISeaMapPiece)
            {
                return Equals((ISeaMapPiece)obj);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return SeaMapPieceComparer.Instance.GetHashCode(this);
        }
       
    }
}
