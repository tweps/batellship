﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Map.Pieces
{
    public sealed class ShipMastPiece : SeaMapPiece
    {
        public Ship Ship { get; private set; }
        
        public ShipMastPiece(Ship owner, int x, int y)
            : base(x, y)
        {
            this.Ship = owner;
            this.State = BoardState.NotHit;
        }
       
        public override BoardState Shoot()
        {
            IsHit = true;
            State = BoardState.Hit;            
            return Ship.IsSunken ? BoardState.HitAndSunken : BoardState.Hit;            
        }

        public override BoardState State
        {
            get
            {
                if (Ship.IsSunken)
                {
                    return BoardState.HitAndSunken;
                }
                return base.State;
            }
            protected set
            {
                base.State = value;
            }
        }
           
        

    }
}
