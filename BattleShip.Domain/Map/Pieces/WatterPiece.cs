﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Map.Pieces
{
    public class WatterPiece : SeaMapPiece
    {
        public WatterPiece(int x, int y)
            : base(x, y)
        {

        }

        public override BoardState Shoot()
        {
            IsHit = true;
            State = BoardState.Mishit;
            return State; 
        }
    }
}
