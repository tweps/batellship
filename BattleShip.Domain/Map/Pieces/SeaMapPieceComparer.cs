﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Map.Pieces
{
    public class SeaMapPieceComparer : IEqualityComparer<ISeaMapPiece>
    {
        public static readonly SeaMapPieceComparer Instance = new SeaMapPieceComparer();

        public bool Equals(ISeaMapPiece x, ISeaMapPiece y)
        {
            return x.PositionX == y.PositionX
                  && x.PositionY == y.PositionY
                  && x.State == y.State
                  && x.IsHit == y.IsHit
                  ;
        }

        public int GetHashCode(ISeaMapPiece obj)
        {
            return string.Concat(obj.PositionX, ",", obj.PositionY, "=", obj.State).GetHashCode();
        }
    }
}
