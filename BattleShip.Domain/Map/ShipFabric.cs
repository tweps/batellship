﻿using BattleShip.Domain.Map.Pieces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Map
{
    public class ShipFabric
    {
        public static readonly ShipFabric Instance = new ShipFabric();

        private ShipFabric()
        {
        }

        public IShip Construct(int x, int y, int masts, ShipDirection direction)
        {
            Ship ship = new Ship();
            foreach (var point in CreateShipPoints(x, y, masts, direction))
            {
                ship.AddShipMast(point.Item1, point.Item2);
            }

            ship.HasRecognizedSize = true;
            return ship;
        }

        private IEnumerable<Tuple<int, int>> CreateShipPoints(int x, int y, int masts, ShipDirection direction)
        {
            Tuple<int, int>[] shipPoints = new Tuple<int, int>[masts];
            for (int i = 0; i < masts; i++)
            {
                shipPoints[i] = new Tuple<int, int>(x, y);
                switch (direction)
                {
                    case ShipDirection.North:
                        y--;
                        break;
                    case ShipDirection.South:
                        y++;
                        break;
                    case ShipDirection.East:
                        x++;
                        break;
                    case ShipDirection.West:
                        x--;
                        break;
                }
            }

            return shipPoints;
        }
    }
}
