﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleShip.Domain.Map
{
    public class SeaMapCovered : ISeaMap
    {
        private readonly SeaMapPieceNotHitFilter[,] points;
        private readonly ISeaMap seaBoardMap;

        public SeaMapCovered(ISeaMap seaBoardMap)
        {
            this.seaBoardMap = seaBoardMap;
            this.points = new SeaMapPieceNotHitFilter[seaBoardMap.SizeX, seaBoardMap.SizeY];
            for(int i=0; i<seaBoardMap.SizeX; i++)
            {
                for (int j = 0; j < seaBoardMap.SizeY; j++)
                {
                    points[i, j] = new SeaMapPieceNotHitFilter(seaBoardMap[i, j]);
                }
            }
        }

        #region ISeaMap

        public int SizeX
        {
            get { return seaBoardMap.SizeX; }
        }

        public int SizeY
        {
            get { return seaBoardMap.SizeX; }
        }

        public ISeaMapPiece this[int x, int y]
        {
            get
            {   
                points[x, y].src = seaBoardMap[x,y];
                return points[x, y];
            }
        }

        public ISeaMapPiece this[int i]
        {
            get
            {
                return this[i / SizeX, i % SizeX];
            }
        }

        public ISeaMapPiece LastShoot
        {
            get { return seaBoardMap.LastShoot; }
        }

        #endregion ISeaMap

        #region IEnumerable

        public IEnumerator GetEnumerator()
        {
            var source = (IEnumerable<ISeaMapPiece>)this;
            return source.GetEnumerator();
        }

        #endregion IEnumerable

        #region IEnumerable<IBoardPiece>

        IEnumerator<ISeaMapPiece> IEnumerable<ISeaMapPiece>.GetEnumerator()
        {
            return new SeaMapPiecesEnumerator(this);
        }

        #endregion IEnumerable
    }
}
