﻿using BattleShip.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BattleShip.Domain.Game;
using BattleShip.Domain.Repositories;

namespace BattleShip.Domain
{
    public class BattleShipGame : IBattleShipGame
    {
        private readonly PlayerContextRepository playerContextRepository;
        private readonly GameRepository gameRepository;

        public BattleShipGame(PlayerContextRepository playerContextRepository, GameRepository gameRepository)
        {
            this.playerContextRepository = playerContextRepository;
            this.gameRepository = gameRepository;
        }

        public BattleShipGame() : this(PlayerContextRepository.Instance , GameRepository.Instance)  
        {
        }


        public event EventHandler<PlayerHasNewGameEventArgs> PlayerHasNewGame = delegate { };
        public event EventHandler<PublicGameEventArgs> PublicGameOpned = delegate { };
        public event EventHandler<PublicGameEventArgs> PublicGameClosed  = delegate { };

        public IQueryable<IPlayerContext> GetAllActiveUserContexts(IPlayer player)
        {
            return GetAllUserContexts(player).Where(c => c.Status != GameStatus.PlayerHasLoseTheMatch && c.Status != GameStatus.PlayerHasWinTheMatch);
        }

        public IQueryable<IPlayerContext> GetAllUserContexts(IPlayer player)
        {
            var result = from c in gameRepository.GetAllGames().SelectMany((g) => g.GetPlayerContext(player))
                         select c;

            return result;
        }

        public IQueryable<IGameInfo> GetFreeToJoin(/*IPlayer player*/)
        {
            var query = from g in gameRepository.GetAllNotStartedGames()
                        let p = g.GetPlayers().ToArray()
                        where p.Count() == 1
                        select g.GenerateInfo();

            return query;
        }

        public IGameInfo GetGameInfo(Guid gameId)
        {
            return gameRepository.GetGame(gameId).GenerateInfo();
        }


        public IPlayerContext CreateGame(IPlayer player)
        {
            // To Do - Transactions

            var playerNotStertedGames = gameRepository.GetAllNotStartedGames().Count((g) => g.GetPlayers().Contains(player));
            if(playerNotStertedGames > 0 )
            {
                throw new BattleShipException("You can have only one not started game. Please wait for second player");
            }

            GameDuel gameDuel = gameRepository.CreateNew();

            IPlayerContext playerContext = RegisterPlayerToGame(player, gameDuel);

            PlayerHasNewGame(this, new PlayerHasNewGameEventArgs(player,playerContext));

            if (!gameDuel.HasAllPlayers)
            {
                PublicGameOpned(this, new PublicGameEventArgs(gameDuel.GenerateInfo()));
            }

            return playerContext;
        }

        public IPlayerContext JoinToGame(Guid gameId, IPlayer player)
        {
            // To Do - Transactions

            GameDuel gameDuel = gameRepository.GetGame(gameId);

            if(gameDuel.HasAllPlayers)
            {
                return null;
            }

            IPlayerContext playerContext = RegisterPlayerToGame(player, gameDuel);

            PlayerHasNewGame(this, new PlayerHasNewGameEventArgs(player, playerContext));

            if(gameDuel.HasAllPlayers)
            {
                PublicGameClosed(this, new PublicGameEventArgs(gameDuel.GenerateInfo()));
            }

            return playerContext;
        }

        protected IPlayerContext RegisterPlayerToGame(IPlayer player, GameDuel game)
        {
            if(game.HasAllPlayers)
            {
                return null;
            }

            var playerContext = playerContextRepository.Create(player);
            game.Register(playerContext);

            return playerContext;
        }

        public IPlayerContext Get(Guid contextId)
        {
            return playerContextRepository.Get(contextId);
        }
       
    }
}
