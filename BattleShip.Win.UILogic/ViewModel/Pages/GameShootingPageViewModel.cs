﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BattleShip.ViewModel.Board;
using BattleShip.Domain;
using BattleShip.UILogic;

namespace BattleShip.ViewModel.Pages
{
    public class GameShootingPageViewModel : BattleShipViewModelBase, INavigable
    {
        protected IPlayerContext context;

        public GameShootingPageViewModel()
        {

        }

        private string _GameComment;
        public string GameComment
        {
            get
            {
                return _GameComment;
            }
            set
            {
                Set(() => GameComment, ref _GameComment, value);
            }
        }

        private PlayerBoardViewModel _PlayerBoard;
        public PlayerBoardViewModel PlayerBoard
        {
            get { return _PlayerBoard; }
            protected  set { Set(() => PlayerBoard, ref _PlayerBoard, value); }
        }

        private OponentBoardViewModel _OponentBoard;
        public OponentBoardViewModel OponentBoard
        {
            get { return _OponentBoard; }
            protected set
            {
                Set(() => OponentBoard, ref _OponentBoard, value);
            }
        }
            
        #region INavigable

        public void Activate(object parameter, Dictionary<string, object> state)
        {
            context = parameter as IPlayerContext;
            if (context != null)
            {
                if (context.Player != null && context.Oponent != null)
                {
                    MainTitle = string.Concat(context.Player.PlayerName, " vs ", context.Oponent.PlayerName);
                }

                PlayerBoard = new PlayerBoardViewModel(context.PlayerBoard);
                OponentBoard = new OponentBoardViewModel(context.OponentBoard);
                context.RefreshNotification += OnContextRefreshNotification;
            }
            else
            {
                PlayerBoard = null;
                OponentBoard = null;                
            }

            Refresh();
        }
       
        public void Deactivate(Dictionary<string, object> state)
        {
            if (context != null)
            {
                context.RefreshNotification -= OnContextRefreshNotification;
            }
        }

        private void OnContextRefreshNotification(object sender, EventArgs e)
        {
            Refresh();
        }

        #endregion INavigable

        protected void Refresh()
        {
            if (context == null)
            {
                GameComment = null;
            }

            switch (context.Status)
            {
                case GameStatus.WaitingForOpponent:
                    GameComment = "The opponent hasn't finished arrangement ships yet";
                    break;
                case GameStatus.PlayerTurn:
                    GameComment = "It's your turn. Please make a shoot.";
                    break;
                case GameStatus.OpponentTurn:
                    GameComment = "It is opponent turn. Waiting for his shoot.";
                    break;
                case GameStatus.PlayerHasWinTheMatch:
                    GameComment = "You have won the match";
                    break;
                case GameStatus.PlayerHasLoseTheMatch:
                    GameComment = "You have lost the match";
                    break;
                default:
                    GameComment = context.Status.ToString();
                    break;
            }
           
            OponentBoard.HasPermissionToShoot = context.Status == GameStatus.PlayerTurn;
        }
    }
}
