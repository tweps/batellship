﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Collections.ObjectModel;

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

using BattleShip.Domain;
using BattleShip.Model;
using BattleShip.UILogic.Model;
using BattleShip.UILogic;
using BattleShip.UILogic.Services;
using Windows.UI.Xaml.Media.Imaging;
using BattleShip.UILogic.ViewModel.Pages;


namespace BattleShip.ViewModel.Pages
{
    public class HubPageViewModel : BattleShipViewModelBase, INavigable
    {
        private AvatarsService avatarsService = AvatarsService.Instance;
        private INavigationService navigationService;
        private IGame repository;
        
        public HubPageViewModel(INavigationService navigationService, IGame repository)
        {
            this.navigationService = navigationService;
            this.repository = repository;
            this.SelectCommand = new RelayCommand<GameItem>((i) => i.Execute());
            this.SelectGroupCommand = new RelayCommand<string>(OnSelectGroupCommand);
        }

        #region INavigable

        public void Activate(object parameter, Dictionary<string, object> state)
        {
            LoadContentAsync();                   
        }

        public void Deactivate(Dictionary<string, object> state)
        {
            
        }

        #endregion INavigable

        private IEnumerable<IGrouping<string, GameItem>> _Items;
        public IEnumerable<IGrouping<string, GameItem>> Items
        {
            get
            {
                return _Items;
            }
            set
            {
                Set(() => Items, ref _Items, value);
            }
        }
    
        public ICommand SelectCommand { get; private set; }
        public ICommand SelectGroupCommand { get; private set; }

        private void OnSelectGroupCommand(string groupName)
        {
            var gameItems = Items.FirstOrDefault((i) => i.Key == groupName);
            if (gameItems != null && gameItems.Count()==1)
            {
                gameItems.First().Execute();
            }                        
        }

        protected async void LoadContentAsync()
        {           
            PlayerModel player = AppContext.CurrentPlayer;

            List<GameItem> items = new List<GameItem>();
            items.AddRange(LoadBasicGameOptions());            
            items.AddRange(await LoadPlayerGamesAsync(player));            
            items.AddRange(await LoadFreeGamesAsync(player));
            
            Items = items.GroupBy((i)=>i.Category).ToList();                       
        }

        protected IEnumerable<GameItem> LoadBasicGameOptions()
        {
            const string category = "Start";

            return new GameItem[] { new GameItemSymbol
                    {
                        Category = category,
                        Title = "Create new game",
                        Execute = ()=> GoToNextPage(repository.MatchMake(AppContext.CurrentPlayer)),
                        Symbol = "\uE0C5"
                    }
            };
        }

        protected async Task<IEnumerable<GameItem>> LoadPlayerGamesAsync(IPlayer player)
        {
            const string category = "Active Games";
            
            List<GameItem> list = new List<GameItem>();

            foreach(var context in repository.GetAllUserContexts(player))
            {
                var game = repository.GetGameInfo(context.GameId);

                GameItem item;
                if (game.Players != null)
                {
                    switch (game.Players.Count())
                    {
                        case 0:
                            item = new GameItemSymbol { Symbol = "\uE13D" };
                            break;

                        case 1:
                            if (IsInDesignMode)
                                item = new GameItemOneImage();
                            else
                                item = new GameItemOneImage { Image = await avatarsService.LoadAvatarAsync(game.Players[0]) };
                            break;

                        case 2:
                            if (IsInDesignMode)
                                item = new GameItemTwoImages();
                            else
                                item = new GameItemTwoImages
                               {
                                   ImageFirst = await avatarsService.LoadAvatarAsync(game.Players[0]),
                                   ImageSecond = await avatarsService.LoadAvatarAsync(game.Players[1]),
                               };
                            break;

                        default:
                            throw new InvalidOperationException(game.Players.Count().ToString());
                    }

                    item.Category = category;
                    item.Title = TranslateGameStatus(context.Status);
                    item.Subtitle = game.Created.HasValue ? string.Concat(game.Description, Environment.NewLine, game.Created.ToString()) : game.Description;
                    item.Execute = () => GoToNextPage(context);

                    list.Add(item);
                }
            }

            return list;                            
        }

        protected string TranslateGameStatus(GameStatus status)
        {
            string text;
            switch (status)
            {
                case GameStatus.NotStarted:
                    text = "Not started yet";
                    break;
                case GameStatus.ShipsArrangement:
                    text = "Ships Arrangement";
                    break;
                case GameStatus.WaitingForOpponent:
                    text = "Waiting For Second Player";
                    break;
                case GameStatus.PlayerTurn:
                    text = "Your Turn";
                    break;
                case GameStatus.OpponentTurn:
                    text = "Opponent Turn";
                    break;
                case GameStatus.PlayerHasLoseTheMatch:
                    text = "Game Ended - You Lost";
                    break;
                case GameStatus.PlayerHasWinTheMatch:
                    text = "Game Ended - You Won";
                    break;                
                default:
                    text = status.ToString();
                    break;
            }

            return text;
        }

        protected async Task<IEnumerable<GameItem>> LoadFreeGamesAsync(IPlayer player)
        {
             const string category = "Free To Join";

             List<GameItem> list = new List<GameItem>();
             foreach (var gameInfo in repository.GetFreeToJoin(player).OrderBy((g) => g.Created))
             {
                 BitmapImage image = null;
                 if (!IsInDesignMode)
                 {
                     image = await avatarsService.LoadAvatarAsync(gameInfo.Players[0]);
                 }


                 list.Add(new GameItemOneImage
                          {
                              Category = category,
                              Title = "Join",
                              Subtitle = gameInfo.Description,
                              Image = image,
                              Execute = () => GoToNextPage(repository.JoinToGame(gameInfo.Id, AppContext.CurrentPlayer)),
                          }
                     );
             }

             return list;
        }
       
        private void GoToNextPage(IPlayerContext context)
        {
            NavigationPage nextPage;
            switch (context.Status)
            {
                case GameStatus.NotStarted:
                case GameStatus.ShipsArrangement:
                    nextPage = NavigationPage.GamePlaning;
                    break;

                case GameStatus.WaitingForOpponent:
                case GameStatus.PlayerTurn:
                case GameStatus.OpponentTurn:
                case GameStatus.PlayerHasLoseTheMatch:
                case GameStatus.PlayerHasWinTheMatch:
                    nextPage = NavigationPage.GameBattel;
                    break;

                default:
                    throw new InvalidOperationException(context.Status.ToString());
            }


            navigationService.Navigate(nextPage, context);
        }
    }
}
