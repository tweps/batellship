﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Command;

using BattleShip.ViewModel.Messeges;
using BattleShip.Domain;
using BattleShip.ViewModel.Board;
using BattleShip.ViewModel.ShipSelector;
using BattleShip.UILogic;


namespace BattleShip.ViewModel.Pages
{
    public class GamePlaningPageViewModel : BattleShipViewModelBase, INavigable
    {
        private IPlayerContext context;
        private INavigationService navigationService;
        private RelayCommand startGameRelayCommand;

        public GamePlaningPageViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;
            this.startGameRelayCommand = new RelayCommand(OnStartGameCommand, () => CanStartGame);
                       
            Messenger.Default.Register<PropertyChangedMessage<bool>>(this, OnPropertyChangedMessage);                                       
        }

        #region INavigable

        public void Activate(object parameter, Dictionary<string, object> state)
        {
            context = parameter as IPlayerContext;
            if (context != null)
            {                
                context.RefreshNotification += OnContextRefreshNotification;

                if (context.Player != null && context.Oponent != null)
                {
                    MainTitle = string.Concat(context.Player.PlayerName, " vs ", context.Oponent.PlayerName);
                }

                MessegeToken token = new MessegeToken();
                PlayerBoard = new Board.PlaningBoardViewModel(context.PlayerBoard, token);
                ShipSelector = new ShipSelector.ShipSelectorViewModel(token);
                
                ShipSelector.Substract(context.PlayerBoard.GetAllShips());
            }
            else
            {
                PlayerBoard = null;
                ShipSelector = null;
            }

            Refresh();
        }

        public void Deactivate(Dictionary<string, object> state)
        {
            if (context != null)
            {
                context.RefreshNotification -= OnContextRefreshNotification;
            }           
        }

        #endregion INavigable

        private void OnContextRefreshNotification(object sender, EventArgs e)
        {
            Refresh();
        }

        private void Refresh()
        {
            PlaningSectionVisible = context != null && context.Status == GameStatus.ShipsArrangement;
        }

        private PlaningBoardViewModel _PlayerBoard;
        public PlaningBoardViewModel PlayerBoard 
        {
            get
            {
                return _PlayerBoard;
            }
            protected set
            {
                Set(() => PlayerBoard, ref _PlayerBoard, value);
            }
        }

        private ShipSelectorViewModel _ShipSelector;
        public ShipSelectorViewModel ShipSelector
        {
            get
            {
                return _ShipSelector;
            }
            protected set
            {
                Set(() => ShipSelector, ref _ShipSelector, value);
            }
        }

        private bool _PlaningSectionVisible;
        public bool PlaningSectionVisible
        {
            get
            {
                return _PlaningSectionVisible;
            }
            set
            {
                if (Set(() => PlaningSectionVisible, ref _PlaningSectionVisible, value))
                {
                    RaisePropertyChanged(() => WaitingInfoSectionVisible);
                }
            }
        }

        public bool WaitingInfoSectionVisible
        {
            get
            {
                return !PlaningSectionVisible;
            }
        }

        public ICommand StartGameCommand
        {
            get
            {
                return startGameRelayCommand;
            }
        }

        private bool _CanStartGame;
        public bool CanStartGame
        {
            get
            {
                return _CanStartGame;
            }
            set
            {                
                Set(() => CanStartGame, ref _CanStartGame, value);
            }
        }

        private void OnPropertyChangedMessage(PropertyChangedMessage<bool> message)
        {
            string propertyName = ExtractPropertyName(() => ShipSelector.AllShipsArePlaced);
            if (message.PropertyName != propertyName)
                return;
            CanStartGame = message.NewValue;
            startGameRelayCommand.RaiseCanExecuteChanged();
        }

        private void OnStartGameCommand()
        {
            PlayerBoard.ConfirmShipPlacement();

            navigationService.GoBack();
            navigationService.Navigate(NavigationPage.GameBattel, context);            
        }       
    }
}
