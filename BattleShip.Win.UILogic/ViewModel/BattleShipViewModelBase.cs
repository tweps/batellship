﻿using BattleShip.UILogic.ViewModel.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BattleShip.ViewModel
{
    public abstract class BattleShipViewModelBase : GalaSoft.MvvmLight.ViewModelBase, IMainPageViewModel
    {

        public BattleShipViewModelBase()
        {
            MainTitle = "Battel Ship";
        }

        private string _MainTitle;
        public virtual string MainTitle
        {
            get
            {
                return _MainTitle;
            }
            protected set
            {
                Set(() => MainTitle, ref _MainTitle, value);
            }
        }

        public static string ExtractPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            if (propertyExpression == null)
            {
                throw new ArgumentNullException("propertyExpression");
            }

            MemberExpression memberExpression = propertyExpression.Body as MemberExpression;

            if (memberExpression == null)
            {
                throw new ArgumentException("The expression is not a member access expression.", "propertyExpression");
            }

            PropertyInfo property = memberExpression.Member as PropertyInfo;

            if (property == null)
            {
                throw new ArgumentException("The member access expression does not access a property.",
                                            "propertyExpression");
            }

            MethodInfo getMethod = property.GetMethod;

            if (getMethod.IsStatic)
            {
                throw new ArgumentException("The referenced property is a static property.", "propertyExpression");
            }

            return memberExpression.Member.Name;
        }       
    }
}
