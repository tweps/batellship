﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GalaSoft.MvvmLight.Command;
using BattleShip.Domain;
using BattleShip.UILogic.Model;


namespace BattleShip.ViewModel.Board
{            
    public class OponentBoardViewModel : BoardViewModel 
    {
        private IOponentBoard oponentBoard;
        
        public OponentBoardViewModel(IOponentBoard oponentBoard) : base(oponentBoard.SeaMap)
        {
            this.oponentBoard = oponentBoard;

            this.BoardPieceSelectedCommand = new RelayCommand<BoardPieceViewModel>(Shoot, (p)=> BoardPieceIsAvalibleToShoot(p));
            this.BoardPieceProposeCommand = new RelayCommand<object>((p)=>UpdateProposedShootTarget(p as BoardPieceViewModel));
            this.BoardPieceOptionCommand = null;           
        }

        private bool _HasPermissionToShoot;
        public bool HasPermissionToShoot
        {
            get
            {
                return _HasPermissionToShoot;
            }
            set
            {
                Set(() => HasPermissionToShoot, ref _HasPermissionToShoot, value);
            }
        }

        private bool BoardPieceIsAvalibleToShoot(BoardPieceViewModel shootTarget)
        {
            if (shootTarget != null && !shootTarget.IsHit && HasPermissionToShoot)
            {
                return true;
            }

            return false;
        }


        private void UpdateProposedShootTarget(BoardPieceViewModel shootTarget)
        {
            foreach (BoardPieceViewModel boardPieceViewModel in BoardPieces)
            {
                boardPieceViewModel.Highlighting = HighlightingType.NotHighlighted;
            }

            if(BoardPieceIsAvalibleToShoot(shootTarget))
            {
                shootTarget.Highlighting = HighlightingType.Allowed;
            }
            
        }

        private void Shoot(BoardPieceViewModel shootTarget)
        {
            UpdateProposedShootTarget(null);

            if (BoardPieceIsAvalibleToShoot(shootTarget))
            {
                HasPermissionToShoot = false;
                oponentBoard.Shoot(shootTarget.PositionX, shootTarget.PositionY);                               
                Reaload();                
            }            
        }
    }

}
