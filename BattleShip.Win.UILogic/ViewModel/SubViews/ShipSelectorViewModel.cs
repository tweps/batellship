﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GalaSoft.MvvmLight.Messaging;

using BattleShip.ViewModel.Messeges;
using BattleShip.Domain;
using BattleShip.Domain.Repositories;


namespace BattleShip.ViewModel.ShipSelector
{
    public class ShipSelectorViewModel : BattleShipViewModelBase
    {
        private MessegeToken token;

        public ShipSelectorViewModel(MessegeToken token)
        {
            this.token = token;
            SelectionsItems = new ObservableCollection<ShipSelectorItemModel>();

            foreach (var shipDef in AppContext.Definitions.ShipsDefinitions)
            {
                SelectionsItems.Add(new ShipSelectorItemModel(shipDef));
            }

            if (SelectionsItems.Count > 0)
            {
                SelectedShipItem = SelectionsItems[0];
            }

            if (token != null)
            {
                Messenger.Default.Register<ShipAddedToBoardMessage>(this, token, (m) => ChangeItemsState(m.Content, (i) => i.AmountOfShips--));
                Messenger.Default.Register<ShipRemovedFromBoardMessage>(this, token, (m) => ChangeItemsState(m.Content, (i) => i.AmountOfShips++));
            }
        }

        public override void Cleanup()
        {
            base.Cleanup();
            Messenger.Default.Unregister(this);
        }

        public ObservableCollection<ShipSelectorItemModel> SelectionsItems { get; private set; }

        private ShipSelectorItemModel _SelectedShipItem;
        public ShipSelectorItemModel SelectedShipItem
        {
            get
            {
                return _SelectedShipItem;
            }
            set
            {
                if (Set(() => SelectedShipItem, ref _SelectedShipItem, value))
                {
                    Messenger.Default.Send<ShipSelectorChangedMessage>(new ShipSelectorChangedMessage(SelectedShipItem), token);
                }
            }
        }

        private bool _AllShipsArePlaced;
        public bool AllShipsArePlaced
        {
            get
            {
                return _AllShipsArePlaced;
            }
            set
            {
                Set(() => AllShipsArePlaced, ref _AllShipsArePlaced, value, true);
            }
        }

        private void SelectFirstAllowed()
        {
            int index = -1;
            if (SelectedShipItem != null && !SelectedShipItem.IsAllowedToSelect)
            {
                index = SelectionsItems.IndexOf(SelectedShipItem);
            }

            Func<int, int, bool> func = (iStart, iEnd) =>
            {
                for (int i = iStart; i < iEnd; i++)
                {
                    if (SelectionsItems[i].IsAllowedToSelect)
                    {
                        SelectedShipItem = SelectionsItems[i];
                        return true;
                    }
                }
                return false;
            };

            if (func(index + 1, SelectionsItems.Count))
            {
                return;
            }

            if (func(0, index))
            {
                return;
            }

            SelectedShipItem = null;
        }

        public void Substract(IEnumerable<IShip> ships)
        {
            foreach (IShip ship in ships)
            {
                ChangeItemsState(ship, (i) => i.AmountOfShips--);
            }
        }

        private void ChangeItemsState(IShip ship, Action<ShipSelectorItemModel> action)
        {
            var searchResult = SelectionsItems.FirstOrDefault((i) => i.IsTheSameShipType(ship));
            if (searchResult != null)
            {
                action(searchResult);
            }

            SelectFirstAllowed();

            AllShipsArePlaced = SelectionsItems.All((i) => i.AmountOfShips == 0);
        }       
    }    
}
