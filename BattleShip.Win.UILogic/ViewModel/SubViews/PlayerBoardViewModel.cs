﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BattleShip.Domain;

namespace BattleShip.ViewModel.Board
{
    public class PlayerBoardViewModel : BoardViewModel 
    {
        private IPlayerBoard playerBoard;

        public PlayerBoardViewModel(IPlayerBoard playerBoard)
            : base(playerBoard.SeaMap)
        {
            this.playerBoard = playerBoard;
        }
    }
}
