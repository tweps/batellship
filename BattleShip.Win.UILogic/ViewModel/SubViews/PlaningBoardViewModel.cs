﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

using BattleShip.Domain;
using BattleShip.ViewModel.Messeges;
using BattleShip.ViewModel.ShipSelector;
using BattleShip.Domain.Map;
using BattleShip.UILogic.Model;


namespace BattleShip.ViewModel.Board
{
    public class PlaningBoardViewModel : BoardViewModel 
    {
        private IPlayerBoard playerSeaBoardModel;
        private MessegeToken token;
       
        public PlaningBoardViewModel(IPlayerBoard playerSeaBoardModel, MessegeToken token)
            : base(playerSeaBoardModel.SeaMap)
        {
            this.token = token;
            this.playerSeaBoardModel = playerSeaBoardModel;
            
            Direction = ShipDirection.East;
            
            BoardPieceSelectedCommand = new RelayCommand<BoardPieceViewModel>((p)=>PlaceShip(p));            
            BoardPieceOptionCommand = new RelayCommand<BoardPieceViewModel>((p)=>ChangeProposShipDirection(p));
            BoardPieceProposeCommand = new RelayCommand<object>((p) => ProposeBoardPiece = (p as BoardPieceViewModel));

            Messenger.Default.Register<ShipSelectorChangedMessage>(this, token, (m) => ShipSelector = m.Content);
        }

        public override void Cleanup()
        {
            base.Cleanup();
            Messenger.Default.Unregister(this);
        }
                
        private ShipDirection _Direction = ShipDirection.East;
        protected ShipDirection Direction 
        {
            get
            {
                return _Direction;
            }
            set
            {
                if (Set(()=>Direction, ref _Direction, value))
                {
                    UpdateProposedShip();
                }
            }
        }

        private ShipSelectorItemModel _ShipSelector;
        public ShipSelectorItemModel ShipSelector
        {
            get
            {
                return _ShipSelector;
            }
            set
            {
                if (Set(() => ShipSelector, ref _ShipSelector, value))
                {
                    UpdateProposedShip();
                }
            }
        }
                

        private BoardPieceViewModel _ProposeBoardPiece;
        protected BoardPieceViewModel ProposeBoardPiece 
        {
            get
            {
                return _ProposeBoardPiece;
            }
            set
            {
                if (Set(()=>ProposeBoardPiece, ref _ProposeBoardPiece, value))
                {
                    UpdateProposedShip();
                }
            }
        }

        private void ChangeProposShipDirection(BoardPieceViewModel proposeBoardPiece)
        {
            switch (Direction)
            {
                case ShipDirection.North:
                    Direction = ShipDirection.East;
                    break;
                case ShipDirection.East:
                    Direction = ShipDirection.South;
                    break;
                case ShipDirection.South:
                    Direction = ShipDirection.West;
                    break;
                case ShipDirection.West:
                    Direction = ShipDirection.North;
                    break;
            }

            ProposeBoardPiece = proposeBoardPiece;
        }

        private void PlaceShip(BoardPieceViewModel proposeBoardPiece)
        {            
            if (proposeBoardPiece != null )
            {
                IShip ship = playerSeaBoardModel.GetShip(proposeBoardPiece.PositionX, proposeBoardPiece.PositionY);

                if (ship != null)
                {
                    // remove already placed ship                                       
                    playerSeaBoardModel.Remove(ship);

                    Messenger.Default.Send<ShipRemovedFromBoardMessage>(new ShipRemovedFromBoardMessage(ship), token);

                    foreach (ISeaMapPiece boardPiece in ship.Masts)
                    {
                        Reload(boardPiece);
                    }
                }
                else
                {
                    // Create new ship                    
                    if (ShipSelector != null && ShipSelector.AmountOfShips > 0)
                    {
                        ship = ShipFabric.Instance.Construct(ProposeBoardPiece.PositionX, ProposeBoardPiece.PositionY, ShipSelector.Masts, Direction);

                        if (playerSeaBoardModel.CanAddShip(ship))
                        {
                            playerSeaBoardModel.Add(ship);

                            foreach (ISeaMapPiece boardPiece in ship.Masts)
                            {
                                Reload(boardPiece);
                            }

                            Messenger.Default.Send<ShipAddedToBoardMessage>(new ShipAddedToBoardMessage(ship),token);                                
                        }
                    }
                }
            }
        }
        
        private void UpdateProposedShip()
        {            
            foreach (BoardPieceViewModel boardPieceViewModel in BoardPieces)
            {
                boardPieceViewModel.Highlighting = HighlightingType.NotHighlighted;
            }

            if (ProposeBoardPiece != null && ShipSelector != null && ShipSelector.AmountOfShips > 0)
            {
                IShip ship = playerSeaBoardModel.GetShip(ProposeBoardPiece.PositionX, ProposeBoardPiece.PositionY);

                if (ship == null)
                {                    
                    ship = ShipFabric.Instance.Construct(ProposeBoardPiece.PositionX, ProposeBoardPiece.PositionY, ShipSelector.Masts, Direction);
                    bool canBePlaced = playerSeaBoardModel.CanAddShip(ship);
                   
                    foreach (var mast in ship.Masts)
                    {                        

                        var piece = getBoardPieceViewModel(mast.PositionX, mast.PositionY);
                        if (piece != null)
                        {                            
                            piece.Highlighting = canBePlaced ? HighlightingType.Allowed : HighlightingType.NotAllowed;
                        }
                    }
                }
            }            
        }

        public void ConfirmShipPlacement()
        {
            playerSeaBoardModel.ConfirmShipPlacement();
        }
    }
}
