﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml;

using BattleShip.Domain;
using BattleShip.Domain.Map.Pieces;
using BattleShip.UILogic.Model;

namespace BattleShip.ViewModel.Board
{
    public class BoardPieceViewModel : BattleShipViewModelBase, ISeaMapPiece 
    {        
        #region ISeaMapPiece

        private int _PositionX;
        public int PositionX
        {
            get
            {
                return _PositionX;
            }
            set
            {
                Set(() => PositionX, ref _PositionX, value);
            }
        }

        private int _PositionY;
        public int PositionY
        {
            get
            {
                return _PositionY;
            }
            set
            {
                Set(() => PositionY, ref _PositionY, value);
            }
        }

        private BoardState _State;
        public BoardState State
        {
            get
            {
                return _State;
            }
            set
            {
                Set(() => State, ref _State, value);                
            }
        }

        private bool _IsHit;
        public bool IsHit
        {
            get
            {
                return _IsHit;
            }
            set
            {
                Set(() => IsHit, ref _IsHit, value);
            }
        }

        public bool Equals(ISeaMapPiece other)
        {
            return SeaMapPieceComparer.Instance.Equals(this, other);
        }

        public override bool Equals(object obj)
        {
            if (obj is ISeaMapPiece)
            {
                return Equals((ISeaMapPiece)obj);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return SeaMapPieceComparer.Instance.GetHashCode(this);
        }

        #endregion ISeaMapPiece

        public BoardPieceViewModel()
        {
            PositionX = 0;
            PositionY = 0;
            State = BoardState.Unknown;
        }

        public BoardPieceViewModel(ISeaMapPiece sourceBoardPiece)
        {
            Update(sourceBoardPiece);
        }


        public void Update(ISeaMapPiece sourceBoardPiece)
        {
            if (!SeaMapPieceComparer.Instance.Equals(this, sourceBoardPiece))
            {
                this.PositionX = sourceBoardPiece.PositionX;
                this.PositionY = sourceBoardPiece.PositionY;
                this.IsHit = sourceBoardPiece.IsHit;
                this.State = sourceBoardPiece.State;
            }
        }

        private HighlightingType _Highlighting;
        public HighlightingType Highlighting
        {
            get
            {
                return _Highlighting;
            }
            set
            {
                Set(()=>Highlighting, ref _Highlighting, value);
            }
        }       
    }
}
