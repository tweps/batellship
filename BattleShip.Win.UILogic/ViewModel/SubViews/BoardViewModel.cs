﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using BattleShip.Domain;


namespace BattleShip.ViewModel.Board
{
    public abstract class BoardViewModel : BattleShipViewModelBase
    {       
        protected BoardViewModel(ISeaMap seaMap)
        {
            this.SeaMap = seaMap;

            BoardPieces = new ObservableCollection<BoardPieceViewModel>();
            foreach (ISeaMapPiece boardPiece in seaMap.OrderBy((p) => GetIndex(p.PositionX, p.PositionY)))
            {
                BoardPieces.Add(new BoardPieceViewModel(boardPiece));
            }                      
        }

        protected void Reaload()
        {            
            foreach (ISeaMapPiece boardPiece in SeaMap)
            {
                Reload(boardPiece);
            }
        }

        protected void Reload(ISeaMapPiece boardPiece)
        {
            ISeaMapPiece src = SeaMap[boardPiece.PositionX, boardPiece.PositionY];
            BoardPieceViewModel boardPieceViewModel = getBoardPieceViewModel(boardPiece.PositionX, boardPiece.PositionY);

            boardPieceViewModel.Update(src);            
        }
        
        public ISeaMap SeaMap { get; private set; }
        
        public IEnumerable<string> LabelsColumns
        {
            get
            {
                return Enumerable.Range(0, SeaMap.SizeX).Select((i) => (i + 1).ToString());
            }
        }

        public IEnumerable<string> LabelsRows
        {
            get
            {
                return Enumerable.Range(0, SeaMap.SizeY).Select((i) => ((Char)(((Int32)'A') + i)).ToString());
            }
        }

        public int MaximumRowsOrColumns
        {
            get
            {
                return SeaMap.SizeX;
            }
        }
        
        public ObservableCollection<BoardPieceViewModel> BoardPieces { get; private set; }

        private int GetIndex(int x, int y)
        {
            return x * MaximumRowsOrColumns + y;
        }

        protected BoardPieceViewModel getBoardPieceViewModel(int x, int y)
        {
            if( x >= MaximumRowsOrColumns || y >= MaximumRowsOrColumns || x<0 || y<0 )
            {
                return null;
            }

            int i = GetIndex(x, y);
            if (i < 0 || i > BoardPieces.Count - 1 )
            {
                return null;
            }
            return BoardPieces[i];
        }

        public ICommand BoardPieceSelectedCommand { get; protected set; }
        public ICommand BoardPieceProposeCommand { get; protected set; }
        public ICommand BoardPieceOptionCommand { get; protected set; }        
    }
}
