﻿using BattleShip.ViewModel.ShipSelector;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.ViewModel.Messeges
{
    public class ShipSelectorChangedMessage : GenericMessage<ShipSelectorItemModel>
    {
        public ShipSelectorChangedMessage(ShipSelectorItemModel shipSelectorItem)
            : base(shipSelectorItem)
        {
        }        
    }
}
