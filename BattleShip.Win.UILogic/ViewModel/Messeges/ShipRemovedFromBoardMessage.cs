﻿using BattleShip.Domain;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.ViewModel.Messeges
{
    public class ShipRemovedFromBoardMessage : GenericMessage<IShip>
    {
        public ShipRemovedFromBoardMessage(IShip ship)
            : base(ship)
        {
        }
    }
}
