﻿using BattleShip.Domain;
using BattleShip.UILogic.Model;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System.UserProfile;
using Windows.UI.Xaml.Media.Imaging;

namespace BattleShip
{
    public static class AppContext
    {
        static AppContext()
        {
            var task = LoadDefaultPlayer();
            task.Wait();

            CurrentPlayer = task.Result;
        }


        public readonly static PlayerModel CurrentPlayer;
        

        private async static Task<PlayerModel> LoadDefaultPlayer()
        {            
            return new PlayerModel
            {
                PlayerName = await UserInformation.GetDisplayNameAsync(),               
            };
        }
            
        
        public static IGameDefinitions Definitions
        {
            get
            {
                return ServiceLocator.Current.GetInstance<IGameDefinitions>();
            }
        }
    }
}
