﻿using BattleShip.Domain;
using BattleShip.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System.UserProfile;
using Windows.UI.Xaml.Media.Imaging;

namespace BattleShip.UILogic.Services
{
    public class AvatarsService
    {
        public readonly static AvatarsService Instance = new AvatarsService();

        private AvatarsService()
        {
        }


        private BitmapImage avatar = null;
        public async Task<BitmapImage> LoadAvatarAsync(IPlayer player)
        {
            if (BattleShipViewModelBase.IsInDesignModeStatic)
            {
                return null;
            }

            if (avatar == null)
            {
                try
                {
                    StorageFile image = UserInformation.GetAccountPicture(AccountPictureKind.SmallImage) as StorageFile;
                    if (image != null)
                    {
                        IRandomAccessStream imageStream = await image.OpenReadAsync();
                        avatar = new BitmapImage();
                        avatar.SetSource(imageStream);
                    }
                }
                catch
                {
                    avatar = null;
                }
            }

            return avatar;
        }

    }
}
