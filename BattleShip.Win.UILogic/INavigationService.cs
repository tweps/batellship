﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace BattleShip.UILogic
{
    public interface INavigationService
    {
        //event NavigatingCancelEventHandler Navigating;
        void InitializeFrame(Frame frame);
        bool Navigate(NavigationPage page);
        bool Navigate(NavigationPage page, object parameter);
        void GoBack();        
    }   
}
