﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UILogic.Model
{
    public enum HighlightingType
    {
        NotHighlighted,
        Allowed,
        NotAllowed
    }
}
