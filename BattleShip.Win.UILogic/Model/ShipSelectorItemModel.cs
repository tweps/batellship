﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using BattleShip.Domain;

namespace BattleShip.ViewModel.ShipSelector
{
    public class ShipSelectorItemModel : ObservableObject
    {
        public ShipSelectorItemModel()
        {
        }

        public ShipSelectorItemModel(ShipDefinition shipDefinition)
        {
            Masts = shipDefinition.Masts;
            AmountOfShips = shipDefinition.AmountOfShips;
        }

        private int _Masts;
        public int Masts
        {
            get
            {
                return _Masts;
            }
            set
            {
                if(Set(()=>Masts, ref _Masts, value))                
                {
                    RaisePropertyChanging(() => ShipType);                    
                }
            }
        }

        private int _AmountOfShips;
        public int AmountOfShips
        {
            get
            {
                return _AmountOfShips;
            }
            set
            {                
                Set(()=>AmountOfShips,ref _AmountOfShips, value);
            }
        }


        public string ShipType
        {
            get
            {
                StringBuilder result = new StringBuilder();
                for (int i = 0; i < Masts; i++)
                {
                    result.Append("\uE003");
                }

                return result.ToString();
            }
        }

        public bool IsAllowedToSelect
        {
            get
            {
                return Masts > 0 && AmountOfShips > 0;
            }
        }

        public bool IsTheSameShipType(IShip ship)
        {
            return ship.Masts.Count() == Masts;
        }
    }
}
