﻿using BattleShip.Domain;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System.UserProfile;
using Windows.UI.Xaml.Media.Imaging;

namespace BattleShip.UILogic.Model
{
    public class PlayerModel : ObservableObject, IPlayer
    {
        public string PlayerName { get; set; }
        public string AvatarUrl { get; set; }        
    }
}
