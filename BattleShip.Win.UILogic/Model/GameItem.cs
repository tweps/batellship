﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GalaSoft.MvvmLight;

using BattleShip.Domain;
using Windows.UI.Xaml.Media.Imaging;

namespace BattleShip.UILogic.Model
{
    public abstract class GameItem : ObservableObject
    {
        public string Category { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }

        public Action Execute { get; set; }
    }

    public class GameItemOneImage : GameItem
    {
        public BitmapImage Image { get; set; }
    }


    public class GameItemTwoImages : GameItem
    {
        public BitmapImage ImageFirst { get; set; }
        public BitmapImage ImageSecond { get; set; }
    }

    public class GameItemSymbol : GameItem
    {
        public string Symbol { get; set; }
    }
}
